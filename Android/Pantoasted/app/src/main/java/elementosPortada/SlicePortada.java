package elementosPortada;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pantoasted.app.FrontSlices;
import com.pantoasted.app.R;

/**
 * Created by root on 30-06-14.
 */
public class SlicePortada extends FrameLayout {

    Context context;
    TextView titulo,subtitulo;
    int id;

    public SlicePortada(Context context,String titulo,String subtitulo,Bitmap imagen,String id,int id2,ViewGroup.LayoutParams parametros1,ViewGroup.LayoutParams parametros2) {
        super(context);
        this.context = context;
        this.id = id2;
        //Inicializo los valores y ademas construyo los widgets que contiene
        construirTextView(titulo,subtitulo);

        //Construyo mi FrameLayout
        this.setId(Integer.parseInt(id));
        //Obtener alguna medida en dpi
        final float scale = context.getResources().getDisplayMetrics().density;
        int pixels = (int) (135 * scale + 0.5f);
        this.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, pixels));
        this.setBackground(new BitmapDrawable(imagen));


        this.addView(this.titulo,parametros1);
        this.addView(this.subtitulo,parametros2);
    }

    public void construirTextView(String titulo,String subtitulo){
        //Construyo 2 textViews para luego inicializarlos
        this.titulo = new TextView(this.getContext());
        this.subtitulo = new TextView(this.getContext());

        //Titulo
        this.titulo.setId(++this.id);
        this.titulo.setText(titulo);
        this.titulo.setTextColor(Color.WHITE);
        //Subtitulo
        this.subtitulo.setId(++this.id);
        this.subtitulo.setText(subtitulo);
        this.subtitulo.setTextColor(Color.WHITE);

    }
}
