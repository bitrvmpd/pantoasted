package decodificador;

/**
 * Created by root on 07-06-14.
 */

import com.google.gson.*;

public class JSONDecoder {

    //Decodificador de la cadena en formato JSON
    public static String decodificar(String cadenaJson, String id) {
        Gson gson = new Gson(); //Instanciar googleJson
        JsonParser parser = new JsonParser(); //Instanciar decodificador
        JsonElement obj = parser.parse(cadenaJson); //Convierte String a Json
        JsonObject jsobj = obj.getAsJsonObject();//Convierte para obtener por id
        return jsobj.get(id).getAsString();//Se obtiene el por id deseado
        //y se retorna como String limpio.
    }

    //No decodifica, solo transforma a objeto json, luego lo recorro y uso decodificar con objeto
    public static JsonArray retornoArray(String cadenaJson){
        try{
            Gson gson = new Gson();
            JsonParser parser = new JsonParser();
            JsonArray obj = parser.parse(cadenaJson).getAsJsonArray();
            return obj;
        }
        catch(Exception ex){
            return null;
        }
    }

    public static String decodificar(JsonElement jso,String id){
        return jso.getAsJsonObject().get(id).getAsString();
    }

}
