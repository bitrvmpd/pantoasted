package com.pantoasted.app;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeIntents;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.google.gson.JsonArray;

import org.apache.http.HttpException;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;

import decodificador.JSONDecoder;
import decodificador.staticFiles;


public class DetalleSlice extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener,YouTubeThumbnailView.OnInitializedListener,RatingBar.OnRatingBarChangeListener {

    int onStartCount = 0;
    String  id_rev = "",url_video="";
    TextView titulo,subtitulo,cuerpo,cuerpoComentario;
    FrameLayout header,frameYoutube,nuevoComentario,textoComentario;
    RatingBar puntuacion;
    final String DEVELOPER_KEY = "AIzaSyBwvlabTgHVE4wBBqMF0urwyn36l5_XBCQ";
    YouTubeThumbnailView.OnInitializedListener listener;
    YouTubeThumbnailView youTubeView;
    YouTubeThumbnailLoader youtubeLoader;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    Button seccionComentarios,enviarComentario;
    LinearLayout listaDetalle,listaComentarios;
    RelativeLayout contenedorComentario;
    EditText txtNuevoComentario;
    String calificacion;
    String puntos;
    JsonArray coleccionComentariosArray;
    String coleccionComentariosString;
    ImageView imagenAvatar;
    boolean comentariosOcultos = false;
    Bitmap[] imagenesAvatar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detalle_slice);
        //Animacion
        onStartCount = 1;
        if (savedInstanceState == null) // 1st time
        {
            this.overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left);
        } else // already created so reverse animation
        {
            onStartCount = 2;
        }
        //Fin animacion

        //Rescato los elementos a llenar
        puntuacion = (RatingBar) findViewById(R.id.ratingBar);
        id_rev = getIntent().getStringExtra("id_rev");
        titulo = (TextView) findViewById(R.id.lblTituloDetalle);
        subtitulo = (TextView) findViewById(R.id.lblSubtituloDetalle);
        cuerpo = (TextView) findViewById(R.id.lblCuerpo);
        header = (FrameLayout) findViewById(R.id.pictureHeader);
        frameYoutube = (FrameLayout) findViewById(R.id.frameYoutube);
        nuevoComentario = (FrameLayout) findViewById(R.id.nuevoComentario);
        seccionComentarios = (Button) findViewById(R.id.button2);
        enviarComentario = (Button) findViewById(R.id.button);
        listaDetalle = (LinearLayout) findViewById(R.id.linearLayoutDetalle);
        listaComentarios = (LinearLayout) findViewById(R.id.listaComentarios);
        txtNuevoComentario = (EditText) findViewById(R.id.txtNuevoComentario);
        contenedorComentario = (RelativeLayout) findViewById(R.id.contenedorComenario);
        imagenAvatar = (ImageView) findViewById(R.id.imageView2);
        textoComentario = (FrameLayout) findViewById(R.id.contenedorTexto);
        cuerpoComentario = (TextView) findViewById(R.id.textView);

        cuerpo.setMovementMethod(new ScrollingMovementMethod());

        //Agrego onclickListener
        seccionComentarios.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Check if Name text control is not empty
                if (comentariosOcultos) {
                    listaDetalle.removeView(listaComentarios);
                    comentariosOcultos = false;
                } else {
                    listaDetalle.addView(listaComentarios,listaDetalle.getChildCount());
                    comentariosOcultos = true;
                }
            }
        });

        //Listener nuevo comentario
        enviarComentario.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(txtNuevoComentario.getText().length()>0 && txtNuevoComentario.getText().toString()!=""){
                    AsyncCallWS async2 = new AsyncCallWS();
                    String[] test = {"nuevo"};
                    async2.execute(test);
                }
            }
        });

        //Inicio la consulta del WS
        AsyncCallWS async = new AsyncCallWS();
        async.execute();


        //Debo generar el video desde aqui
        youTubeView = (YouTubeThumbnailView)findViewById(R.id.youtube_view);

         listener = this;

        //Determino si el usuario esta logueado o no, si no lo esta; no se mostrarán comentarios ni
        //La barra de puntuación
        if(staticFiles.permiso!=""){
            puntuacion.setVisibility(View.VISIBLE);
            nuevoComentario.setVisibility(View.VISIBLE);
        }else{
            puntuacion.setVisibility(View.GONE);
            nuevoComentario.setVisibility(View.GONE);
        }
        //Elimina el linearlayout que contendra todo.
        listaDetalle.removeView(listaComentarios);
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                        YouTubeInitializationResult error) {
        Toast.makeText(this, "Oh no! " + error.toString(),
                Toast.LENGTH_LONG).show();
    }
    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player,
                                        boolean wasRestored) {
        //player.loadVideo(url_video);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.detalle_slice, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(this, FrontSlices.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        if (onStartCount > 1) {
            this.overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);

        } else if (onStartCount == 1) {
            onStartCount++;
        }
    }

    @Override
    public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {

        try {
            youtubeLoader = youTubeThumbnailLoader ;
            youTubeThumbnailLoader.setOnThumbnailLoadedListener(new ThumbnailLoadedListener());

            youtubeLoader.setVideo(url_video);
        }catch(Exception ex){
            Toast.makeText(getApplicationContext(),ex.getMessage(),Toast.LENGTH_LONG);
        }
    }

    @Override
    public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {

    }

    @Override
    public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
        puntos= Math.round( v*2) + "";
        AsyncCallWS task3 = new AsyncCallWS();
        task3.execute("puntos");
    }

    //Async task
    private class AsyncCallWS extends AsyncTask<String, Void, Void> {
        JsonArray arregloRandom;
        String arrayJson;
        Bitmap[] imagenes = new Bitmap[4];
        Context context;
        String result2="";
        String param;


        @Override
        protected Void doInBackground(String... params) {
            //Invoke webservice in background
            //Prueba para retornar publicaciones
            try {
                if(params!=null && params.length>0) {
                    switch (params[0].substring(0, 1).toCharArray()[0]) {
                        case 'c':
                            //Llamada a metodo que carga los comentarios
                            param ="c";
                            break;
                        case 'n':
                            //Llamada al WS que ingresa el nuevo comentario
                            if (txtNuevoComentario.getText().length() > 0 && txtNuevoComentario.getText().toString() != "") {
                                param ="n";
                                result2 = WebService.nuevoComentario(id_rev, staticFiles.usuario, txtNuevoComentario.getText().toString());

                                //Recargar los comentarios o solo agregar al final el nuevo

                            }
                        case 'p':
                            result2 = WebService.actualizarPuntosPublicacion(id_rev,puntos,puntos,puntos,puntos);
                            param ="p";
                            return null;
                    }
                }
                arrayJson = WebService.consultarReview(id_rev);
                coleccionComentariosString = WebService.obtenerComentarios(id_rev);
                imagenes = cargaImagenes(arrayJson);
                cargaImagen();

            }catch (Exception ex){
                System.out.println(ex.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //Set response
            //tv.setText(displayText);
            //Make ProgressBar invisible
            //pg.setVisibility(View.INVISIBLE);
            //Toast.makeText(getApplicationContext(), mensajeToast, Toast.LENGTH_SHORT).show();
            try{

                if(result2.equals("true")){
                    String mensaje="";
                    if(param.equals("p")){
                        mensaje="Puntuacion almacenada exitosamente!";
                    }else if(param.equals("n")){
                        mensaje = "Comentario almacenado exitosamente!";
                    }
                    if(mensaje!="") {
                        Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT).show();
                    }
                    txtNuevoComentario.setText("");
                    result2="";
                }else{
                    int index_img = getIntent().getIntExtra("index_img", 0);
                    Bitmap imagenes2 = imagenes[0];
                    String arrayJson2 = arrayJson;
                    //Lleno con la informacion
                    header.setBackground(new BitmapDrawable(imagenes2));
                    titulo.setText(JSONDecoder.decodificar(arrayJson2, "titulo"));
                    subtitulo.setText(JSONDecoder.decodificar(arrayJson2, "subtitulo") + " - By " + JSONDecoder.decodificar(arrayJson2, "autor"));
                    cuerpo.setText(JSONDecoder.decodificar(arrayJson2, "review"));
                    cuerpo.setMovementMethod(new ScrollingMovementMethod());
                    url_video = JSONDecoder.decodificar(arrayJson2, "video");
                    calificacion = JSONDecoder.decodificar(arrayJson2, "puntuacion");
                    calificacion = calificacion.substring(0,1)+"."+calificacion.substring(2,3);
                    puntuacion.setRating(Float.parseFloat(calificacion)/2);
                    cargarComentarios();
                    //youTubeView.initialize(DEVELOPER_KEY, listener);
                    youTubeView.initialize(DEVELOPER_KEY, listener);

                    puntuacion.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                        public void onRatingChanged(RatingBar ratingBar, float rating,
                                                    boolean fromUser) {
                            puntos= Math.round(rating*2) + "";
                            AsyncCallWS task3 = new AsyncCallWS();
                            task3.execute("puntos");

                        }
                    });
                    youTubeView.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            Intent intent = YouTubeIntents.createPlayVideoIntentWithOptions(getApplicationContext(), url_video, true, true);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            getApplicationContext().startActivity(intent);
                        }
                    });
                }

            }catch(Exception ex){
                System.out.println(ex.getMessage());
            }
        }

        @Override
        protected void onPreExecute() {
            //Make ProgressBar invisible
            //pg.setVisibility(View.VISIBLE);
            //Hide keyboard
            ocultarTeclado();
        }


        @Override
        protected void onProgressUpdate(Void... values) {
        }



    }

    //Metodo para ocultar el teclado justo cuando se inicia el thread
    private void ocultarTeclado(){
        InputMethodManager inputManager = (InputMethodManager) this
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        //check if no view has focus:
        View v=this.getCurrentFocus();
        if(v==null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }


    public Bitmap[] cargaImagenes(String arrayJson){
        JsonArray insideArreglo = JSONDecoder.retornoArray(arrayJson);
        Bitmap[] x2 = new Bitmap[1];

        try {
            Bitmap x = null;
                String url = JSONDecoder.decodificar(arrayJson, "header");
                InputStream input = null;
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bmOptions.inSampleSize = 1;

                URLConnection connection = new URL(url).openConnection();
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestMethod("GET");

                httpConnection.connect();
                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    input = httpConnection.getInputStream();
                }

                x = BitmapFactory.decodeStream(input, null, bmOptions);
                input.close();
                x2[0] = x;

        }
        catch(Exception ex){

        }

        return x2;
    }

    private final class ThumbnailLoadedListener implements
            YouTubeThumbnailLoader.OnThumbnailLoadedListener {

        @Override
        public void onThumbnailError(YouTubeThumbnailView arg0, YouTubeThumbnailLoader.ErrorReason arg1) {

        }

        @Override
        public void onThumbnailLoaded(YouTubeThumbnailView arg0, String arg1) {

        }

    }


    public void cargarComentarios(){
        coleccionComentariosArray = JSONDecoder.retornoArray(coleccionComentariosString);

        for (int i = 0 ; i<coleccionComentariosArray.size();i++){
            RelativeLayout rl = new RelativeLayout(this);
            //Construir relative layout e ir agregandolo a linearLayout
            rl.setLayoutParams(contenedorComentario.getLayoutParams());
            rl.setId(i + 8000);

            //Construyo la imagen
            ImageView iv =new ImageView(this);
            iv.setId(i+9000);
            iv.setLayoutParams(imagenAvatar.getLayoutParams());
            iv.setMaxWidth(50);
            iv.setMaxHeight(50);
            iv.setBackground(new BitmapDrawable(imagenesAvatar[i]));
            //iv.setBackground(imagenAvatar.getBackground());
            //Construyo el texto
            FrameLayout fl  = new FrameLayout(this);
            fl.setId(10000+i);

            RelativeLayout.LayoutParams parametrosRelativos  = (RelativeLayout.LayoutParams)textoComentario.getLayoutParams();
            parametrosRelativos.addRule(RelativeLayout.RIGHT_OF,iv.getId());

            fl.setLayoutParams(parametrosRelativos);
            fl.setBackground(textoComentario.getBackground());

                //Construyo el textview que va dentro
                TextView tv = new TextView(this);
                tv.setId(7000+i);
                tv.setLayoutParams(cuerpoComentario.getLayoutParams());
                tv.setTextColor(cuerpoComentario.getTextColors());
                tv.setText(JSONDecoder.decodificar(coleccionComentariosArray.get(i),"cuerpo"));
            fl.addView(tv,textoComentario.getLayoutParams());

            //Finalmente agrego ambos a mi relativeLayout y luego al LinearLayout
            rl.addView(iv,imagenAvatar.getLayoutParams());
            rl.addView(fl,parametrosRelativos);
            listaComentarios.addView(rl,contenedorComentario.getLayoutParams());
        }
    }

    public void cargaImagen(){
        coleccionComentariosArray = JSONDecoder.retornoArray(coleccionComentariosString);
        imagenesAvatar = new Bitmap[coleccionComentariosArray.size()];

        try {
            for (int i = 0;i<coleccionComentariosArray.size();i++){
                Bitmap x = null;
                String url = "http://201.241.0.13:8080/PantoastedServer" + JSONDecoder.decodificar(coleccionComentariosArray.get(i),"avatar");
                //String url = "http://192.168.1.132:8080/PantoastedServer"+ JSONDecoder.decodificar(coleccionComentariosArray.get(i),"avatar");
                InputStream input = null;
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bmOptions.inSampleSize = 1;

                URLConnection connection = new URL(url).openConnection();
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestMethod("GET");
                httpConnection.connect();
                int code = httpConnection.getResponseCode();

                if (code == HttpURLConnection.HTTP_OK) {
                    input = httpConnection.getInputStream();
                }

                x = BitmapFactory.decodeStream(input, null, bmOptions);
                imagenesAvatar[i] = x;
                input.close();
            }
        } catch(Exception ex){
            System.out.println(ex.getMessage());
        }

    }



}
