package com.pantoasted.app;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import decodificador.staticFiles;


public class MainActivity extends ActionBarActivity {

    Button b;
    TextView tv;
    EditText et,et2;
    ProgressBar pg;
    String editText,editText2;
    String displayText;
    String mensajeToast;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Inicializo y rescato controles
        //Name Text control
        et = (EditText) findViewById(R.id.editText1);
        //Name Text control
        et2 = (EditText) findViewById(R.id.editText2);
        //Display Text control
        tv = (TextView) findViewById(R.id.tv_result);
        //Button to trigger web service invocation
        b = (Button) findViewById(R.id.button1);
        //Display progress bar until web service invocation completes
        pg = (ProgressBar) findViewById(R.id.progressBar1);
        //Button Click Listener
        b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Check if Name text control is not empty
                if (et.getText().length() != 0 && et.getText().toString() != "") {
                    //Get the text control value
                    editText = et.getText().toString();
                    editText2 = et2.getText().toString();
                    //Create instance for AsyncCallWS
                    AsyncCallWS task = new AsyncCallWS();
                    //Call execute
                    task.execute();
                    //If text control is empty
                } else {
                    tv.setText("Please enter name");
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private class AsyncCallWS extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            //Invoke webservice in background
            String credenciales = WebService.iniciarSesion(editText,editText2);
            String displayText2 = decodificador.JSONDecoder.decodificar(credenciales,"privilegio");
            String usuario = editText;

            if(displayText2.equals("admin")){
                mensajeToast = "LogIn exitoso!";
                staticFiles.usuario = usuario;
                staticFiles.permiso = displayText2;
            }else if(displayText2.equals("normal")){
                mensajeToast = "LogIn exitoso!";
                staticFiles.usuario = usuario;
                staticFiles.permiso = displayText2;
            }else if(displayText2.equals("autor")){
                mensajeToast = "LogIn exitoso!";
                staticFiles.usuario = usuario;
                staticFiles.permiso = displayText2;
            }else if(displayText2.equals("null")){
                mensajeToast = "Combinacion usuario/contraseña incorrecta.";
            }

            //Prueba para retornar publicaciones
            //displayText = WebService.retornaRandom();

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //Set response
            tv.setText(displayText);
            //Make ProgressBar invisible
            pg.setVisibility(View.INVISIBLE);
            Toast.makeText(getApplicationContext(), mensajeToast,Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onPreExecute() {
            //Make ProgressBar invisible
            pg.setVisibility(View.VISIBLE);
            //Hide keyboard
            ocultarTeclado();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }



    }

    //Metodo para ocultar el teclado justo cuando se inicia el thread
    private void ocultarTeclado(){
        InputMethodManager inputManager = (InputMethodManager) this
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        //check if no view has focus:
        View v=this.getCurrentFocus();
        if(v==null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
}
