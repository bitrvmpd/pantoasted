package com.pantoasted.app;

/**
 * Created by edo on 07-06-14.
 */

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;


//Clase donde se hace el llamado a todos los webservices Que entega mi WSDL
public class WebService {

    //Namespace of the Webservice - can be found in WSDL
    private static String NAMESPACE = "http://controlador/";
    //Webservice URL - WSDL File location
    //Aqui es donde se ingresa la ubicacion del WS, por default dejare la mia externa, asi pueden
    //probar sin problemas
    private static String URL = "http://201.241.0.13:8080/WSStandAlone/PantoastedWS?wsdl";
    //private static String URL = "http://192.168.1.132:8080/WSStandAlone/PantoastedWS?wsdl";
    //SOAP Action URI again Namespace + Web method name
    private static String SOAP_ACTION = "http://controlador/";

    public static String iniciarSesion(String usuario, String contrasena) {
        String resTxt = null;
        String webMethName = "iniciarSesion";
        // Create request
        SoapObject request = new SoapObject(NAMESPACE, webMethName);
        // Property which holds input parameters --> only hold one parameter
        PropertyInfo usuarioP = new PropertyInfo();
        PropertyInfo contrasenaP = new PropertyInfo();
        // Set Usuario
        usuarioP.setName("usuario");
        // Set Value
        usuarioP.setValue(usuario);
        // Set Contrasena
        contrasenaP.setName("contrasena");
        // Set Value
        contrasenaP.setValue(contrasena);
        // Set dataType
        usuarioP.setType(String.class);
        contrasenaP.setType(String.class);
        // Add the property to request object
        request.addProperty(usuarioP);
        request.addProperty(contrasenaP);
        // Create envelope
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

        try {
            // Invoke web service
            androidHttpTransport.call(SOAP_ACTION + webMethName, envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            // Assign it to resTxt variable static variable
            resTxt = response.toString();

        } catch (Exception e) {
            //Print error
            e.printStackTrace();
            //Assign error message to resTxt
            resTxt = "Error occured";
        }
        //Return resTxt to calling object
        return resTxt;

    }

    public static String registrarUsuario(String usuario, String contrasena) {
        String resTxt = null;
        String webMethName = "registrarUsuario";
        // Create request
        SoapObject request = new SoapObject(NAMESPACE, webMethName);
        // Property which holds input parameters --> only hold one parameter
        PropertyInfo usuarioP = new PropertyInfo();
        PropertyInfo contrasenaP = new PropertyInfo();
        // Set Usuario
        usuarioP.setName("usuario");
        // Set Value
        usuarioP.setValue(usuario);
        // Set Contrasena
        contrasenaP.setName("contrasena");
        // Set Value
        contrasenaP.setValue(contrasena);
        // Set dataType
        usuarioP.setType(String.class);
        contrasenaP.setType(String.class);
        // Add the property to request object
        request.addProperty(usuarioP);
        request.addProperty(contrasenaP);
        // Create envelope
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

        try {
            // Invoke web service
            androidHttpTransport.call(SOAP_ACTION + webMethName, envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            // Assign it to resTxt variable static variable
            resTxt = response.toString();

        } catch (Exception e) {
            //Print error
            e.printStackTrace();
            //Assign error message to resTxt
            resTxt = "Error occured";
        }
        //Return resTxt to calling object
        return resTxt;

    }

    public static String crearPerfil(String usuario,String correo,String url_avatar,String fecha_nac,String sexo) {
        String resTxt = null;
        String webMethName = "crearPerfil";
        // Create request
        SoapObject request = new SoapObject(NAMESPACE, webMethName);
        // Property which holds input parameters --> only hold one parameter
        PropertyInfo usuarioP = new PropertyInfo();
        PropertyInfo correoP = new PropertyInfo();
        PropertyInfo url_avatarP = new PropertyInfo();
        PropertyInfo fecha_nacP = new PropertyInfo();
        PropertyInfo sexoP = new PropertyInfo();
        // Set Usuario
        usuarioP.setName("usuario");
        correoP.setName("correo");
        url_avatarP.setName("url_avatar");
        fecha_nacP.setName("fecha_nac");
        sexoP.setName("sexo");
        // Set Value
        usuarioP.setValue(usuario);
        correoP.setValue(correo);
        url_avatarP.setValue(url_avatar);
        fecha_nacP.setValue(fecha_nac);
        sexoP.setValue(sexo);
        // Set dataType
        usuarioP.setType(String.class);
        correoP.setType(String.class);
        url_avatarP.setType(String.class);
        fecha_nacP.setType(String.class);
        sexoP.setType(String.class);
        // Add the property to request object
        request.addProperty(usuarioP);
        request.addProperty(correoP);
        request.addProperty(url_avatarP);
        request.addProperty(fecha_nacP);
        request.addProperty(sexoP);
        // Create envelope
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

        try {
            // Invoke web service
            androidHttpTransport.call(SOAP_ACTION + webMethName, envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            // Assign it to resTxt variable static variable
            resTxt = response.toString();

        } catch (Exception e) {
            //Print error
            e.printStackTrace();
            //Assign error message to resTxt
            resTxt = "Error occured";
        }
        //Return resTxt to calling object
        return resTxt;

    }

    public static String cargarPerfil(String usuario) {
        String resTxt = null;
        String webMethName = "cargarPerfil";
        // Create request
        SoapObject request = new SoapObject(NAMESPACE, webMethName);
        // Property which holds input parameters --> only hold one parameter
        PropertyInfo usuarioP = new PropertyInfo();
        // Set Usuario
        usuarioP.setName("usuario");
        // Set Value
        usuarioP.setValue(usuario);
        // Set dataType
        usuarioP.setType(String.class);
        // Add the property to request object
        request.addProperty(usuarioP);
        // Create envelope
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

        try {
            // Invoke web service
            androidHttpTransport.call(SOAP_ACTION + webMethName, envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            // Assign it to resTxt variable static variable
            resTxt = response.toString();

        } catch (Exception e) {
            //Print error
            e.printStackTrace();
            //Assign error message to resTxt
            resTxt = "Error occured";
        }
        //Return resTxt to calling object
        return resTxt;

    }

    public static String consultarReview(String id_review) {
        String resTxt = null;
        String webMethName = "consultarReview";
        // Create request
        SoapObject request = new SoapObject(NAMESPACE, webMethName);
        // Property which holds input parameters --> only hold one parameter
        PropertyInfo id_reviewP = new PropertyInfo();
        // Set Usuario
        id_reviewP.setName("id_review");
        // Set Value
        id_reviewP.setValue(id_review);
        // Set dataType
        id_reviewP.setType(String.class);
        // Add the property to request object
        request.addProperty(id_reviewP);
        // Create envelope
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

        try {
            // Invoke web service
            androidHttpTransport.call(SOAP_ACTION + webMethName, envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            // Assign it to resTxt variable static variable
            resTxt = response.toString();

        } catch (Exception e) {
            //Print error
            e.printStackTrace();
            //Assign error message to resTxt
            resTxt = "Error occured";
        }
        //Return resTxt to calling object
        return resTxt;

    }

    public static String obtenerComentarios(String id_review) {
        String resTxt = null;
        String webMethName = "obtenerComentarios";
        // Create request
        SoapObject request = new SoapObject(NAMESPACE, webMethName);
        // Property which holds input parameters --> only hold one parameter
        PropertyInfo id_reviewP = new PropertyInfo();
        // Set Usuario
        id_reviewP.setName("id_review");
        // Set Value
        id_reviewP.setValue(id_review);
        // Set dataType
        id_reviewP.setType(String.class);
        // Add the property to request object
        request.addProperty(id_reviewP);
        // Create envelope
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

        try {
            // Invoke web service
            androidHttpTransport.call(SOAP_ACTION + webMethName, envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            // Assign it to resTxt variable static variable
            resTxt = response.toString();

        } catch (Exception e) {
            //Print error
            e.printStackTrace();
            //Assign error message to resTxt
            resTxt = "Error occured";
        }
        //Return resTxt to calling object
        return resTxt;

    }

    public static String nuevoComentario(String id_review,String id_usuario,String cuerpo) {
        String resTxt = null;
        String webMethName = "nuevoComentario";
        // Create request
        SoapObject request = new SoapObject(NAMESPACE, webMethName);
        // Property which holds input parameters --> only hold one parameter
        PropertyInfo id_reviewP = new PropertyInfo();
        PropertyInfo id_usuarioP = new PropertyInfo();
        PropertyInfo cuerpoP = new PropertyInfo();
        // Set Usuario
        id_reviewP.setName("id_review");
        id_usuarioP.setName("id_usuario");
        cuerpoP.setName("cuerpo");
        // Set Value
        id_reviewP.setValue(id_review);
        id_usuarioP.setValue(id_usuario);
        cuerpoP.setValue(cuerpo);
        // Set dataType
        id_reviewP.setType(String.class);
        id_usuarioP.setType(String.class);
        cuerpoP.setType(String.class);
        // Add the property to request object
        request.addProperty(id_reviewP);
        request.addProperty(id_usuarioP);
        request.addProperty(cuerpoP);
        // Create envelope
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

        try {
            // Invoke web service
            androidHttpTransport.call(SOAP_ACTION + webMethName, envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            // Assign it to resTxt variable static variable
            resTxt = response.toString();

        } catch (Exception e) {
            //Print error
            e.printStackTrace();
            //Assign error message to resTxt
            resTxt = "Error occured";
        }
        //Return resTxt to calling object
        return resTxt;

    }

    public static String actualizarPuntos(String usuario,String operacion) {
        String resTxt = null;
        String webMethName = "actualizarPuntos";
        // Create request
        SoapObject request = new SoapObject(NAMESPACE, webMethName);
        // Property which holds input parameters --> only hold one parameter
        PropertyInfo usuarioP = new PropertyInfo();
        PropertyInfo operacionP = new PropertyInfo();
        // Set Usuario
        usuarioP.setName("usuario");
        operacionP.setName("operacion");
        // Set Value
        usuarioP.setValue(usuario);
        operacionP.setValue(operacion);
        // Set dataType
        usuarioP.setType(String.class);
        operacionP.setType(String.class);
        // Add the property to request object
        request.addProperty(usuarioP);
        request.addProperty(operacionP);
        // Create envelope
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

        try {
            // Invoke web service
            androidHttpTransport.call(SOAP_ACTION + webMethName, envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            // Assign it to resTxt variable static variable
            resTxt = response.toString();

        } catch (Exception e) {
            //Print error
            e.printStackTrace();
            //Assign error message to resTxt
            resTxt = "Error occured";
        }
        //Return resTxt to calling object
        return resTxt;

    }

    public static String modificarComentario(String p_id_comentario,String p_cuerpo) {
        String resTxt = null;
        String webMethName = "modificarComentario";
        // Create request
        SoapObject request = new SoapObject(NAMESPACE, webMethName);
        // Property which holds input parameters --> only hold one parameter
        PropertyInfo p_id_comentarioP = new PropertyInfo();
        PropertyInfo p_cuerpoP = new PropertyInfo();
        // Set Usuario
        p_id_comentarioP.setName("p_id_comentario");
        p_cuerpoP.setName("p_cuerpo");
        // Set Value
        p_id_comentarioP.setValue(p_id_comentario);
        p_cuerpoP.setValue(p_cuerpo);
        // Set dataType
        p_id_comentarioP.setType(String.class);
        p_cuerpoP.setType(String.class);
        // Add the property to request object
        request.addProperty(p_id_comentarioP);
        request.addProperty(p_cuerpoP);
        // Create envelope
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

        try {
            // Invoke web service
            androidHttpTransport.call(SOAP_ACTION + webMethName, envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            // Assign it to resTxt variable static variable
            resTxt = response.toString();

        } catch (Exception e) {
            //Print error
            e.printStackTrace();
            //Assign error message to resTxt
            resTxt = "Error occured";
        }
        //Return resTxt to calling object
        return resTxt;

    }

    public static String actualizarPuntosPublicacion(String id_review,String rendimiento,String jugabilidad,String contenido,String controles) {
        String resTxt = null;
        String webMethName = "actualizarPuntosPublicacion";
        // Create request
        SoapObject request = new SoapObject(NAMESPACE, webMethName);
        // Property which holds input parameters --> only hold one parameter
        PropertyInfo id_reviewP = new PropertyInfo();
        PropertyInfo rendimientoP = new PropertyInfo();
        PropertyInfo jugabilidadP = new PropertyInfo();
        PropertyInfo contenidoP = new PropertyInfo();
        PropertyInfo controlesP = new PropertyInfo();
        // Set Usuario
        id_reviewP.setName("id_review");
        rendimientoP.setName("rendimiento");
        jugabilidadP.setName("jugabilidad");
        contenidoP.setName("contenido");
        controlesP.setName("controles");
        // Set Value
        id_reviewP.setValue(id_review);
        rendimientoP.setValue(rendimiento);
        jugabilidadP.setValue(jugabilidad);
        contenidoP.setValue(contenido);
        controlesP.setValue(controles);
        // Set dataType
        id_reviewP.setType(String.class);
        rendimientoP.setType(String.class);
        jugabilidadP.setType(String.class);
        contenidoP.setType(String.class);
        controlesP.setType(String.class);
        // Add the property to request object
        request.addProperty(id_reviewP);
        request.addProperty(rendimientoP);
        request.addProperty(jugabilidadP);
        request.addProperty(contenidoP);
        request.addProperty(controlesP);
        // Create envelope
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

        try {
            // Invoke web service
            androidHttpTransport.call(SOAP_ACTION + webMethName, envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            // Assign it to resTxt variable static variable
            resTxt = response.toString();

        } catch (Exception e) {
            //Print error
            e.printStackTrace();
            //Assign error message to resTxt
            resTxt = "Error occured";
        }
        //Return resTxt to calling object
        return resTxt;

    }

    public static String retornaRandom() {
        String resTxt = null;
        String webMethName = "retornaRandom";
        // Create request
        SoapObject request = new SoapObject(NAMESPACE, webMethName);
        // Create envelope
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

        try {
            // Invoke web service
            androidHttpTransport.call(SOAP_ACTION + webMethName, envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            // Assign it to resTxt variable static variable
            resTxt = response.toString();

        } catch (Exception e) {
            //Print error
            e.printStackTrace();
            //Assign error message to resTxt
            resTxt = "Error occured";
        }
        //Return resTxt to calling object
        return resTxt;

    }

    public static String retornarTop() {
        String resTxt = null;
        String webMethName = "retornarTop";
        // Create request
        SoapObject request = new SoapObject(NAMESPACE, webMethName);
        // Create envelope
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

        try {
            // Invoke web service
            androidHttpTransport.call(SOAP_ACTION + webMethName, envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            // Assign it to resTxt variable static variable
            resTxt = response.toString();

        } catch (Exception e) {
            //Print error
            e.printStackTrace();
            //Assign error message to resTxt
            resTxt = "Error occured";
        }
        //Return resTxt to calling object
        return resTxt;

    }

    public static String nuevasPublicaciones() {
        String resTxt = null;
        String webMethName = "Nuevas_Publicaciones";
        // Create request
        SoapObject request = new SoapObject(NAMESPACE, webMethName);
        // Create envelope
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

        try {
            // Invoke web service
            androidHttpTransport.call(SOAP_ACTION + webMethName, envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            // Assign it to resTxt variable static variable
            resTxt = response.toString();

        } catch (Exception e) {
            //Print error
            e.printStackTrace();
            //Assign error message to resTxt
            resTxt = "Error occured";
        }
        //Return resTxt to calling object
        return resTxt;

    }

    public static String eliminarComentario(String id_comentario) {
        String resTxt = null;
        String webMethName = "eliminarComentario";
        // Create request
        SoapObject request = new SoapObject(NAMESPACE, webMethName);
        // Property which holds input parameters --> only hold one parameter
        PropertyInfo id_comentarioP = new PropertyInfo();
        // Set Usuario
        id_comentarioP.setName("id_comentario");
        // Set Value
        id_comentarioP.setValue(id_comentario);
        // Set dataType
        id_comentarioP.setType(String.class);
        // Add the property to request object
        request.addProperty(id_comentarioP);
        // Create envelope
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        // Set output SOAP object
        envelope.setOutputSoapObject(request);
        // Create HTTP call object
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);

        try {
            // Invoke web service
            androidHttpTransport.call(SOAP_ACTION + webMethName, envelope);
            // Get the response
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            // Assign it to resTxt variable static variable
            resTxt = response.toString();

        } catch (Exception e) {
            //Print error
            e.printStackTrace();
            //Assign error message to resTxt
            resTxt = "Error occured";
        }
        //Return resTxt to calling object
        return resTxt;

    }



}
