package com.pantoasted.app;

/**
 * Created by root on 10-06-14.
 */
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.gson.JsonArray;

import decodificador.JSONDecoder;
import decodificador.staticFiles;

public class SplashScreenActivity extends Activity {

    // Set the duration of the splash screen
    private static final long SPLASH_SCREEN_DELAY = 2000;
    Bitmap[] imagenes2;
    String arrayJson2;
    Intent mainIntent;
    TimerTask task;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set portrait orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Hide title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.splash_screen);





        //Cargo los datos
        //Create instance for AsyncCallWS
        AsyncCallWS task2 = new AsyncCallWS(this);
        //Call execute
        task2.execute();

    }

    private class AsyncCallWS extends AsyncTask<String, Void, Void> {
        JsonArray arregloRandom;
        String arrayJson;
        Bitmap[] imagenes = new Bitmap[4];
        Context context;

        public AsyncCallWS(Context ct){
            context = ct;
        }

        @Override
        protected Void doInBackground(String... params) {
            //Invoke webservice in background
            //Prueba para retornar publicaciones
            try {
                arrayJson = WebService.retornarTop();
                imagenes = cargaImagenes(arrayJson);

            }catch (Exception ex){
                System.out.println(ex.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //Set response
            //tv.setText(displayText);
            //Make ProgressBar invisible
            //pg.setVisibility(View.INVISIBLE);
            //Toast.makeText(getApplicationContext(), mensajeToast, Toast.LENGTH_SHORT).show();
           try{
               imagenes2 = imagenes;
               arrayJson2 = arrayJson;

               mainIntent = new Intent(context,FrontSlices.class);
               //mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               staticFiles.imagenes = imagenes2;
               //mainIntent.putExtra("imagenes",imagenes2);
               mainIntent.putExtra("arrayJson", arrayJson2);
               //context.startActivity(new Intent(context,FrontSlices.class));
               //setResult(Activity.RESULT_OK,mainIntent);
               context.startActivity(mainIntent);
            }catch(Exception ex){
                System.out.println(ex.getMessage());
            }finally{
               SplashScreenActivity.this.finish();
           }
        }

        @Override
        protected void onPreExecute() {
            //Make ProgressBar invisible
            //pg.setVisibility(View.VISIBLE);
            //Hide keyboard
            //ocultarTeclado();
        }


        @Override
        protected void onProgressUpdate(Void... values) {
        }



    }
    public Bitmap[] cargaImagenes(String arrayJson){
        JsonArray insideArreglo = JSONDecoder.retornoArray(arrayJson);
        Bitmap[] x2 = new Bitmap[10];

        try {
            Bitmap x = null;
            for (int i = 0; i < insideArreglo.size(); i++) {
                String url = JSONDecoder.decodificar(insideArreglo.get(i), "url_header");
                InputStream input = null;
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bmOptions.inSampleSize = 1;

                URLConnection connection = new URL(url).openConnection();
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestMethod("GET");

                httpConnection.connect();
                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    input = httpConnection.getInputStream();
                }

                x = BitmapFactory.decodeStream(input, null, bmOptions);
                input.close();

                x2[i] = x;
            }
        }
        catch(Exception ex){

        }

        return x2;
    }

}