package com.pantoasted.app;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.pantoasted.app.R;

import org.json.JSONArray;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import decodificador.JSONDecoder;
import decodificador.staticFiles;
import elementosPortada.SlicePortada;

public class FrontSlices extends ActionBarActivity {


     FrameLayout slice1,slice2,slice3,slice4;
     TextView titulo1,titulo2,titulo3,titulo4;
     LinkedList<TextView> arrayTV,arraySubtitulo;
     LinkedList<FrameLayout> arrayFL;
     LinearLayout ListaSlices;
    int onStartCount = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.front_slices2);

        onStartCount = 1;
        if (savedInstanceState == null) // 1st time
        {
            this.overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left);
        } else // already created so reverse animation
        {
            onStartCount = 2;
        }

        arrayTV = new LinkedList<TextView>();
        arraySubtitulo = new LinkedList<TextView>();
        arrayFL = new LinkedList<FrameLayout>();
        ListaSlices  = (LinearLayout)findViewById(R.id.listaSlices);

        //Cargo los datos
        String arrayJson;
        Bitmap[] imagenes = new Bitmap[4];

        arrayFL.add((FrameLayout) findViewById(R.id.frameLayout));
        //arrayFL.add((FrameLayout) findViewById(R.id.frameLayout2));
        //arrayFL.add((FrameLayout) findViewById(R.id.frameLayout3));
        //arrayFL.add((FrameLayout) findViewById(R.id.frameLayout4));

        arrayTV.add((TextView) findViewById(R.id.textView));
        //arrayTV.add((TextView) findViewById(R.id.textView2));
        //arrayTV.add((TextView) findViewById(R.id.textView3));
        //arrayTV.add((TextView) findViewById(R.id.textView4));

        arraySubtitulo.add((TextView) findViewById(R.id.subTitulo));
        //arraySubtitulo.add((TextView) findViewById(R.id.textView2));
        //arraySubtitulo.add((TextView) findViewById(R.id.textView3));
        //arraySubtitulo.add((TextView) findViewById(R.id.textView4));

        Random rand = new Random();
        arrayJson = getIntent().getStringExtra("arrayJson");
        JsonArray arregloRandom = JSONDecoder.retornoArray(arrayJson);
        //imagenes = ((Bitmap[]) getIntent().getSerializableExtra("imagenes"));
        imagenes = staticFiles.imagenes;
        int ran = 0;
        List nums = new LinkedList();
        //Atributos de la base de datos
        String id_rev ="";
        String subtitulo ="";
        String usuario ="";
        String titulo ="";
        try {
            int idCounter = 1000;
            int contadorFramelayout = 2;
            ViewGroup.LayoutParams parametrosTitulo = arrayFL.get(0).getChildAt(0).getLayoutParams();
            ViewGroup.LayoutParams parametrossubtitulo = arrayFL.get(0).getChildAt(1).getLayoutParams();
            if (arregloRandom.size() > 0) {
                for (int i = 0; i < arregloRandom.size(); i++) {
                     id_rev = JSONDecoder.decodificar(arregloRandom.get(i), "id_rev");
                    staticFiles.id_rev = id_rev;
                    //arrayTV.get(i).setText(JSONDecoder.decodificar(arregloRandom.get(i), "titulo"));
                    titulo = JSONDecoder.decodificar(arregloRandom.get(i), "titulo");
                    //arrayFL.get(i).setBackgroundDrawable(new BitmapDrawable(imagenes[i]));
                    subtitulo = JSONDecoder.decodificar(arregloRandom.get(i), "subtitulo");
                    usuario = JSONDecoder.decodificar(arregloRandom.get(i), "usuario");

                    ListaSlices.addView(new SlicePortada(getApplicationContext(),titulo,subtitulo,
                            imagenes[i],id_rev,idCounter,parametrosTitulo,parametrossubtitulo));
                    final int copiaContador = contadorFramelayout;
                    ListaSlices.getChildAt(contadorFramelayout).setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            //Con esto envio de la actividad de listar, a la del detalle del review
                            Intent detalleRev = new Intent(getApplicationContext(),DetalleSlice.class);
                            detalleRev.putExtra("id_rev",ListaSlices.getChildAt(copiaContador).getId()+"");
                            startActivity(detalleRev);
                        }
                    });

                    //Aumento para setear el evento y que no tome el espacio
                    contadorFramelayout+=2;

                    //Agrego un espacio
                    ViewGroup.LayoutParams space = ((Space)findViewById(R.id.spaceSlices)).getLayoutParams();
                    Space sp1 = new Space(getApplicationContext());
                    sp1.setId(idCounter+2000);
                    sp1.setLayoutParams(space);
                    ListaSlices.addView(sp1);
                    //Fin agregar espacio
                    idCounter+=5;

                }
            }
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }

        //Create instance for AsyncCallWS
        //AsyncCallWS task = new AsyncCallWS();
        //Call execute
        //task.execute();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.front_slices, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            // Start the next activity
            Intent mainIntent = new Intent().setClass(
                    FrontSlices.this, MainActivity.class);
            startActivity(mainIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart(){
        super.onStart();
        if (onStartCount > 1) {
            this.overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);

        } else if (onStartCount == 1) {
            onStartCount++;
        }
    }


    private class AsyncCallWS extends AsyncTask<String, Void, Void> {


        JsonArray arregloRandom;
        String arrayJson;
        Bitmap[] imagenes = new Bitmap[4];
        @Override
        protected Void doInBackground(String... params) {
            //Invoke webservice in background
            //Prueba para retornar publicaciones
            try {
                arrayJson = getIntent().getStringExtra("arrayJson");
                imagenes = ((Bitmap[]) getIntent().getSerializableExtra("imagenes"));

            }catch (Exception ex){
                System.out.println(ex.getMessage());
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //Set response
            //tv.setText(displayText);
            //Make ProgressBar invisible
            //pg.setVisibility(View.INVISIBLE);
            //Toast.makeText(getApplicationContext(), mensajeToast, Toast.LENGTH_SHORT).show();



            /*arrayFL.add((FrameLayout) findViewById(R.id.frameLayout));
            arrayFL.add((FrameLayout) findViewById(R.id.frameLayout2));
            arrayFL.add((FrameLayout) findViewById(R.id.frameLayout3));
            arrayFL.add((FrameLayout) findViewById(R.id.frameLayout4));

            arrayTV.add((TextView) findViewById(R.id.textView));
            arrayTV.add((TextView) findViewById(R.id.textView2));
            arrayTV.add((TextView) findViewById(R.id.textView3));
            arrayTV.add((TextView) findViewById(R.id.textView4));

            Random rand = new Random();
            JsonArray arregloRandom = JSONDecoder.retornoArray(arrayJson);
            int ran = 0;
            List nums = new LinkedList();
            try {
                if (arregloRandom.size() > 0) {
                    for (int i = 0; i < 2; i++) {
                        String id_rev = JSONDecoder.decodificar(arregloRandom.get(i), "id_rev");
                        arrayTV.get(i).setText(JSONDecoder.decodificar(arregloRandom.get(i), "titulo"));
                        arrayFL.get(i).setBackgroundDrawable(new BitmapDrawable(imagenes[i]));
                        String subtitulo = JSONDecoder.decodificar(arregloRandom.get(ran), "subtitulo");
                        String usuario = JSONDecoder.decodificar(arregloRandom.get(ran), "usuario");
                    }
                }
            }catch(Exception ex){
                System.out.println(ex.getMessage());
            }*/
        }

        @Override
        protected void onPreExecute() {
            //Make ProgressBar invisible
            //pg.setVisibility(View.VISIBLE);
            //Hide keyboard
            //ocultarTeclado();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }



    }

    public Bitmap[] cargaImagenes(String arrayJson){
        JsonArray insideArreglo = JSONDecoder.retornoArray(arrayJson);
        Bitmap[] x2 = new Bitmap[4];

        try {
            Bitmap x = null;
            for (int i = 0; i < insideArreglo.size(); i++) {
                String url = JSONDecoder.decodificar(insideArreglo.get(i), "url_header");
                InputStream input = null;
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                bmOptions.inSampleSize = 1;

                URLConnection connection = new URL(url).openConnection();
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestMethod("GET");

                httpConnection.connect();
                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    input = httpConnection.getInputStream();
                }

                x = BitmapFactory.decodeStream(input, null, bmOptions);
                input.close();
                x2[i] = x;
            }
        }
        catch(Exception ex){

            }

        return x2;
    }


}
