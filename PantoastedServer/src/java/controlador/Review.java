/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author EduardoElías
 */
public class Review extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Review</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Review at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        String cadenaJson = controlador.Controlador.consultarSlice(request.getParameter("id_review"));

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<link href=\"bootstrapFW/css/bootstrap.css\" rel=\"stylesheet\">");
            out.println("<link href=\"bootstrapFW/css/starter-template.css\" rel=\"stylesheet\">");
            out.println("<script type=\"text/javascript\" src=\"http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js\"></script>");
            out.println("<script type=\"text/javascript\" src=\"http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js\"></script>");
            out.println("<title>Pantoasted Perfil</title>");

            out.println("<style>");

            out.println(".bg {");
            out.print("background: url('");
            out.print(controlador.Controlador.cargarReview(cadenaJson, "header"));
            out.print("') no-repeat center center;");
            out.println("position: fixed;");
            out.println("width: 100%;");
            out.println("height: 350px;");
            out.println("top:0;");
            out.println("left:0;");
            out.println("z-index: -1;");
            out.println("}");

            out.println(".jumbotron {");
            out.println("height: 350px;");
            out.println("color: white;");

            out.println("text-shadow: black 0.1em 0.1em 0.2em;");
            out.println("background:transparent;");
            out.println("}");
            out.println("</style>");

            //Codigo javascrip para copiar el id del comentario a un campo hidden para edicion de comentario
            out.println("<script type=\"text/javascript\">");
            out.print("function Prueba(a) {\n"
                    + " var field  ;\n"
                    + "  field =  document.getElementById(\"id_comentario\");\n"
                    + "field.value = a;\n"
                    + "document.getElementById(\"cuerpoComentario\").innerHTML = document.getElementById(\"cuerpo\"+a).innerHTML; \n"
                    + " $(\"#editarComentarioModal\").modal(\"show\");"
                    + "};"
                    + ""
                    + "function ModalReporte(a,tipo){\n"
                    +" var field = document.getElementById(\"id_review\").value = a;\n"
                    + "var tipo = document.getElementById(\"tipo\").value = tipo;\n"
                    + "$(\"#reportarModal\").modal(\"show\");\n"
                    + "}");
            out.println("</script>");
            out.println("</head>");
            out.println("<body>");
            controlador.Header.CargarHeader(request.getCookies(), response, request, "index");
            //Luego del header comienzo a cargar todo el contenido
            out.println("<div class=\"container\">");
            out.println("<div class=\"bg\"></div>");
            out.println("<div class=\"jumbotron\">");
            out.println("<h1>");
            out.print(controlador.Controlador.cargarReview(cadenaJson, "titulo"));
            out.println("</h1>");
            out.println("<p class=\"lead\">");
            out.println(controlador.Controlador.cargarReview(cadenaJson, "subtitulo"));
            out.println("\n by " + controlador.Controlador.cargarReview(cadenaJson, "autor"));
            out.println("</p>");
            out.println("</div>");
            out.println("<div class=\"row\">");
            out.println("<div class=\"col-md-12\">");
            out.println("<div class=\"panel panel-primary\">");

            out.println("<div class=\"panel-body\">");
            out.println("<div style=\"height: 350px\" class=\"row\">");
            out.println("<div class=\"col-md-4\">");
            out.println("<p>");
            out.print(controlador.Controlador.cargarReview(cadenaJson, "review"));
            out.println("</p>");
            out.println("</div>");
            out.println("<div class=\"col-md-4\">");
            out.print("<iframe width=\"350\" height=\"315\" src=\"//www.youtube.com/embed/");
            out.print(controlador.Controlador.cargarReview(cadenaJson, "video"));
            out.print("\" frameborder=\"0\" allowfullscreen=\"\"></iframe>");
            out.println("</div>");
            //Inicio evaluacion
            out.println("<form action=\"VotarPublicacion\" method=\"post\">");
            out.println("<div class=\"col-md-4\">\n"
                    + "        <div class=\"panel panel-primary\">\n"
                    + "          <div class=\"panel-heading\">\n"
                    + "            <h3 class=\"panel-title\">Evaluacion: "+controlador.Controlador.cargarReview(cadenaJson, "puntuacion")
                    +"</h3><a onclick=\"ModalReporte("+request.getParameter("id_review")+","+"'p'"+")\" class=\"btn btn-danger\" >Reportar</a>\n"
                    + "          </div>\n"
                    + "          <div class=\"panel-body\">\n"
                    + "            <div class=\"row\">\n"
                    + "              <div class=\"col-md-6\">\n"
                    + "                <p>\n"
                    + "                  <b>Rendimiento</b>\n"
                    + "                </p>\n"
                    + "              </div>\n"
                    + "              <div class=\"col-md-6\">\n"
                    + "                <select name=\"rendimiento\">\n" +
"                                        <option value=\"1\">1</option>\n" +
"                                        <option value=\"2\">2</option>\n" +
"                                        <option value=\"3\">3</option>\n" +
"                                        <option value=\"4\">4</option>\n" +
"                                        <option value=\"5\">5</option>\n" +
"                                        <option value=\"6\">6</option>\n" +
"                                        <option value=\"7\">7</option>\n" +
"                                        <option value=\"8\">8</option>\n" +
"                                        <option value=\"9\">9</option>\n" +
"                                        <option value=\"10\">10</option> \n"
                    + "            </select>  </div>\n"
                    + "            </div>\n"
                    + "            <div class=\"row\">\n"
                    + "              <div class=\"col-md-6\">\n"
                    + "                <p>\n"
                    + "                  <b>Jugabilidad</b>\n"
                    + "                </p>\n"
                    + "              </div>\n"
                    + "              <div class=\"col-md-6\">\n"
                    + "                <select name=\"jugabilidad\">\n" +
"                                        <option value=\"1\">1</option>\n" +
"                                        <option value=\"2\">2</option>\n" +
"                                        <option value=\"3\">3</option>\n" +
"                                        <option value=\"4\">4</option>\n" +
"                                        <option value=\"5\">5</option>\n" +
"                                        <option value=\"6\">6</option>\n" +
"                                        <option value=\"7\">7</option>\n" +
"                                        <option value=\"8\">8</option>\n" +
"                                        <option value=\"9\">9</option>\n" +
"                                        <option value=\"10\">10</option> \n"
                    + "             </select>  </div>\n"
                    + "            </div>\n"
                    + "            <div class=\"row\">\n"
                    + "              <div class=\"col-md-6\">\n"
                    + "                <p>\n"
                    + "                  <b>Contenido</b>\n"
                    + "                </p>\n"
                    + "              </div>\n"
                    + "              <div class=\"col-md-6\">\n"
                    + "                <select name=\"contenido\">\n" +
"                                        <option value=\"1\">1</option>\n" +
"                                        <option value=\"2\">2</option>\n" +
"                                        <option value=\"3\">3</option>\n" +
"                                        <option value=\"4\">4</option>\n" +
"                                        <option value=\"5\">5</option>\n" +
"                                        <option value=\"6\">6</option>\n" +
"                                        <option value=\"7\">7</option>\n" +
"                                        <option value=\"8\">8</option>\n" +
"                                        <option value=\"9\">9</option>\n" +
"                                        <option value=\"10\">10</option> \n"
                    + "             </select>  </div>\n"
                    + "            </div>\n"
                    + "            <div class=\"row\">\n"
                    + "              <div class=\"col-md-6\">\n"
                    + "                <p>\n"
                    + "                  <b>Controles</b>\n"
                    + "                </p>\n"
                    + "              </div>\n"
                    + "              <div class=\"col-md-6\">\n"
                    + "                <select name=\"controles\">\n" +
"                                        <option value=\"1\">1</option>\n" +
"                                        <option value=\"2\">2</option>\n" +
"                                        <option value=\"3\">3</option>\n" +
"                                        <option value=\"4\">4</option>\n" +
"                                        <option value=\"5\">5</option>\n" +
"                                        <option value=\"6\">6</option>\n" +
"                                        <option value=\"7\">7</option>\n" +
"                                        <option value=\"8\">8</option>\n" +
"                                        <option value=\"9\">9</option>\n" +
"                                        <option value=\"10\">10</option> \n"
                    + "           </select>    </div>\n"
                    + "            </div>\n"
                    + "            <div class=\"row\">\n"
                    + "              <div class=\"col-md-6\"></div>\n"
                    + "              <div class=\"col-md-6 text-center\">\n"
                    +"                  <input name=\"id_review\" type=\"hidden\" value=\""+request.getParameter("id_review") +"\">"
                    + "                <input type=\"submit\" name=\"votar\" value=\"Votar\" class=\"btn btn-primary\">\n"
                    + "              </div>\n"
                    + "            </div>\n"
                    + "\n"
                    + "          </div>\n"
                    + "        </div>\n"
                    + "      </div>");
            out.println("</form>");
            //termino evaluacion
            out.println("</div>");
            out.println("<h1 class=\"text-center\">Comentarios</h1>");

            //Contenedor al inicio de los comentarios para escribir un nuevo comentario
            out.println("<div class=\"panel panel-info \">");
            out.println("<div class=\"panel-heading\">");
            out.print("Nuevo comentario:");
            out.println("</div>");
            out.println("<center><form action=\"NuevoComentario\" method=\"post\"><div class=\"panel-body\">");
            out.println("<input type=\"text\" hidden name=\"review_id\" value=\"" + request.getParameter("id_review") + "\">");
            out.print("<textarea name=\"review\" class=\"form-control\" rows=\"4\" style=\"margin: 0px; width: 341px; height: 164px;\"></textarea>");
            out.println("<div class=\"row\">");

            out.println("<input type=\"submit\" class=\"btn btn-primary\" value=\"Comentar\">");

            out.println("</div>");
            out.println("</div>");
            out.println("</div></form></center>");
            controlador.Controlador.modalReportes(request, response);
            //HASTA ACA EL PANEL DEL NUEVO COMENTARIO
            out.println("<span>");

            controlador.Controlador.Comentarios(request, response, request.getParameter("id_review"));
            out.println("</span>");

            out.println("</div>");
            out.println("</div>");
            out.println("</div>");
            out.println("</div>");
            out.println("</div>");

            out.println("<script type=\"text/javascript\">var jumboHeight = $('.jumbotron').outerHeight();");
            out.println("function parallax() {");
            out.println("var scrolled = $(window).scrollTop();");
            out.println("$('.bg').css('height', (jumboHeight - scrolled) + 'px');");
            out.println("}");

            out.println("$(window).scroll(function(e) {");
            out.println("parallax();");
            out.println("});</script>");
            //Aqui termina

            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
