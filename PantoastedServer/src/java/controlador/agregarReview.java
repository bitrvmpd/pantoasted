/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author EduardoElías
 */
public class agregarReview extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet agregarReview</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet agregarReview at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        try{
        String usuario = controlador.Controlador.retornoUsuario(request.getCookies());
        String titulo = request.getParameter("titulo");
        String subtitulo = request.getParameter("subtitulo");
        String genero = request.getParameter("genero");
        String cuerpo = request.getParameter("review");
        String url_video = request.getParameter("video");
        url_video = url_video.substring((url_video.indexOf("=")+1));
        String url_header = request.getParameter("header");
        String rendimiento = request.getParameter("rendimiento");
        String jugabilidad = request.getParameter("jugabilidad");
        String contenido = request.getParameter("contenido");
        String controles = request.getParameter("controles");
        String respuesta = agregarReview(usuario,  titulo,  subtitulo,
             genero,  cuerpo,  url_video,  url_header,rendimiento,jugabilidad,contenido,controles);
        respuesta = pantoastedDB.DecodificadorJSON.decodificar(respuesta, "respuesta");
        if(respuesta.equals("true")){
            response.sendRedirect("exito.jsp");
        }else{
            response.sendRedirect("index.jsp");
        }
        }catch(Exception ex){
            response.sendRedirect("crearPublicacion.jsp");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private static String agregarReview(java.lang.String usuario, java.lang.String titulo, java.lang.String subtitulo, java.lang.String genero, java.lang.String review, java.lang.String urlVideo, java.lang.String urlHeader, java.lang.String rendimiento, java.lang.String jugabilidad, java.lang.String contenido, java.lang.String controles) {
        clienteWSStandA.PantoastedWS_Service service = new clienteWSStandA.PantoastedWS_Service();
        clienteWSStandA.PantoastedWS port = service.getPantoastedWSPort();
        return port.agregarReview(usuario, titulo, subtitulo, genero, review, urlVideo, urlHeader, rendimiento, jugabilidad, contenido, controles);
    }

}
