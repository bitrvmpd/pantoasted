/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;


import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author EduardoElías
 */
public class SubirImagen extends HttpServlet {
    private String dirUploadFiles;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SubirImagen</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SubirImagen at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try (PrintWriter out = response.getWriter()) {
            dirUploadFiles = getServletContext().getRealPath(getServletContext().getInitParameter("dirUploadFiles"));//Espesifica la ruta donde se guarda
            HttpSession sesion = request.getSession(true);
            if (ServletFileUpload.isMultipartContent(request)) {
                FileItemFactory factory = new DiskFileItemFactory();
                ServletFileUpload upload = new ServletFileUpload(factory);

                upload.setSizeMax(new Long(getServletContext().getInitParameter("maxFileSize")).longValue());
                List listUploadFiles = null;
                FileItem item = null;
                try {
                    listUploadFiles = upload.parseRequest(request);
                    Iterator it = listUploadFiles.iterator();
                    while (it.hasNext()) {
                        item = (FileItem) it.next();
                        if (!item.isFormField()) {
                            if (item.getSize() > 0) {
                                String nombre = item.getName(); //Aqui se debe editar para guardarlo con el nombre de usuario.
                                //Leer desde la cookie.
                                String tipo = item.getContentType();
                                long tamanio = item.getSize();
                                //Aca verificar que sea extension .jpg
                                String extension = nombre.substring(nombre.lastIndexOf("."));
                                if (extension.equals(".jpg") || extension.equals(".jpeg")) {
                                    nombre = controlador.Controlador.retornoUsuario(request.getCookies());
                                    File archivo = new File(dirUploadFiles, nombre + extension);
                                    item.write(archivo);
                                    if (archivo.exists()) {
                                        response.sendRedirect("editarPerfil.jsp");
                                    } else {
                                        System.out.println("ups un error al subir el archivo");
                                    }
                                } else {
                                    //Indicar que el formato del archivo no es el que corresponde
                                    System.out.println("El formato no corresponde");
                                }
                            }
                        }
                    }

                } catch (FileUploadException e) {
                    System.out.println(e.getMessage());
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
            
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
