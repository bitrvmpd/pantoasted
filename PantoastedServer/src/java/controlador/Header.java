/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ok-Computer
 */
public class Header {

    public static void CargarHeader(Cookie[] cookie, HttpServletResponse response, HttpServletRequest request, String jsp) throws IOException {
        PrintWriter out = response.getWriter();

        out.println("<style type=\"text/css\"> body {background-color: #4c4c4c !important; }</style>");
        out.println("<div class=\"navbar navbar-default navbar-inverse navbar-fixed-top\" id=\"navbar\">");

        out.println("<div class=\"container\">");
        out.println("<div class=\"navbar-header\">");
        out.println("<a href=\"index.jsp\" class=\"navbar-brand\">Pantoasted</a>");
        out.println("<button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">");
        out.println("<span class=\"sr-only\">Toggle navigation</span>");
        out.println("<span class=\"icon-bar\"></span>");
        out.println("<span class=\"icon-bar\"></span>");
        out.println("<span class=\"icon-bar\"></span>");
        out.println("</button>");
        out.println(" </div>");
        out.println("<div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">");
        out.println(" <ul class=\"nav navbar-nav navbar-right \">");

        if (cookie != null && (cookie.length > 1)) {

            for (Cookie temp : cookie) {
                if (!jsp.equals("index")) {
                    if (temp.getName().equals("Privilegio")) {
                        //controlador.Controlador.Rediccion(temp.getValue(), response, jsp);
                    }
                }

                if (temp.getName().equals("Usuario")) {

                    out.println("<li><a href=\"perfil.jsp\">" + temp.getValue() + "</a></li>");

                }
                if (temp.getName().equals("Privilegio")) {
                    if (temp.getValue().equals("mod") || temp.getValue().equals("admin")) {
                        out.println("<li><a href=\"Reportes.jsp\">Reportes</a></li>");
                    }
                }
                if (temp.getName().equals("Privilegio")) {
                    if (temp.getValue().equals("mod") || temp.getValue().equals("admin") || temp.getValue().equals("autor") ) {
                        out.println("<li><a href=\"crearPublicacion.jsp\">Nueva Publicacion</a></li>");
                    }
                }
                
                if (temp.getName().equals("Privilegio")) {
                    if (temp.getValue().equals("admin")) {
                        out.println("<li><a href=\"modUsers.jsp\">Mod Usuarios</a></li>");
                    }
                }
                

            }
            out.println("<li>");
            out.println("<form action=\"salirServlet\" method=\"post\">");
            out.println("<input class=\"btn btn-primary navbar-btn navbar-right\" type=\"submit\" value=\"Salir\" ></input>");
            out.println("</form>");
            out.println("</li>");
        } else {
            out.println("</ul>");
            out.println("<a href=\"login.jsp\" type=\"button\" class=\"btn btn-primary navbar-btn navbar-right\">Ingresar</a> ");
            out.println(" <a href=\"Registro.jsp\" type=\"button\" class=\"btn btn-primary navbar-btn navbar-right\">Registro</a>");
        }
        out.println("</div>");
        out.println("</div>");
        out.println("</div>");

    }
}
