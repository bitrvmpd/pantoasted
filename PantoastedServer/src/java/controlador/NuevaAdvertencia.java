/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author edo
 */
public class NuevaAdvertencia extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet NuevaAdvertencia</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet NuevaAdvertencia at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        //Llamar al WS encargado de hacer esto 
        if(request.getParameter("id_review")!=null && !"".equals(request.getParameter("id_review"))){
            nuevaAdvertenciaRev(request.getParameter("id_review"),request.getParameter("id"));
        }else{
            nuevaAdvertenciaCom(request.getParameter("id_com"),request.getParameter("id"));
        }
        response.sendRedirect("exito.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private static void nuevaAdvertenciaCom(java.lang.String idCom, java.lang.String idRep) {
        clienteWSStandA.PantoastedWS_Service service = new clienteWSStandA.PantoastedWS_Service();
        clienteWSStandA.PantoastedWS port = service.getPantoastedWSPort();
        port.nuevaAdvertenciaCom(idCom, idRep);
    }

    private static void nuevaAdvertenciaRev(java.lang.String idReview, java.lang.String idRep) {
        clienteWSStandA.PantoastedWS_Service service = new clienteWSStandA.PantoastedWS_Service();
        clienteWSStandA.PantoastedWS port = service.getPantoastedWSPort();
        port.nuevaAdvertenciaRev(idReview, idRep);
    }

  

}
