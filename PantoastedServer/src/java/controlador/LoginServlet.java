/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pantoastedDB.*;
import javax.servlet.http.Cookie;

/**
 *
 * @author EduardoElías
 */
public class LoginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String guardar_sesion = request.getParameter("guardar_sesion");
        String respuesta = DecodificadorJSON.decodificar(iniciarSesion(request.getParameter("usuario"),
                request.getParameter("contrasena")), "privilegio");

        if (respuesta.equals("null") || respuesta.equals("false")) {

            response.sendRedirect("login.jsp");
        } else {

            Cookie usuario = new Cookie("Usuario", request.getParameter("usuario"));

            Cookie privilegio = new Cookie("Privilegio", respuesta);
            if (guardar_sesion != null && guardar_sesion.equals("on")) {
                privilegio.setMaxAge((60 * 60 * 24) * 365);
                usuario.setMaxAge((60 * 60 * 24) * 365);
            } else if (guardar_sesion == null) {
                privilegio.setMaxAge(-1);
                usuario.setMaxAge(-1);
            }
            response.addCookie(usuario);
            response.addCookie(privilegio);
            response.sendRedirect("index.jsp");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private static String iniciarSesion(java.lang.String usuario, java.lang.String contrasena) {
        clienteWSStandA.PantoastedWS_Service service = new clienteWSStandA.PantoastedWS_Service();
        clienteWSStandA.PantoastedWS port = service.getPantoastedWSPort();
        return port.iniciarSesion(usuario, contrasena);
    }

}
