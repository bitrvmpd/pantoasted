/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Luis
 */
public class Portada {

    public static void CargarPortada(HttpServletResponse response, HttpServletRequest request) throws IOException {
        PrintWriter out = response.getWriter();

        out.println("<div class=\"container\">\n"
                + "            <div class=\"row\">\n"
                + "                <div class=\"col-md-12\">\n"
                + "                </div>\n"
                + "            </div>\n"
                + "            <div class=\"row\">\n"
                + "                <div class=\"col-md-12\">\n"
                + "\n"
                + "                </div>\n"
                + "            </div>\n"
                + "            <div class=\"row\">\n"
                + "                <div class=\"col-md-12 text-center\">\n"
                + "\n"
                + "                </div>\n"
                + "            </div>\n"
                + "            <div class=\"row\">\n"
                + "                <div class=\"col-md-12\">\n"
                + "\n"
                + "                </div>\n"
                + "            </div>\n"
                + "\n"
                + "\n"
                + "            <div class=\"row Publicaciones\" >\n");
                //Llenado del carousel
                         controlador.Controlador.cargaCarousel(request, response);
                         
               out.println(
                  "                <div class=\" col-md-3\">\n");
                     controlador.Controlador.Nuevas_publicaciones(request, response);
                out.println("            </div>\n"
                + "            <div class=\"row\">\n"
                + "\n"
                + "\n"
                + "            </div>\n"
                + "            <div class=\"row\">\n"
                + "                <div class=\"col-md-8\">\n"
                + "\n"
                + "                </div>\n"
                + "            </div>\n"
                + "            <div class=\"row\">\n"
                + "    <div class=\"col-md-8\">\n"
                + "      <div class=\"panel panel-primary\">\n"
                + "        <div class=\"panel-heading\">\n"
                + "          <h3 class=\"panel-title\">Publicaciones Random</h3>\n"
                + "        </div>\n"
                + "\n"
                + "        <div class=\"panel-body \">\n");
                          controlador.Controlador.revRandom(request, response);
                out.println(
                 "        </div>\n"
                + "      </div>\n"
                + "    </div>\n"
                + "    <div class=\"col-md-4\">\n"
                + "      <div class=\"panel panel-primary\">\n"
                + "        <div class=\"panel-heading\">Status</div>\n"
                + "        <div class=\"panel-body \">\n"
                + "\n"
                + "        </div>\n"
                + "      </div>\n"
                + "    </div>\n"
                + "  </div>\n"
                + "            <div class=\"row\">\n"
                + "                <div class=\"col-md-12\">\n"
                + "                    <hr>\n"
                + "                </div>\n"
                + "            </div>\n"
                + "            <div class=\"row\">\n"
                + "                <div class=\"col-md-12\">\n"
                + "                    <div class=\"col-md-6\">\n"
                + "                        <div class=\"thumbnail\">\n"
                + "                            <img src=\"http://placehold.it/240x240\" class=\"img-responsive\">\n"
                + "                            <div class=\"caption\"></div>\n"
                + "                        </div>\n"
                + "                    </div>\n"
                + "                    <div class=\"col-md-6\">\n"
                + "                        <div class=\"col-md-6\">\n"
                + "                            <div class=\"jumbotron\">\n"
                + "                                <h1>Hello, world!</h1>\n"
                + "                                <p>...</p>\n"
                + "                                <p><a class=\"btn btn-primary btn-large\">Learn more</a>\n"
                + "                                </p>\n"
                + "                            </div>\n"
                + "                        </div>\n"
                + "                        <div class=\"jumbotron\">\n"
                + "                            <h1>Hello, world!</h1>\n"
                + "                            <p>...</p>\n"
                + "                            <p><a class=\"btn btn-primary btn-large\">Learn more</a>\n"
                + "                            </p>\n"
                + "                        </div>\n"
                + "                    </div>\n"
                + "                    <hr>\n"
                + "                </div>\n"
                + "            </div>\n"
                + "        </div>");

    }

}
