/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.google.gson.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pantoastedDB.*;
import java.util.*;

/**
 *
 * @author Ok-Computer
 */
public class Controlador {

    public static void Rediccion(String privilegio, HttpServletResponse response, String jsp) throws IOException {
        if (privilegio.equals("normal")) {
            response.sendRedirect("index.jsp");
        }
    }

    //Retorna la cadena Json que se almacena en la pagina como variable
    public static String cargarPerfil(Cookie[] cookies) {
        //ToDo reducir las consultas al WS
        String retorno = "";
        for (Cookie var : cookies) {
            if (var.getName().equals("Usuario")) {
                retorno = cargarPerfil(var.getValue());
            }
        }
        if (retorno.equals("null") || retorno.equals("false") || retorno.equals("") || retorno == null) {
            return "false";
        }
        return retorno;
    }

    private static String cargarPerfil(java.lang.String usuario) {
        clienteWSStandA.PantoastedWS_Service service = new clienteWSStandA.PantoastedWS_Service();
        clienteWSStandA.PantoastedWS port = service.getPantoastedWSPort();
        return port.cargarPerfil(usuario);
    }

    public static String retornoUsuario(Cookie[] cookies) {
        String retorno = "";
        for (Cookie var : cookies) {
            if (var.getName().equals("Usuario")) {
                retorno = var.getValue();
            }
        }
        return retorno;
    }

    public static String cargarReview(String cadena_json, String id_json) {
        //ToDo reducir las consultas al WS //Listo!
        String retorno = "";
        retorno = DecodificadorJSON.decodificar(cadena_json, id_json);
        return retorno;
    }

    //Retorna solo la cadena json del review
    public static String consultarSlice(String idReview) {
        return consultarReview(idReview);
    }

    //Retorno el id  que quiero rescatar en el perfil
    public static String cargarPerfil(String json, String id) {
        if (!json.equals("false")) {
            return DecodificadorJSON.decodificar(json, id);
        } else {
            return "false";
        }
    }

    private static String consultarReview(java.lang.String idReview) {
        clienteWSStandA.PantoastedWS_Service service = new clienteWSStandA.PantoastedWS_Service();
        clienteWSStandA.PantoastedWS port = service.getPantoastedWSPort();
        return port.consultarReview(idReview);
    }

    public static void Comentarios(HttpServletRequest request, HttpServletResponse response, String id_review) {
        //Aca debo consultar el documento json y recorrerlo
        PrintWriter out;
        try {
            JsonArray comentarios = pantoastedDB.DecodificadorJSON.retornoArray(obtenerComentarios(id_review));
            String cadenaJson = controlador.Controlador.consultarSlice(request.getParameter("id_review"));
            out = response.getWriter();
            for (int i = 0; i < comentarios.size(); i++) {
                //Obteniendo el JSON ahora lo comienzo a recorrer
                //Cada comentario resulta ser un panel 
                out.println("<div class=\"panel panel-default\">");
                out.println("<div class=\"panel-body\">");
                out.println("<div class=\"row\">");
                out.println("<div class=\"col-md-2\">");
                out.println("<img src=\"" + pantoastedDB.DecodificadorJSON.decodificar(comentarios.get(i), "avatar") + "\" class=\"img-circle img-responsive\">");
                out.println("<div class=\"row\">");
                out.println("<div class=\"col-md-12 text-center\">");
                out.println("<b>");
                out.println("<p>" + pantoastedDB.DecodificadorJSON.decodificar(comentarios.get(i), "autor") + "</p>");
                out.println("</b>");
                out.println("</div>");
                out.println("</div>");
                out.println("</div>");
                out.println("<div class=\"col-md-7\">");
                out.print("<p id=\"cuerpo" + pantoastedDB.DecodificadorJSON.decodificar(comentarios.get(i), "id") + "\">" + pantoastedDB.DecodificadorJSON.decodificar(comentarios.get(i), "cuerpo") + "</p>");
                out.println("</div>");
                out.println("<div class=\"col-md-3 text-center\">");

                out.println("<div style=\"height: 40px\" class=\"row\">");
                out.println("<div class=\"col-md-12 text-center\">");
                //Muestro el boton editar solo en los comentarios del autor logueado
                String nombreUsuario = "";
                for (Cookie var : request.getCookies()) {
                    if (var.getName().equals("Usuario")) {
                        nombreUsuario = var.getValue();
                    }
                }
                if ((pantoastedDB.DecodificadorJSON.decodificar(comentarios.get(i), "autor").toLowerCase().equals(nombreUsuario.toLowerCase())) || controlador.Controlador.cargarReview(cadenaJson, "autor").toLowerCase().equals(nombreUsuario.toLowerCase())) {
                    out.println("<a onClick=\"Prueba(" + pantoastedDB.DecodificadorJSON.decodificar(comentarios.get(i), "id") + ")\" id=\"modalComentario\"class=\"btn btn-primary\" >Editar</a>");
                }
                out.println("</div>");
                out.println("</div>");
                out.println("<div style=\"height: 40px\" class=\"row\">");
                out.println("<div class=\"col-md-4 text-right\">0</div>");
                out.println("<div class=\"col-md-8 text-left\">");
                out.println("<form action=\"votarServlet\" method=\"post\">");
                out.println("<input name=\"usuario\" type=\"hidden\" value=\"" + pantoastedDB.DecodificadorJSON.decodificar(comentarios.get(i), "autor") + "\">");
                out.println("<input type=\"hidden\" value=\"+\" name=\"operacion\">");
                out.println("<input type=\"submit\" class=\"btn btn-sucess\" value=\"++\">");
                out.println("</form>");
                out.println("</div>");
                out.println("</div>");
                out.println("<div style=\"height: 40px\" class=\"row\">");
                out.println("<div class=\"col-md-4 text-right\">0</div>");
                out.println("<div class=\"col-md-8 text-left\">");
                out.println("<form action=\"votarServlet\" method=\"post\">");
                out.println("<input name=\"usuario\" type=\"hidden\" value=\"" + pantoastedDB.DecodificadorJSON.decodificar(comentarios.get(i), "autor") + "\">");
                out.println("<input type=\"hidden\" value=\"-\" name=\"operacion\">");
                out.println("<input type=\"submit\" class=\"btn btn-danger\" value=\"--\">");
                out.println("</form>");
                out.println("</div>");
                out.println("</div>");
                out.println("</div>");
                out.println("</div>");
                out.println("<div class=\"row\">");
                out.println("<div class=\"col-md-3 text-right\">");
                out.println("<button class=\"btn btn-warning\" data-toggle=\"modal\" onclick=\"ModalReporte(" + pantoastedDB.DecodificadorJSON.decodificar(comentarios.get(i), "id") + "," + "'c'" + ")\">Reportar</button>");
                out.println("</div>");
                out.println("</div>");
                out.println("</div>");
                out.println("</div>");
            }
        } catch (IOException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static String obtenerComentarios(java.lang.String idReview) {
        clienteWSStandA.PantoastedWS_Service service = new clienteWSStandA.PantoastedWS_Service();
        clienteWSStandA.PantoastedWS port = service.getPantoastedWSPort();
        return port.obtenerComentarios(idReview);
    }

    public static void modalReportes(HttpServletRequest request, HttpServletResponse response) {
        //Lugar donde se escribe la salida en html del modal
        PrintWriter out;
        try {
            out = response.getWriter();

            //Modal para edicion de comentarios (llamada a un servlet diferente)
            out.println("<div class=\"modal fade\" id=\"editarComentarioModal\">");
            out.println("<div class=\"modal-dialog\">");
            out.println("<div class=\"modal-content\">");
            out.println("<div class=\"modal-header\">");
            out.println("<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>");
            out.println("<h4 class=\"modal-title\">Editar Comentario</h4>");
            out.println("</div>");
            out.println("<div class=\"modal-body\">");
            out.println("<form method=\"post\" action=\"ModificarComentario\">");
            out.println("<textarea id=\"cuerpoComentario\" class=\"form-control\" rows=\"3\" name=\"cuerpo\"></textarea>");
            //el id del comentario se carga por codigo javascript a este campo hidden al momento de invocar modal.
            out.println("<input id=\"id_comentario\" name=\"id_comentario\" type=\"hidden\" value=\"2\">");
            out.println("</div>");
            out.println("<div class=\"modal-footer\">");
            out.println("<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>");
            out.println("<input type=\"submit\" class=\"btn btn-primary\" value=\"Guardar\">");
            out.println("</form>");
            out.println("</div>");
            out.println("</div>");
            out.println("</div>");
            out.println("</div>");

            //Modal para reporte de Review (llamada a un servlet diferente)
            out.println("<div class=\"modal fade\" id=\"reportarModal\">");
            out.println("<div class=\"modal-dialog\">");
            out.println("<div class=\"modal-content\">");
            out.println("<div class=\"modal-header\">");
            out.println("<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>");
            out.println("<h4 class=\"modal-title\">Reportar Publicacion</h4>");
            out.println("</div>");
            out.println("<div class=\"modal-body\">");
            out.println("<form method=\"post\" action=\"GenerarReporte\">");
            out.println("<textarea id=\"cuerpoReporte\" class=\"form-control\" rows=\"3\" name=\"cuerpo\"></textarea>");
            //el id del comentario se carga por codigo javascript a este campo hidden al momento de invocar modal.
            out.println("<input id=\"id_review\" name=\"id_review\" type=\"hidden\">");
            out.println("<input id=\"tipo\" name=\"tipo\" type=\"hidden\" value=\"p\">");
            out.println("</div>");
            out.println("<div class=\"modal-footer\">");
            out.println("<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>");
            out.println("<input type=\"submit\" class=\"btn btn-primary\" value=\"Guardar\">");
            out.println("</form>");
            out.println("</div>");
            out.println("</div>");
            out.println("</div>");
            out.println("</div>");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void cargarReportes(HttpServletRequest request, HttpServletResponse response) {
        //Aca debo consultar el documento json y recorrerlo
        PrintWriter out;
        try {
            out = response.getWriter();

            //Inicio JSP
            out.print("<div style=\"height: 40px\"></div>\n"
                    + "        <div class=\"container\">\n"
                    + "            <div class=\"row\">\n"
                    + "                <div class=\"col-md-6\">\n"
                    + "                    <div class=\"panel panel-default\">\n"
                    + "                        <div class=\"panel-heading\">\n"
                    + "                            <h3 class=\"panel-title\">Publicaciones</h3>\n"
                    + "                        </div>\n"
                    + "                        <div class=\"panel-body\">\n");
            //Seccion de un Reporte Publicacion
            java.util.List<java.lang.String> reportes = retornarReportes();

            JsonArray publicaciones = pantoastedDB.DecodificadorJSON.retornoArray(reportes.get(0));

            for (int i = 0; i < publicaciones.size(); i++) {
                String id_rev = pantoastedDB.DecodificadorJSON.decodificar(publicaciones.get(i), "id_rev");
                String motivo = pantoastedDB.DecodificadorJSON.decodificar(publicaciones.get(i), "motivo");
                String titulo = pantoastedDB.DecodificadorJSON.decodificar(publicaciones.get(i), "titulo");
                String id_rep = pantoastedDB.DecodificadorJSON.decodificar(publicaciones.get(i), "id");
                out.print(
                        "                            <div class=\"row\">\n"
                        + "                                    <div class=\"col-md-8 text-left\">\n"
                        + "                                    <b>" + titulo + "</b>\n"
                        + "                                </div>\n"
                        + "                                <div class=\"col-md-8 text-left\">\n"
                        + "<form action=\"Review\" method=\"post\">"
                        + "<input type=\"text\" hidden value=\"" + id_rev + "\" name=\"id_review\">"
                        + "                                            <input class=\"btn btn-group\" type=\"submit\" value=\"" + motivo + "\">"
                        + "</form>"
                        + "                                </div>\n"
                          
                                
                                +"     <div class=\"col-md-2\">\n"
                        + "                              <form action=\"Derivar\" method=\"post\">"
                                           + "<input type=\"text\" hidden value=\"" + id_rep + "\" name=\"id\">"
                                + "<input type=\"text\" hidden value=\"review\" name=\"id_procedimiento\">"
                        + "      <input type=\"submit\" class=\"btn btn-warning\" value=\"Derivar\">\n"
                        + "                              </form>  </div>\n"
                                
                                
                        + "                                <div class=\"col-md-2\">\n"
                        + "                              <form action=\"NuevaAdvertencia\" method=\"post\">"
                        + "<input type=\"text\" hidden value=\"" + id_rev + "\" name=\"id_review\">"
                                           + "<input type=\"text\" hidden value=\"" + id_rep + "\" name=\"id\">"
                        + "      <input type=\"submit\" class=\"btn btn-warning\" value=\"Advertir\">\n"
                        + "                              </form>  </div>\n"
                        + "                                <div class=\"col-md-2\">\n"
                        + "<form action=\"EliminarReview\" method=\"post\">"
                        + "<input type=\"text\" hidden value=\"" + id_rev + "\" name=\"id_review\">"
                        + "                                    <input type=\"submit\" class=\"btn btn-danger\" draggable=\"true\" value=\"Eliminar\">\n"
                        + "</form>"
                        + "                                </div>\n"
                        + "                            </div>\n");

            }

            //Fin seccion reporte Publicacion
            out.print(
                    "                        </div>\n"
                    + "                    </div>\n"
                    + "                </div>\n"
                    + "                <div class=\"col-md-6\">\n"
                    + "                    <div class=\"panel panel-default\">\n"
                    + "                        <div class=\"panel-heading\">\n"
                    + "                            <h3 class=\"panel-title\">Comentarios</h3>\n"
                    + "                        </div>\n"
                    + "                        <div class=\"panel-body\">\n");
            //Inicio seccion de reporte Comentarios
            JsonArray comentarios = pantoastedDB.DecodificadorJSON.retornoArray(reportes.get(1));

            for (int i = 0; i < comentarios.size(); i++) {
                String motivo = pantoastedDB.DecodificadorJSON.decodificar(comentarios.get(i), "motivo");
                String id_com = pantoastedDB.DecodificadorJSON.decodificar(comentarios.get(i), "id_com");
                String cuerpo = pantoastedDB.DecodificadorJSON.decodificar(comentarios.get(i), "cuerpo");
                String id_rep = pantoastedDB.DecodificadorJSON.decodificar(comentarios.get(i), "id");
                out.print(
                        "                            <div class=\"row\">\n"
                        + "                                <div class=\"col-md-6 text-left\">\n"
                        + "                                    <b>" + motivo + "</b>\n"
                        + "                                </div>\n"
                        + "                                <div class=\"col-md-2\">\n"
                        + "                                <form action=\"NuevaAdvertencia\" method=\"post\">"
                        + "<input type=\"text\" hidden value=\"" + id_com + "\" name=\"id_com\">"
                                 + "<input type=\"text\" hidden value=\"" + id_rep + "\" name=\"id\">"
                        + "      <input type=\"submit\" class=\"btn btn-warning\" value=\"Advertir\">\n"
                        + "                              </form> \n"
                        + "                                </div>\n"
                        + "       <div class=\"col-md-2\">\n"
                        + "<a onClick=\"Prueba(" + id_com + ")\" id=\"modalComentario\"class=\"btn btn-primary\" >Editar</a>"
                        + "<input type=\"text\" hidden value=\"" + cuerpo + "\" id=\"cuerpo" + id_com + "\">"
                        + "<input type=\"text\" hidden value=\"" + id_com + "\" id=\"id_comentario\">"
                        + "                                </div>\n"
                        + "                                <div class=\"col-md-2\">\n"
                        + "<form action=\"EliminarComentario\" method=\"post\">"
                        + "<input type=\"text\" hidden value=\"" + id_com + "\" name=\"id_comentario\">"
                        + "                                    <input type=\"submit\" class=\"btn btn-danger\" value=\"Eliminar\">\n"
                        + "</form>"
                        + "                                </div>\n"
                        + "                            </div>\n <br/>");
            }
            //Fin seccion de reporte Comentarios
            out.print(
                    "\n"
                    + "                        </div>\n"
                    + "                    </div>\n"
                    + "                </div>\n"
                    + "            </div>\n"
                    + "            <div class=\"row\">\n"
                    + "\n"
                    + "            </div>\n"
                    + "        </div>");

            //FIN JSP
        } catch (Exception ex) {

        }
    }

    private static java.util.List<java.lang.String> retornarReportes() {
        clienteWSStandA.PantoastedWS_Service service = new clienteWSStandA.PantoastedWS_Service();
        clienteWSStandA.PantoastedWS port = service.getPantoastedWSPort();
        return port.retornarReportes();
    }

    public static void revRandom(HttpServletRequest request, HttpServletResponse response) {
        //Aca debo consultar el documento json y recorrerlo
        PrintWriter out;
        Random rand = new Random();
        try {
            out = response.getWriter();
            JsonArray random = pantoastedDB.DecodificadorJSON.retornoArray(retornaRandom());
            int ran = 0;
            List nums = new LinkedList();
            if (random.size() > 0) {
                for (int i = 0; i < 4; i++) {

                    ran = rand.nextInt(random.size());

                    if (nums.isEmpty()) {
                        nums.add(ran);
                        String id_rev = pantoastedDB.DecodificadorJSON.decodificar(random.get(ran), "id_rev");
                        String titulo = pantoastedDB.DecodificadorJSON.decodificar(random.get(ran), "titulo");
                        String subtitulo = pantoastedDB.DecodificadorJSON.decodificar(random.get(ran), "subtitulo");
                        String usuario = pantoastedDB.DecodificadorJSON.decodificar(random.get(ran), "usuario");

                    //Obteniendo el JSON ahora lo comienzo a recorrer
                        //Cada comentario resulta ser un panel 
                        out.println("<form action=\"Review\" method=\"post\">\n"
                                + "  <div class=\"row\">\n"
                                + "  <div class=\"col-md-12\">\n"
                                + "  <input type=\"text\" hidden value=" + id_rev + " name=\"id_review\">\n"
                                + "  <input type=\"submit\" class=\"btn btn-info btn-lg\" value=\" " + titulo + " (" + subtitulo + ") - " + usuario + " \">\n"
                                + "  </div>\n"
                                + "  </div>\n"
                                + "  </form>");
                    } else {
                        if (nums.contains(ran)) {
                            i--;
                        } else {
                            nums.add(ran);
                            String id_rev = pantoastedDB.DecodificadorJSON.decodificar(random.get(ran), "id_rev");
                            String titulo = pantoastedDB.DecodificadorJSON.decodificar(random.get(ran), "titulo");
                            String subtitulo = pantoastedDB.DecodificadorJSON.decodificar(random.get(ran), "subtitulo");
                            String usuario = pantoastedDB.DecodificadorJSON.decodificar(random.get(ran), "usuario");

                        //Obteniendo el JSON ahora lo comienzo a recorrer
                            //Cada comentario resulta ser un panel 
                            out.println("<form action=\"Review\" method=\"post\">\n"
                                    + "  <div class=\"row\">\n"
                                    + "  <div class=\"col-md-12\">\n"
                                    + "  <input type=\"text\" hidden value=" + id_rev + " name=\"id_review\">\n"
                                    + "  <input type=\"submit\" class=\"btn btn-info btn-lg\" value=\" " + titulo + " (" + subtitulo + ") - " + usuario + " \">\n"
                                    + "  </div>\n"
                                    + "  </div>\n"
                                    + "  </form>");
                        }
                    }

                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static String retornaRandom() {
        clienteWSStandA.PantoastedWS_Service service = new clienteWSStandA.PantoastedWS_Service();
        clienteWSStandA.PantoastedWS port = service.getPantoastedWSPort();
        return port.retornaRandom();
    }

    //Carga del carousel
    public static void cargaCarousel(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();
            JsonArray topReviews = pantoastedDB.DecodificadorJSON.retornoArray(retornarTop());

            //Realizo la carga de la publicacion
            String id_rev = pantoastedDB.DecodificadorJSON.decodificar(topReviews.get(0), "id_rev");
            String titulo = pantoastedDB.DecodificadorJSON.decodificar(topReviews.get(0), "titulo");
            String subtitulo = pantoastedDB.DecodificadorJSON.decodificar(topReviews.get(0), "subtitulo");
            String url_header = pantoastedDB.DecodificadorJSON.decodificar(topReviews.get(0), "url_header");
            if (topReviews.size() > 0) {
                out.println(
                        "                <div class=\" col-md-9\">\n"
                        + "                    <div id=\"carousel-example-generic\" class=\"carousel slide carousel-fit\"  data-ride=\"carousel\" data-interval=\"5000\" >\n"
                        + "                        <div class=\"carousel-inner\">\n"
                        + "                            <a class=\"item active\" onclick=\"document.getElementById('frm" + id_rev + "').submit()\">\n"
                        + "                            <form action=\"Review\" method=\"post\" id=\"frm" + id_rev + "\">"
                        + "                                <input type=\"text\" hidden value=" + id_rev + " name=\"id_review\">\n"
                        + "                                <img class=\"img-responsive \"  src=\"" + url_header + "\">\n"
                        + "                                <div class=\"carousel-caption\">\n"
                        + "                                    <h2 style=\"text-shadow: black 0.1em 0.1em 0.2em;\">" + titulo + "</h2>\n"
                        + "                                    <p style=\"text-shadow: black 0.1em 0.1em 0.2em;\">" + subtitulo + "</p>\n"
                        + "                                </div>\n"
                        + "</form>"
                        + "                            </a>\n"
                );
                for (int i = 1; i < topReviews.size(); i++) {
                    id_rev = pantoastedDB.DecodificadorJSON.decodificar(topReviews.get(i), "id_rev");
                    titulo = pantoastedDB.DecodificadorJSON.decodificar(topReviews.get(i), "titulo");
                    subtitulo = pantoastedDB.DecodificadorJSON.decodificar(topReviews.get(i), "subtitulo");
                    url_header = pantoastedDB.DecodificadorJSON.decodificar(topReviews.get(i), "url_header");

                    out.println(
                            "                            <a class=\"item\" onclick=\"document.getElementById('frm" + id_rev + "').submit()\">\n"
                            + "                           <form action=\"Review\" method=\"post\" id=\"frm" + id_rev + "\">"
                            + "                                <input type=\"text\" hidden value=" + id_rev + " name=\"id_review\">\n"
                            + "                                <img  class=\"img-responsive \"  src=\"" + url_header + "\">\n"
                            + "                                <div class=\"carousel-caption\">\n"
                            + "                                    <h2 style=\"text-shadow: black 0.1em 0.1em 0.2em;\">" + titulo + "</h2>\n"
                            + "                                    <p style=\"text-shadow: black 0.1em 0.1em 0.2em;\">" + subtitulo + "</p>\n"
                            + "                                </div>\n"
                            + "</form>"
                            + "                            </a>\n"
                    );

                }

                //Termino el carousel
                out.println(
                        "                        </div>\n"
                        + "\n"
                        + "                        <a class=\"left carousel-control\" href=\"#carousel-example-generic\" data-slide=\"prev\">\n"
                        + "                            <span class=\"glyphicon glyphicon-chevron-left\"></span>\n"
                        + "                        </a>\n"
                        + "                        <a class=\"right carousel-control\" href=\"#carousel-example-generic\" data-slide=\"next\">\n"
                        + "                            <span class=\"glyphicon glyphicon-chevron-right\"></span>\n"
                        + "                        </a>\n"
                        + "                    </div>\n"
                        + "                </div>\n");
            }
        } catch (Exception ex) {

        }
    }

    private static String retornarTop() {
        clienteWSStandA.PantoastedWS_Service service = new clienteWSStandA.PantoastedWS_Service();
        clienteWSStandA.PantoastedWS port = service.getPantoastedWSPort();
        return port.retornarTop();
    }

    public static void Nuevas_publicaciones(HttpServletRequest request, HttpServletResponse response) {
        String id;
        String titulo;
        try {
            JsonArray nuevas = pantoastedDB.DecodificadorJSON.retornoArray(nuevasPublicaciones());
            PrintWriter out = response.getWriter();

            out.print("<div class=\" panel panel-primary \">\n"
                    + "                        <div class=\"panel-heading\">Publicaciones</div>\n"
                    + "                        <div class=\"panel-body \">\n"
                    + "                            \n"
                    + "                            <table class=\"table\">\n");
            for (int i = 0; i < nuevas.size(); i++) {
                id = pantoastedDB.DecodificadorJSON.decodificar(nuevas.get(i), "id_rev");
                titulo = pantoastedDB.DecodificadorJSON.decodificar(nuevas.get(i), "titulo");

                out.print("<tr>\n"
                        + " <td>\n"
                        + "    <form action=\"Review\" method=\"post\">\n"
                        + "    <input type=\"text\" hidden value=\"" + id + "\" name=\"id_review\">\n"
                        + "    <input class=\"btn btn-group\" type=\"submit\" value=\"" + titulo + "\">\n"
                        + "    </form>\n"
                        + " </td>\n"
                        + "</tr>\n");
            }

            out.print("\n"
                    + "\n"
                    + "                            </table>\n"
                    + "                            \n"
                    + "                            \n"
                    + "                        </div>\n"
                    + "                    </div>\n"
                    + "                </div>\n");
        } catch (Exception e) {

        }
    }

    private static String nuevasPublicaciones() {
        clienteWSStandA.PantoastedWS_Service service = new clienteWSStandA.PantoastedWS_Service();
        clienteWSStandA.PantoastedWS port = service.getPantoastedWSPort();
        return port.nuevasPublicaciones();
    }
    
    
    
     public static void carga_baneados(HttpServletRequest request, HttpServletResponse response) {
         try {
            PrintWriter out = response.getWriter();
            JsonArray baneados = pantoastedDB.DecodificadorJSON.retornoArray(cargaBaneados());
out.print("<div style=\"height: 40px\" ></div>\n" +
"        <div class=\"container\" style=\"width: 700px;\">\n" +
"            <div class=\"row\">\n" +
"                <div class=\"col-md-12\">\n" +
"                    <div class=\"panel panel-danger\">\n" +
"                        <div class=\"panel-heading\">\n" +
"                            <h3 class=\"panel-title\">Moderar Usuarios</h3>\n" +
"                        </div>\n" +
"                        <div class=\"panel-body\">");
            //Realizo la carga de la publicacion
                for (int i = 0; i < baneados.size(); i++) {
                    String nom_usuario = pantoastedDB.DecodificadorJSON.decodificar(baneados.get(i), "nom_usuario");
                    out.print(" <div class=\"row\">\n" +
"                                <div class=\"col-md-2 text-left\">\n" +
"                                    <a class=\"btn btn-primary\">"+nom_usuario+"</a>\n" +
"                                </div>\n" +
"                                <div class=\"col-md-2\">\n" +
                            "<form action=\"baneoDefinido\" method=\"post\">"+
"                                    <input type=\"radio\" id=\"definido\" onclick=\"radiobutton()\" name=\"ban\" value=\"definido\">Definido\n" +
"                                    <br>\n" +
"                                    <input type=\"radio\" name=\"ban\" onclick=\"radiobutton()\" value=\"indefinido\">Indefinido\n" +
"                                </div>\n" +
"                                <div class=\"col-md-4\">\n" +
"                                    <input type=\"text\" id=\"texto\" name=\"fecha\" placeholder=\"ej: 12/06/14\">\n" +
                            "<input type=\"text\" hidden value=\""+nom_usuario+"\" name=\"nom_usuario\">"+
"                                </div>\n" +
                           
"                                <div class=\"col-md-2\">\n" +
"                                    <input type=\"submit\" class=\"btn btn-warning\" value=\"Bloquear\">\n" +
"                                </div>\n" +
                             "</form>"+
                            "<form action=\"baneoIndefinido\" method=\"post\">"+
"                                <div class=\"col-md-2\">\n" +
                             "<input type=\"text\" hidden value=\""+nom_usuario+"\" name=\"nom_usuario\">"+
"                                    <input type=\"submit\" class=\"btn btn-danger\" value=\"Eliminar\">\n" +
"                                </div>\n" +
"                            </div></form>"
                    );
                }
                
                out.print("</div>\n" +
"                    </div>\n" +
"                </div>\n" +
"            </div>\n" +
"        </div>");
            
        } catch (Exception ex) {

        }
     }

    private static String cargaBaneados() {
        clienteWSStandA.PantoastedWS_Service service = new clienteWSStandA.PantoastedWS_Service();
        clienteWSStandA.PantoastedWS port = service.getPantoastedWSPort();
        return port.cargaBaneados();
    }
}
