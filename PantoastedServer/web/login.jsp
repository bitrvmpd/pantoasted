<%-- 
    Document   : Pantoasted_login
    Created on : 09-04-2014, 10:13:18 AM
    Author     : Jean Labra
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="bootstrapFW/css/bootstrap.css" rel="stylesheet">
        <link href="bootstrapFW/css/starter-template.css" rel="stylesheet">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
        <title>Pantoasted Login</title>
    </head>
    <body >
        <% //Header
            controlador.Header.CargarHeader(request.getCookies(), response, request, "index");%>
        <style type="text/css">
            body {
                background-color: #4c4c4c !important; }

            @keyframes animacion /* Safari and Chrome */{
                from   {opacity:0.0; position:absolute; left:100px;}
                to {opacity:1.0; position:absolute; left:500px;}
            }

            @-webkit-keyframes animacion /* Safari and Chrome */{
                from   {opacity:0.0; position:absolute; left:100px;}
                to {opacity:1.0; position:absolute; left:500px;}
            }

            .aparecer{
                animation:animacion 1s;
                -webkit-animation:animacion 1s;
            }
        </style>

        
        <div style="aparecer">
        <div style="width:300px;" class="container ">
            <div class="panel panel-primary ">
                <div class="panel-heading">Ingreso </div>
                <div class="panel-body ">
                    <form action="LoginServlet" method="post">
                        <div  class="input-group">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-user"> </span></span><input name="usuario" type="text" class="form-control" placeholder="Usuario">
                        </div>
                        <div style="height: 10px"></div>
                        <div class="input-group">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-lock"> </span></span><input name="contrasena" type="password" class="form-control" placeholder="Contraseña" >
                        </div>
                        <div style="height: 10px"></div>
                        <div >
                            <input name="guardar_sesion" type="checkbox" class="navbar-left">&nbsp;No cerrar sesion</input>
                            <input type="submit" class="btn btn-primary navbar-right" value="Ingresar" >
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>

    </body>
</html>
