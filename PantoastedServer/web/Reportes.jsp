<%-- 
    Document   : Reportes
    Created on : 15-04-2014, 10:54:07 PM
    Author     : Ok-Computers
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Reportes</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="bootstrapFW/css/bootstrap.css" rel="stylesheet">
        <link href="bootstrapFW/css/starter-template.css" rel="stylesheet">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
        <!--Codigo javascrip para copiar el id del comentario a un campo hidden para edicion de comentario-->
        <% out.println("<script type=\"text/javascript\">");
            out.print("function Prueba(a) {\n"
                    + " document.getElementById(\"id_comentario2\").value = a;\n"
                    + "document.getElementById(\"cuerpoComentario\").innerHTML = document.getElementById(\"cuerpo\"+a).value; \n"
                    + " $(\"#editarComentarioModal\").modal(\"show\");"
                    + "};"
                    + ""
                    + "function ModalReporte(a,tipo){\n"
                    + " var field = document.getElementById(\"id_review\").value = a;\n"
                    + "var tipo = document.getElementById(\"tipo\").value = tipo;\n"
                    + "$(\"#reportarModal\").modal(\"show\");\n"
                    + "}");
            out.println("</script>");

            out.println("<div class=\"modal fade\" id=\"editarComentarioModal\">");
            out.println("<div class=\"modal-dialog\">");
            out.println("<div class=\"modal-content\">");
            out.println("<div class=\"modal-header\">");
            out.println("<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>");
            out.println("<h4 class=\"modal-title\">Editar Comentario</h4>");
            out.println("</div>");
            out.println("<div class=\"modal-body\">");
            out.println("<form method=\"post\" action=\"ModificarComentario\">");
            out.println("<textarea id=\"cuerpoComentario\" class=\"form-control\" rows=\"3\" name=\"cuerpo\"></textarea>");
            out.println("<input id=\"id_comentario2\" name=\"id_comentario2\" type=\"hidden\" value=\"185\">");
            out.println("<div class=\"modal-footer\">");
            out.println("<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancelar</button>");
            out.println("<input type=\"submit\" class=\"btn btn-primary\" value=\"Guardar\">");
            out.println("</form>");
            out.println("</div>");%>
    </head>
    <body>
        <% //Header
            controlador.Header.CargarHeader(request.getCookies(), response, request, "index");%>
        <% controlador.Controlador.cargarReportes(request, response);%>
    </body>
</html>
