<%-- 
    Document   : review
    Created on : 17-04-2014, 17:38:29 PM
    Author     : LuisOrostizaga
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <%String cadenaJson = controlador.Controlador.consultarSlice(request.getParameter("id_review")); %>
        <link href="bootstrapFW/css/bootstrap.css" rel="stylesheet">
        <link href="bootstrapFW/css/starter-template.css" rel="stylesheet">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
        <title>Pantoasted Perfil</title>

        <style>
            
            .bg {
                background: url('<%out.print(controlador.Controlador.cargarReview(cadenaJson, "header"));%>') no-repeat center center;
                position: fixed;
                width: 100%;
                height: 350px; /*same height as jumbotron */
                top:0;
                left:0;
                z-index: -1;
                /*test*/
            }

            .jumbotron {
                height: 350px;
                color: white;
                /*text-shadow: #444 0 1px 1px;*/
                text-shadow: black 0.1em 0.1em 0.2em;
                background:transparent;
            }
        </style>
    </head>

    <body>
        <%
            //Header
            controlador.Header.CargarHeader(request.getCookies(), response, request, "index");
        %>
        <div style="height: 40px"></div>
        <div class="container">
            <div class="bg"></div>
                            <div class="jumbotron">
                                <h1><%out.print(controlador.Controlador.cargarReview(cadenaJson, "titulo"));%></h1>
                                <p class="lead"><%out.println(controlador.Controlador.cargarReview(cadenaJson, "subtitulo"));%>
                                <%out.println("\n by "+controlador.Controlador.cargarReview(cadenaJson, "autor"));%></p>
                            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">

                        <div class="panel-body">
                            <div style="height: 350px" class="row">
                                <div class="col-md-4">
                                    <p><%out.print(controlador.Controlador.cargarReview(cadenaJson, "review"));%></p>
                                </div>
                                <div class="col-md-4">
                                    <iframe width="350" height="315" src="http://www.youtube.com/embed/<%out.print(controlador.Controlador.cargarReview(cadenaJson, "video"));%>" frameborder="0" allowfullscreen=""></iframe>
                                </div>

                            </div>
                            <h1 class="text-center">Comentarios</h1>
                            
                           
                                <span>
                                    
                            <% controlador.Controlador.Comentarios(request, response, request.getParameter("id_review")); %>
                                </span>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">var jumboHeight = $('.jumbotron').outerHeight();
            function parallax() {
                var scrolled = $(window).scrollTop();
                $('.bg').css('height', (jumboHeight - scrolled) + 'px');
            }

            $(window).scroll(function(e) {
                parallax();
            });</script>
    </body>
</html>
