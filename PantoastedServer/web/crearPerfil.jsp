<%-- 
    Document   : perfil
    Created on : 11-04-2014, 22:12:29 PM
    Author     : LuisOrostizaga
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <link href="bootstrapFW/css/bootstrap.css" rel="stylesheet">
        <link href="bootstrapFW/css/starter-template.css" rel="stylesheet">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
        <title>Pantoasted Perfil</title>
    </head>

    <body>
       
        <% //Header
            controlador.Header.CargarHeader(request.getCookies(), response, request, "index");%>
        <style type="text/css">
            body {
                background-color: #4c4c4c !important; }
            </style>

            <div style="height: 20px"></div>
        <div class="container ">
            <div class="panel panel-primary">
                <div class="panel-heading">Nuevo Perfil</div>
                <div class="panel-body ">

                    <form action="CrearPerfil"
                          method="post"
                          >
                        <div class="col-md-6">
                            <div class="page-header">
                                <h1>Usuario
                                    <small><%
            //Hacer la redireccion al modificar perfil en caso de que no exista.
out.print(controlador.Controlador.retornoUsuario(request.getCookies()));%></small>
                                </h1>
                            </div>
                            <div class="page-header">
                                <h1>Email
                                    <small><input name="correo" type="text" class="form-control" placeholder="myemail@email.com"></small>
                                </h1>
                            </div>
                            <div class="page-header">
                                <h1>Fecha de nacimiento
                                    <small><input name="nacimiento" type="text" class="form-control" placeholder="31/12/99" ></small>
                                </h1>
                            </div>
                            <div class="page-header">
                                <h1>Sexo
                                    <small><select name="sexo">
                                            <option value="m" selected>Masculino</option>
                                                    <option value="f">Femenino</option>
                                                    <option value="o">Otro</option>
                                        </select></small>
                                </h1>
                            </div>
                        </div>
                        <input type="submit" value="Guardar">
                    </form>
                    <div style="height: 10px"></div>
                    <div class="col-md-6">
                         <form action="SubirImagen"
                          method="post"
                          enctype="multipart/form-data">
                        <div style="height: 10px"></div>
                        <img src="" class="img-responsive">
                        <div style="height: 10px"></div>
                        <div style="height: 10px"></div>

                        <div  class="input-group">

                            <span> <input name="usuario" type="file" class="form-control" placeholder="url-avatar"> 

                            </span>
                        </div>
                        <div style="height: 10px"></div>
                        <input type="submit" class="btn btn-primary navbar-right" value="Subir">
                         </form>
                    </div>


                </div>
            </div>
        </div>

    </body>
</html>