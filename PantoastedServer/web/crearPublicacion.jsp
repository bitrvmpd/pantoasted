<%-- 
    Document   : crearPublicacion
    Created on : 11-04-2014, 00:18:29 PM
    Author     : LuisOrostizaga
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <link href="bootstrapFW/css/bootstrap.css" rel="stylesheet">
        <link href="bootstrapFW/css/starter-template.css" rel="stylesheet">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
        <title>Pantoasted Perfil</title>
    </head>

    <body>
        <%
            //Header
            controlador.Header.CargarHeader(request.getCookies(), response, request, "index");
        %>
        <div style="height: 40px"></div>
        
        <div style="width:700px;" class="container">
            <form action="agregarReview" method="post">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Crear Publicacion</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-2">
                                    <b>Titulo</b>
                                </div>
                                <div class="col-md-10">
                                    <div class="input-group">
                                        <input name="titulo" type="text" class="form-control">
                                    </div>
                                </div>
                                <div style="height: 40px"></div>
                                <div class="col-md-2">
                                    Subtitulo
                                </div>
                                <div class="col-md-10">
                                    <div class="input-group">
                                        <input name="subtitulo" type="text" class="form-control">
                                    </div>
                                </div>
                                <div style="height: 40px"></div>
                                <div class="col-md-2">
                                    Genero
                                </div>
                                <div class="col-md-10">
                                    <select name="genero">
                                        <option value="20">RPG</option>
                                        <option value="21">MMORPG</option>
                                        <option value="22">FPS</option>
                                        <option value="23">Accion</option>
                                        <option value="24">Puzzle</option>
                                        <option value="25">Shooter</option>
                                        <option value="26">Multiplayer</option>
                                        <option value="27">Survival</option>
                                        <option value="28">Aventura</option>
                                        <option value="29">Arcade</option>
                                        <option value="30">Estrategia</option>
                                        <option value="31">Musical</option>
                                        <option value="32">Indie</option>
                                        <option value="33">Casual</option>
                                        <option value="34">Carreras</option>
                                        <option value="35">Simuladores</option>
                                        <option value="36">Deportes</option>
                                    </select>
                                </div>
                                <div style="height: 30px"></div>
                                <div class="col-md-2">
                                    Reseña
                                </div>
                                <div class="col-md-10">
                                    <div class="input-group">
                                        <textarea name="review" class="form-control" rows="4" style="margin: 0px; width: 341px; height: 164px;"></textarea>
                                    </div>
                                </div>
                                <div style="height: 170px"></div>
                                <div class="col-md-2">
                                    Url-Video
                                </div>
                                <div class="col-md-10">
                                    <div class="input-group">
                                        <input name="video" type="text" class="form-control">
                                    </div>
                                </div>
                                <div style="height: 40px"></div>
                                <div class="col-md-2">
                                    Url-Header
                                </div>
                                <div class="col-md-10">
                                    <div class="input-group">
                                        <input name="header" type="text" class="form-control">
                                    </div>
                                </div>
                                </br>
                                <div class="col-md-2">
                                    Rendimiento
                                </div>
                                <div class="col-md-10">
                                    <div class="input-group">
                                        <select name="rendimiento">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                    </select>
                                    </div>
                                </div>
                                </br>
                                <div class="col-md-2">
                                    Jugabilidad
                                </div>
                                <div class="col-md-10">
                                    <div class="input-group">
                                        <select name="jugabilidad">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                    </select>
                                    </div>
                                </div>
                                </br>
                                <div class="col-md-2">
                                    Contenido
                                </div>
                                <div class="col-md-10">
                                    <div class="input-group">
                                        <select name="contenido">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                    </select>
                                    </div>
                                </div>
                                </br>
                                <div class="col-md-2">
                                    Controles
                                </div>
                                <div class="col-md-10">
                                    <div class="input-group">
                                        <select name="controles">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                    </select>
                                    </div>
                                </div>
                                <div style="height: 50px"></div>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-primary">Publicar</button>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </body>

</html>