<%-- 
    Document   : perfil
    Created on : 11-04-2014, 22:12:29 PM
    Author     : LuisOrostizaga
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <link href="bootstrapFW/css/bootstrap.css" rel="stylesheet">
        <link href="bootstrapFW/css/starter-template.css" rel="stylesheet">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
        <title>Pantoasted Perfil</title>
    </head>

    <body>
       <% //Header
            controlador.Header.CargarHeader(request.getCookies(), response, request, "index");%>
        <%String cadenaJson = controlador.Controlador.cargarPerfil(request.getCookies());
        if(cadenaJson.equals("false")){
            response.sendRedirect("crearPerfil.jsp");
        }
%>
        <style type="text/css">
            body {
                background-color: #4c4c4c !important; }
            </style>

            <div style="height: 20px"></div>
        <div class="container ">
            <div class="panel panel-primary">
                <div class="panel-heading">Perfil</div>
                <div class="panel-body ">


                    <div class="col-md-6">
                        <div class="page-header">
                            <h1>Usuario
                                <small><%out.print(controlador.Controlador.retornoUsuario(request.getCookies()));%></small>
                            </h1>
                        </div>
                            <div class="page-header">
                            <h1>Puntuacion
                                <small><% out.print(controlador.Controlador.cargarPerfil(cadenaJson,"puntos")); %></small>
                            </h1>
                        </div>
                            <div class="page-header">
                            <h1>Advertencias
                                <small><% out.print(controlador.Controlador.cargarPerfil(cadenaJson,"advertencias")); %></small>
                            </h1>
                        </div>
                        <div class="page-header">
                            <h1>Email
                                <small><% out.print(controlador.Controlador.cargarPerfil(cadenaJson,"correo_e")); %></small>
                            </h1>
                        </div>
                        <div class="page-header">
                            <h1>Fecha de nacimiento
                                <small><% out.print(controlador.Controlador.cargarPerfil(cadenaJson,"fecha_nac")); %></small>
                            </h1>
                        </div>
                        <div class="page-header">
                            <h1>Sexo
                                <small>
                                    <%
                                           String sexo = controlador.Controlador.cargarPerfil(cadenaJson,"sexo");
                                           char[] sexoChar = sexo.trim().toCharArray();
                                            if(sexoChar[0]=='m')
                                            {
                                                out.print("Masculino");
                                            }else if(sexoChar[0]=='f')
                                            {
                                                out.print("Femenino");
                                            }else
                                            {
                                                out.print("Otro");
                                            }
                                    %></small>
                            </h1>
                        </div>
                    </div>
                    <div style="height: 10px"></div>
                    <div class="col-md-6">
                        <div style="height: 10px"></div>
                        <img src="public/img/avatar/<%out.print(controlador.Controlador.retornoUsuario(request.getCookies()));%>.jpg" class="img-responsive">
                        <div style="height: 10px"></div>
                        <div style="height: 10px"></div>
                        <div style="height: 10px"></div>
                        <a href="editarPerfil.jsp" class="btn btn-primary navbar-right" >Editar<a/>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>