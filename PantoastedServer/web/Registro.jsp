<%-- 
    Document   : Registro
    Created on : 10-04-2014, 09:38:16 PM
    Author     : EduardoElías
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="bootstrapFW/css/bootstrap.css" rel="stylesheet">
        <link href="bootstrapFW/css/starter-template.css" rel="stylesheet">
        <style type="text/css">
            body {
                background-color: #4c4c4c !important; }
            .formRegistro{
                padding-top: 10%;
                max-height: 500px;
                max-width: 500px;
                   
            }
            </style>
            <title>Pantoasted</title>
        </head>
        <body>
            <% //Header
            controlador.Header.CargarHeader(request.getCookies(), response, request, "index");%>
            
        <div class="container formRegistro">
            <div id="registro" class ="panel panel-primary">
                <div class = "panel-heading">
                    Registro Nuevo Usuario
                </div>
                <div class="panel-body">
                    <form action="Registrar" method="post">
                    <ul class="nav nav-pills nav-stacked">
                        <li>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                <input name="usuario" type="text" class="form-control" placeholder="Nombre de Usuario">
                            </div>
                            <br/>
                        </li>
                        <li>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                <input name="contrasena" type="password" class="form-control" placeholder="Contraseña">
                            </div>
                        </li>
                        <li>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                <input type="password" class="form-control" placeholder="Confirmar Contraseña">
                            </div>
                            <br/>
                            <br/>
                        </li>
                        
                           <input type="submit" class="btn btn-primary navbar-right " value="Registrar"></input> 
                    </ul>
                        </form>
                </div>
            </div>
        </div>
            
    </body>
</html>
