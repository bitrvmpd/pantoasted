--------------------------------------------------------
-- Archivo creado  - viernes-mayo-02-2014   
--------------------------------------------------------
DROP TABLE "PANTOASTEDDB"."COMENTARIO" cascade constraints;
DROP TABLE "PANTOASTEDDB"."EVALUACION" cascade constraints;
DROP TABLE "PANTOASTEDDB"."GENERO" cascade constraints;
DROP TABLE "PANTOASTEDDB"."PERFIL" cascade constraints;
DROP TABLE "PANTOASTEDDB"."RELATION_8" cascade constraints;
DROP TABLE "PANTOASTEDDB"."REPORTE" cascade constraints;
DROP TABLE "PANTOASTEDDB"."REVIEW" cascade constraints;
DROP TABLE "PANTOASTEDDB"."USUARIO" cascade constraints;
DROP SEQUENCE "PANTOASTEDDB"."SEC_ID_COMEN";
DROP SEQUENCE "PANTOASTEDDB"."SEC_ID_EVAL";
DROP SEQUENCE "PANTOASTEDDB"."SEC_ID_GENERO";
DROP SEQUENCE "PANTOASTEDDB"."SEC_ID_REPORTE";
DROP SEQUENCE "PANTOASTEDDB"."SEC_ID_REV";
DROP SEQUENCE "PANTOASTEDDB"."SEC_ID_USUARIO";
DROP PACKAGE "PANTOASTEDDB"."PANTOASTEDFUNC";
DROP PACKAGE "PANTOASTEDDB"."TIPOS";
DROP PACKAGE BODY "PANTOASTEDDB"."PANTOASTEDFUNC";
--------------------------------------------------------
--  DDL for Sequence SEC_ID_COMEN
--------------------------------------------------------

   CREATE SEQUENCE  "PANTOASTEDDB"."SEC_ID_COMEN"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 140 CACHE 20 ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_ID_EVAL
--------------------------------------------------------

   CREATE SEQUENCE  "PANTOASTEDDB"."SEC_ID_EVAL"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 20 CACHE 20 ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_ID_GENERO
--------------------------------------------------------

   CREATE SEQUENCE  "PANTOASTEDDB"."SEC_ID_GENERO"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 40 CACHE 20 ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_ID_REPORTE
--------------------------------------------------------

   CREATE SEQUENCE  "PANTOASTEDDB"."SEC_ID_REPORTE"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 0 CACHE 20 ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_ID_REV
--------------------------------------------------------

   CREATE SEQUENCE  "PANTOASTEDDB"."SEC_ID_REV"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 80 CACHE 20 ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SEC_ID_USUARIO
--------------------------------------------------------

   CREATE SEQUENCE  "PANTOASTEDDB"."SEC_ID_USUARIO"  MINVALUE 0 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 80 CACHE 20 ORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table COMENTARIO
--------------------------------------------------------

  CREATE TABLE "PANTOASTEDDB"."COMENTARIO" 
   (	"ID_COMEN" NUMBER(38,0), 
	"CUERPO" VARCHAR2(400 CHAR), 
	"REVIEW_ID_REV" NUMBER(38,0), 
	"USUARIO_NOM_USUARIO" VARCHAR2(20 CHAR), 
	"REVIEW_NOM_USUARIO" VARCHAR2(20 CHAR)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table EVALUACION
--------------------------------------------------------

  CREATE TABLE "PANTOASTEDDB"."EVALUACION" 
   (	"ID_EVAL" NUMBER(38,0), 
	"RENDIMIENTO" NUMBER(3,0) DEFAULT 0, 
	"JUGABILIDAD" NUMBER(3,0) DEFAULT 0, 
	"CONTENIDO" NUMBER(3,0) DEFAULT 0, 
	"CONTROLES" NUMBER(3,0) DEFAULT 0, 
	"REVIEW_ID_REV" NUMBER(38,0), 
	"REVIEW_USUARIO_NOM_USUARIO" VARCHAR2(20 CHAR), 
	"NUM_EVALUACIONES" NUMBER(38,0) DEFAULT 0
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table GENERO
--------------------------------------------------------

  CREATE TABLE "PANTOASTEDDB"."GENERO" 
   (	"ID_GENERO" NUMBER(38,0), 
	"NOMBRE" VARCHAR2(50 CHAR)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table PERFIL
--------------------------------------------------------

  CREATE TABLE "PANTOASTEDDB"."PERFIL" 
   (	"ID_USUARIO" NUMBER(38,0), 
	"CORREO_E" VARCHAR2(50 CHAR), 
	"URL_AVATAR" VARCHAR2(200 CHAR), 
	"FECHA_NAC" DATE, 
	"SEXO" CHAR(1 CHAR), 
	"PUNTOS" NUMBER(4,0), 
	"USUARIO_NOM_USUARIO" VARCHAR2(20 CHAR)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table RELATION_8
--------------------------------------------------------

  CREATE TABLE "PANTOASTEDDB"."RELATION_8" 
   (	"REVIEW_ID_REV" NUMBER(38,0), 
	"REVIEW_USUARIO_NOM_USUARIO" VARCHAR2(20 CHAR), 
	"GENERO_ID_GENERO" NUMBER(38,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table REPORTE
--------------------------------------------------------

  CREATE TABLE "PANTOASTEDDB"."REPORTE" 
   (	"ID_REPORTE" NUMBER(38,0), 
	"MOTIVO" VARCHAR2(200 CHAR), 
	"TIPO" CHAR(1 CHAR), 
	"COMENTARIO_ID_COMEN" NUMBER(38,0), 
	"REVIEW_ID_REV" NUMBER(38,0), 
	"REVIEW_USUARIO_NOM_USUARIO" VARCHAR2(20 CHAR), 
	"COMENTARIO_USUARIO_NOM_USUARIO" VARCHAR2(20 CHAR)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table REVIEW
--------------------------------------------------------

  CREATE TABLE "PANTOASTEDDB"."REVIEW" 
   (	"ID_REV" NUMBER(38,0), 
	"TITULO" VARCHAR2(50 CHAR), 
	"SUBTITULO" VARCHAR2(75 CHAR), 
	"URL_VIDEO" VARCHAR2(200 CHAR), 
	"URL_HEADER" VARCHAR2(200 CHAR), 
	"CUERPO" VARCHAR2(4000 CHAR), 
	"GENERO" VARCHAR2(100 CHAR), 
	"USUARIO_NOM_USUARIO" VARCHAR2(20 CHAR)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table USUARIO
--------------------------------------------------------

  CREATE TABLE "PANTOASTEDDB"."USUARIO" 
   (	"NOM_USUARIO" VARCHAR2(20 CHAR), 
	"CONTRASENA" VARCHAR2(15 CHAR), 
	"PRIVILEGIO" NUMBER(1,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
REM INSERTING into PANTOASTEDDB.COMENTARIO
SET DEFINE OFF;
Insert into PANTOASTEDDB.COMENTARIO (ID_COMEN,CUERPO,REVIEW_ID_REV,USUARIO_NOM_USUARIO,REVIEW_NOM_USUARIO) values ('21','lorem ipsum un comentario positivo :D','49','noxer','noxer');
Insert into PANTOASTEDDB.COMENTARIO (ID_COMEN,CUERPO,REVIEW_ID_REV,USUARIO_NOM_USUARIO,REVIEW_NOM_USUARIO) values ('62','asdasd soy el bot','51','pantoastedBOT','noxer');
Insert into PANTOASTEDDB.COMENTARIO (ID_COMEN,CUERPO,REVIEW_ID_REV,USUARIO_NOM_USUARIO,REVIEW_NOM_USUARIO) values ('81','hola!','49','Thewinner','noxer');
Insert into PANTOASTEDDB.COMENTARIO (ID_COMEN,CUERPO,REVIEW_ID_REV,USUARIO_NOM_USUARIO,REVIEW_NOM_USUARIO) values ('121','humm','51','pantoastedBOT','noxer');
Insert into PANTOASTEDDB.COMENTARIO (ID_COMEN,CUERPO,REVIEW_ID_REV,USUARIO_NOM_USUARIO,REVIEW_NOM_USUARIO) values ('60','hum testeando el sitio','51','noxer','noxer');
Insert into PANTOASTEDDB.COMENTARIO (ID_COMEN,CUERPO,REVIEW_ID_REV,USUARIO_NOM_USUARIO,REVIEW_NOM_USUARIO) values ('61','asdasdasdasd','49','pantoastedBOT','noxer');
REM INSERTING into PANTOASTEDDB.EVALUACION
SET DEFINE OFF;
Insert into PANTOASTEDDB.EVALUACION (ID_EVAL,RENDIMIENTO,JUGABILIDAD,CONTENIDO,CONTROLES,REVIEW_ID_REV,REVIEW_USUARIO_NOM_USUARIO,NUM_EVALUACIONES) values ('3','3','4','7','10','63','noxer','1');
Insert into PANTOASTEDDB.EVALUACION (ID_EVAL,RENDIMIENTO,JUGABILIDAD,CONTENIDO,CONTROLES,REVIEW_ID_REV,REVIEW_USUARIO_NOM_USUARIO,NUM_EVALUACIONES) values ('4','45','43','40','46','49','noxer','6');
Insert into PANTOASTEDDB.EVALUACION (ID_EVAL,RENDIMIENTO,JUGABILIDAD,CONTENIDO,CONTROLES,REVIEW_ID_REV,REVIEW_USUARIO_NOM_USUARIO,NUM_EVALUACIONES) values ('5','16','17','18','19','51','noxer','2');
REM INSERTING into PANTOASTEDDB.GENERO
SET DEFINE OFF;
Insert into PANTOASTEDDB.GENERO (ID_GENERO,NOMBRE) values ('20','RPG');
Insert into PANTOASTEDDB.GENERO (ID_GENERO,NOMBRE) values ('21','MMORPG');
Insert into PANTOASTEDDB.GENERO (ID_GENERO,NOMBRE) values ('22','FPS');
Insert into PANTOASTEDDB.GENERO (ID_GENERO,NOMBRE) values ('23','Accion');
Insert into PANTOASTEDDB.GENERO (ID_GENERO,NOMBRE) values ('24','Puzzle');
Insert into PANTOASTEDDB.GENERO (ID_GENERO,NOMBRE) values ('25','Shooter');
Insert into PANTOASTEDDB.GENERO (ID_GENERO,NOMBRE) values ('26','Multiplayer');
Insert into PANTOASTEDDB.GENERO (ID_GENERO,NOMBRE) values ('27','Survival');
Insert into PANTOASTEDDB.GENERO (ID_GENERO,NOMBRE) values ('28','Aventura');
Insert into PANTOASTEDDB.GENERO (ID_GENERO,NOMBRE) values ('29','Arcade');
Insert into PANTOASTEDDB.GENERO (ID_GENERO,NOMBRE) values ('30','Estrategia');
Insert into PANTOASTEDDB.GENERO (ID_GENERO,NOMBRE) values ('31','Musical');
Insert into PANTOASTEDDB.GENERO (ID_GENERO,NOMBRE) values ('32','Indie');
Insert into PANTOASTEDDB.GENERO (ID_GENERO,NOMBRE) values ('33','Casual');
Insert into PANTOASTEDDB.GENERO (ID_GENERO,NOMBRE) values ('34','Carreras');
Insert into PANTOASTEDDB.GENERO (ID_GENERO,NOMBRE) values ('35','Simuladores');
Insert into PANTOASTEDDB.GENERO (ID_GENERO,NOMBRE) values ('36','Deportes');
REM INSERTING into PANTOASTEDDB.PERFIL
SET DEFINE OFF;
Insert into PANTOASTEDDB.PERFIL (ID_USUARIO,CORREO_E,URL_AVATAR,FECHA_NAC,SEXO,PUNTOS,USUARIO_NOM_USUARIO) values ('20','e.noyer.silva@gmail.com','public/img/avatar/noxer.jpg',to_date('20/06/94','DD/MM/RR'),'m','9','noxer');
Insert into PANTOASTEDDB.PERFIL (ID_USUARIO,CORREO_E,URL_AVATAR,FECHA_NAC,SEXO,PUNTOS,USUARIO_NOM_USUARIO) values ('21','foo@gmail.com','http://3.bp.blogspot.com/--1GO3T-4NQU/UcDZ1oNK5HI/AAAAAAAABZw/Qzb8fZUAOGg/s1600/pan+toasted.jpg',to_date('05/04/14','DD/MM/RR'),'o','0','pantoastedBOT');
Insert into PANTOASTEDDB.PERFIL (ID_USUARIO,CORREO_E,URL_AVATAR,FECHA_NAC,SEXO,PUNTOS,USUARIO_NOM_USUARIO) values ('42','uhyiuyiuyiuy','public/img/avatar/jean2.jpg',to_date('21/05/86','DD/MM/RR'),'o','0','jean2');
Insert into PANTOASTEDDB.PERFIL (ID_USUARIO,CORREO_E,URL_AVATAR,FECHA_NAC,SEXO,PUNTOS,USUARIO_NOM_USUARIO) values ('41','test@gmail.com','public/img/avatar/kido.jpg',to_date('15/10/94','DD/MM/RR'),'o','0','kido');
Insert into PANTOASTEDDB.PERFIL (ID_USUARIO,CORREO_E,URL_AVATAR,FECHA_NAC,SEXO,PUNTOS,USUARIO_NOM_USUARIO) values ('60','sebaldebeni@gmail.com','public/img/avatar/sebastian.jpg',to_date('01/06/97','DD/MM/RR'),'o','0','sebastian');
REM INSERTING into PANTOASTEDDB.RELATION_8
SET DEFINE OFF;
REM INSERTING into PANTOASTEDDB.REPORTE
SET DEFINE OFF;
Insert into PANTOASTEDDB.REPORTE (ID_REPORTE,MOTIVO,TIPO,COMENTARIO_ID_COMEN,REVIEW_ID_REV,REVIEW_USUARIO_NOM_USUARIO,COMENTARIO_USUARIO_NOM_USUARIO) values ('1','prueba','C','21','20','noxer','noxer');
REM INSERTING into PANTOASTEDDB.REVIEW
SET DEFINE OFF;
Insert into PANTOASTEDDB.REVIEW (ID_REV,TITULO,SUBTITULO,URL_VIDEO,URL_HEADER,CUERPO,GENERO,USUARIO_NOM_USUARIO) values ('20','Body Hero','El mejor juego de la vida',null,null,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis diam. Pellentesque ut neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In dui magna, posuere eget, vestibulum et, tempor auctor, justo. In ac felis quis tortor malesuada pretium. Pellentesque auctor neque nec urna. Proin sapien ipsum, porta a, auctor quis, euismod ut, mi. Aenean viverra rhoncus pede. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Phasellus a est. Ph','0','noxer');
Insert into PANTOASTEDDB.REVIEW (ID_REV,TITULO,SUBTITULO,URL_VIDEO,URL_HEADER,CUERPO,GENERO,USUARIO_NOM_USUARIO) values ('49','Probando','----fezzzzzz---','//www.youtube.com/embed/YGltYRq2JU4','http://edgecast.lvlup-files.buscafs.com/uploads/news/photos/news_photo_35862_1386695744.png','yup buen juego y asdf
yup buen juego y asdfyup buen juego y asdf
yup buen juego y asdf
yup buen juego y asdf
yup buen juego y asdf
v
yup buen juego y asdf
yup buen juego y asdf
yup buen juego y asdf
yup buen juego y asdf
yup buen juego y asdf
v
yup buen juego y asdfyup buen juego y asdf
yup buen juego y asdf
yup buen juego y asdfyup buen juego y asdfyup buen juego y asdfyup buen juego y asdfyup buen juego y asdfyup buen juego y asdfyup buen juego y asdfyup buen juego y asdfyup buen juego y asdfyup buen juego y asdfyup buen juego y asdfyup buen juego y asdfyup buen juego y asdfyup buen juego y asdfyup buen juego y asdf','32','noxer');
Insert into PANTOASTEDDB.REVIEW (ID_REV,TITULO,SUBTITULO,URL_VIDEO,URL_HEADER,CUERPO,GENERO,USUARIO_NOM_USUARIO) values ('51','Un Titulo','global global asdf','//www.youtube.com/embed/uqtSKkyJgFM','http://img2.meristation.com/files/imagenes/general/braid-screen01_0.jpg','no creo que sea necesario una nueva reseña
no creo que sea necesario una nueva reseñano creo que sea necesario una nueva reseñano creo que sea necesario una nueva reseña
no creo que sea necesario una nueva reseñano creo que sea necesario una nueva reseñano creo que sea necesario una nueva reseñano creo que sea necesario una nueva reseñano creo que sea necesario una nueva reseñano creo que sea necesario una nueva reseña
no creo que sea necesario una nueva reseñano creo que sea necesario una nueva reseñano creo que sea necesario una nueva reseñano creo que sea necesario una nueva reseña
no creo que sea necesario una nueva reseñano creo que sea necesario una nueva reseñano creo que sea necesario una nueva reseñano creo que sea necesario una nueva reseñano creo que sea necesario una nueva reseñano creo que sea necesario una nueva reseñano creo que sea necesario una nueva reseñano creo que sea necesario una nueva reseñano creo que sea necesario una nueva reseñano creo que sea necesario una nueva reseña','30','noxer');
Insert into PANTOASTEDDB.REVIEW (ID_REV,TITULO,SUBTITULO,URL_VIDEO,URL_HEADER,CUERPO,GENERO,USUARIO_NOM_USUARIO) values ('63','Otra prueba','un subtitulo','fgjhfgjh','fgjhfgjh','fgdhghjfghjfgjhfgjh','26','noxer');
REM INSERTING into PANTOASTEDDB.USUARIO
SET DEFINE OFF;
Insert into PANTOASTEDDB.USUARIO (NOM_USUARIO,CONTRASENA,PRIVILEGIO) values ('noxer','0000','3');
Insert into PANTOASTEDDB.USUARIO (NOM_USUARIO,CONTRASENA,PRIVILEGIO) values ('pantoastedBOT','0001','0');
Insert into PANTOASTEDDB.USUARIO (NOM_USUARIO,CONTRASENA,PRIVILEGIO) values ('vale','123456','0');
Insert into PANTOASTEDDB.USUARIO (NOM_USUARIO,CONTRASENA,PRIVILEGIO) values ('Thewinner','adriel0256','3');
Insert into PANTOASTEDDB.USUARIO (NOM_USUARIO,CONTRASENA,PRIVILEGIO) values ('kido','123456','0');
Insert into PANTOASTEDDB.USUARIO (NOM_USUARIO,CONTRASENA,PRIVILEGIO) values ('jean','1234','2');
Insert into PANTOASTEDDB.USUARIO (NOM_USUARIO,CONTRASENA,PRIVILEGIO) values ('jean2','adriel0256','0');
Insert into PANTOASTEDDB.USUARIO (NOM_USUARIO,CONTRASENA,PRIVILEGIO) values ('sebastian','sebaxx1','0');
--------------------------------------------------------
--  DDL for Index REPORTE_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PANTOASTEDDB"."REPORTE_PK" ON "PANTOASTEDDB"."REPORTE" ("ID_REPORTE", "COMENTARIO_ID_COMEN", "COMENTARIO_USUARIO_NOM_USUARIO", "REVIEW_ID_REV", "REVIEW_USUARIO_NOM_USUARIO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index RELATION_8__IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "PANTOASTEDDB"."RELATION_8__IDX" ON "PANTOASTEDDB"."RELATION_8" ("REVIEW_ID_REV", "REVIEW_USUARIO_NOM_USUARIO", "GENERO_ID_GENERO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index EVALUACION__IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "PANTOASTEDDB"."EVALUACION__IDX" ON "PANTOASTEDDB"."EVALUACION" ("REVIEW_ID_REV", "REVIEW_USUARIO_NOM_USUARIO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index PERFIL_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PANTOASTEDDB"."PERFIL_PK" ON "PANTOASTEDDB"."PERFIL" ("ID_USUARIO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index PERFIL__IDX
--------------------------------------------------------

  CREATE UNIQUE INDEX "PANTOASTEDDB"."PERFIL__IDX" ON "PANTOASTEDDB"."PERFIL" ("USUARIO_NOM_USUARIO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index COMENTARIO_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PANTOASTEDDB"."COMENTARIO_PK" ON "PANTOASTEDDB"."COMENTARIO" ("ID_COMEN", "USUARIO_NOM_USUARIO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index GENERO_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PANTOASTEDDB"."GENERO_PK" ON "PANTOASTEDDB"."GENERO" ("ID_GENERO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index USUARIO_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PANTOASTEDDB"."USUARIO_PK" ON "PANTOASTEDDB"."USUARIO" ("NOM_USUARIO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index EVALUACION_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PANTOASTEDDB"."EVALUACION_PK" ON "PANTOASTEDDB"."EVALUACION" ("ID_EVAL") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index REVIEW_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PANTOASTEDDB"."REVIEW_PK" ON "PANTOASTEDDB"."REVIEW" ("ID_REV", "USUARIO_NOM_USUARIO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  Constraints for Table COMENTARIO
--------------------------------------------------------

  ALTER TABLE "PANTOASTEDDB"."COMENTARIO" ADD CONSTRAINT "COMENTARIO_PK" PRIMARY KEY ("ID_COMEN", "USUARIO_NOM_USUARIO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "PANTOASTEDDB"."COMENTARIO" MODIFY ("USUARIO_NOM_USUARIO" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."COMENTARIO" MODIFY ("CUERPO" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."COMENTARIO" MODIFY ("ID_COMEN" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PERFIL
--------------------------------------------------------

  ALTER TABLE "PANTOASTEDDB"."PERFIL" ADD CONSTRAINT "PERFIL_PK" PRIMARY KEY ("ID_USUARIO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "PANTOASTEDDB"."PERFIL" MODIFY ("USUARIO_NOM_USUARIO" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."PERFIL" MODIFY ("PUNTOS" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."PERFIL" MODIFY ("SEXO" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."PERFIL" MODIFY ("FECHA_NAC" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."PERFIL" MODIFY ("CORREO_E" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."PERFIL" MODIFY ("ID_USUARIO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table REPORTE
--------------------------------------------------------

  ALTER TABLE "PANTOASTEDDB"."REPORTE" ADD CONSTRAINT "REPORTE_PK" PRIMARY KEY ("ID_REPORTE", "COMENTARIO_ID_COMEN", "COMENTARIO_USUARIO_NOM_USUARIO", "REVIEW_ID_REV", "REVIEW_USUARIO_NOM_USUARIO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "PANTOASTEDDB"."REPORTE" MODIFY ("COMENTARIO_USUARIO_NOM_USUARIO" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."REPORTE" MODIFY ("REVIEW_USUARIO_NOM_USUARIO" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."REPORTE" MODIFY ("REVIEW_ID_REV" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."REPORTE" MODIFY ("COMENTARIO_ID_COMEN" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."REPORTE" MODIFY ("TIPO" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."REPORTE" MODIFY ("MOTIVO" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."REPORTE" MODIFY ("ID_REPORTE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table USUARIO
--------------------------------------------------------

  ALTER TABLE "PANTOASTEDDB"."USUARIO" ADD CONSTRAINT "USUARIO_PK" PRIMARY KEY ("NOM_USUARIO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "PANTOASTEDDB"."USUARIO" MODIFY ("PRIVILEGIO" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."USUARIO" MODIFY ("CONTRASENA" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."USUARIO" MODIFY ("NOM_USUARIO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table REVIEW
--------------------------------------------------------

  ALTER TABLE "PANTOASTEDDB"."REVIEW" ADD CONSTRAINT "REVIEW_PK" PRIMARY KEY ("ID_REV", "USUARIO_NOM_USUARIO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "PANTOASTEDDB"."REVIEW" MODIFY ("USUARIO_NOM_USUARIO" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."REVIEW" MODIFY ("GENERO" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."REVIEW" MODIFY ("CUERPO" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."REVIEW" MODIFY ("SUBTITULO" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."REVIEW" MODIFY ("TITULO" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."REVIEW" MODIFY ("ID_REV" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table RELATION_8
--------------------------------------------------------

  ALTER TABLE "PANTOASTEDDB"."RELATION_8" ADD CONSTRAINT "RELATION_8__IDX" PRIMARY KEY ("REVIEW_ID_REV", "REVIEW_USUARIO_NOM_USUARIO", "GENERO_ID_GENERO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "PANTOASTEDDB"."RELATION_8" MODIFY ("GENERO_ID_GENERO" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."RELATION_8" MODIFY ("REVIEW_USUARIO_NOM_USUARIO" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."RELATION_8" MODIFY ("REVIEW_ID_REV" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table EVALUACION
--------------------------------------------------------

  ALTER TABLE "PANTOASTEDDB"."EVALUACION" MODIFY ("NUM_EVALUACIONES" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."EVALUACION" ADD CONSTRAINT "EVALUACION_PK" PRIMARY KEY ("ID_EVAL")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "PANTOASTEDDB"."EVALUACION" MODIFY ("REVIEW_USUARIO_NOM_USUARIO" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."EVALUACION" MODIFY ("REVIEW_ID_REV" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."EVALUACION" MODIFY ("CONTROLES" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."EVALUACION" MODIFY ("CONTENIDO" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."EVALUACION" MODIFY ("JUGABILIDAD" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."EVALUACION" MODIFY ("RENDIMIENTO" NOT NULL ENABLE);
  ALTER TABLE "PANTOASTEDDB"."EVALUACION" MODIFY ("ID_EVAL" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table GENERO
--------------------------------------------------------

  ALTER TABLE "PANTOASTEDDB"."GENERO" ADD CONSTRAINT "GENERO_PK" PRIMARY KEY ("ID_GENERO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "PANTOASTEDDB"."GENERO" MODIFY ("ID_GENERO" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table COMENTARIO
--------------------------------------------------------

  ALTER TABLE "PANTOASTEDDB"."COMENTARIO" ADD CONSTRAINT "COMENTARIO_REVIEW_FK" FOREIGN KEY ("REVIEW_ID_REV", "REVIEW_NOM_USUARIO")
	  REFERENCES "PANTOASTEDDB"."REVIEW" ("ID_REV", "USUARIO_NOM_USUARIO") ENABLE;
  ALTER TABLE "PANTOASTEDDB"."COMENTARIO" ADD CONSTRAINT "COMENTARIO_USUARIO_FK" FOREIGN KEY ("USUARIO_NOM_USUARIO")
	  REFERENCES "PANTOASTEDDB"."USUARIO" ("NOM_USUARIO") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table EVALUACION
--------------------------------------------------------

  ALTER TABLE "PANTOASTEDDB"."EVALUACION" ADD CONSTRAINT "EVALUACION_REVIEW_FK" FOREIGN KEY ("REVIEW_ID_REV", "REVIEW_USUARIO_NOM_USUARIO")
	  REFERENCES "PANTOASTEDDB"."REVIEW" ("ID_REV", "USUARIO_NOM_USUARIO") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PERFIL
--------------------------------------------------------

  ALTER TABLE "PANTOASTEDDB"."PERFIL" ADD CONSTRAINT "PERFIL_USUARIO_FK" FOREIGN KEY ("USUARIO_NOM_USUARIO")
	  REFERENCES "PANTOASTEDDB"."USUARIO" ("NOM_USUARIO") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table RELATION_8
--------------------------------------------------------

  ALTER TABLE "PANTOASTEDDB"."RELATION_8" ADD CONSTRAINT "FK_ASS_7" FOREIGN KEY ("REVIEW_ID_REV", "REVIEW_USUARIO_NOM_USUARIO")
	  REFERENCES "PANTOASTEDDB"."REVIEW" ("ID_REV", "USUARIO_NOM_USUARIO") ENABLE;
  ALTER TABLE "PANTOASTEDDB"."RELATION_8" ADD CONSTRAINT "FK_ASS_8" FOREIGN KEY ("GENERO_ID_GENERO")
	  REFERENCES "PANTOASTEDDB"."GENERO" ("ID_GENERO") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table REPORTE
--------------------------------------------------------

  ALTER TABLE "PANTOASTEDDB"."REPORTE" ADD CONSTRAINT "REPORTE_COMENTARIO_FK" FOREIGN KEY ("COMENTARIO_ID_COMEN", "COMENTARIO_USUARIO_NOM_USUARIO")
	  REFERENCES "PANTOASTEDDB"."COMENTARIO" ("ID_COMEN", "USUARIO_NOM_USUARIO") ENABLE;
  ALTER TABLE "PANTOASTEDDB"."REPORTE" ADD CONSTRAINT "REPORTE_REVIEW_FK" FOREIGN KEY ("REVIEW_ID_REV", "REVIEW_USUARIO_NOM_USUARIO")
	  REFERENCES "PANTOASTEDDB"."REVIEW" ("ID_REV", "USUARIO_NOM_USUARIO") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table REVIEW
--------------------------------------------------------

  ALTER TABLE "PANTOASTEDDB"."REVIEW" ADD CONSTRAINT "REVIEW_USUARIO_FK" FOREIGN KEY ("USUARIO_NOM_USUARIO")
	  REFERENCES "PANTOASTEDDB"."USUARIO" ("NOM_USUARIO") ENABLE;
--------------------------------------------------------
--  DDL for Trigger TRIGGER_RANGO_AUTOR
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "PANTOASTEDDB"."TRIGGER_RANGO_AUTOR" 
AFTER UPDATE OF PUNTOS ON PERFIL FOR EACH ROW
 WHEN (NEW.PUNTOS>=100) DECLARE
p_puntos PERFIL.PUNTOS%TYPE:=0;
 BEGIN
 UPDATE USUARIO SET PRIVILEGIO=1 WHERE NOM_USUARIO=:NEW.USUARIO_NOM_USUARIO;
END;
/
ALTER TRIGGER "PANTOASTEDDB"."TRIGGER_RANGO_AUTOR" ENABLE;
--------------------------------------------------------
--  DDL for Package PANTOASTEDFUNC
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "PANTOASTEDDB"."PANTOASTEDFUNC" AS 
  /* TODO enter package declarations (types, exceptions, methods etc) here */ 
  PROCEDURE REG_USUARIO(n_usuario VARCHAR2 ,contrasena VARCHAR2, respuesta OUT VARCHAR2);
  
  PROCEDURE VALIDAR(p_usuario varchar2 , p_contrasena varchar2 , respuesta OUT VARCHAR2);
  
PROCEDURE MODIFICAR_PERFIL(n_usuario VARCHAR2,p_correo_e PERFIL.CORREO_E%type,
  p_url_avatar PERFIL.URL_AVATAR%type,p_fecha_nac PERFIL.FECHA_NAC%type,p_sexo PERFIL.SEXO%type,respuesta OUT VARCHAR2);
  
 PROCEDURE CREAR_PERFIL(n_usuario VARCHAR2,p_correo_e PERFIL.CORREO_E%type,
  p_url_avatar PERFIL.URL_AVATAR%type,p_fecha_nac PERFIL.FECHA_NAC%type,p_sexo PERFIL.SEXO%type,respuesta OUT VARCHAR2);
  
  PROCEDURE CONSULTAR_PERFIL(n_usuario VARCHAR2,p_correo_e OUT PERFIL.CORREO_E%type,
  p_url_avatar OUT PERFIL.URL_AVATAR%type,p_fecha_nac OUT PERFIL.FECHA_NAC%type,p_sexo OUT PERFIL.SEXO%type,p_puntos OUT NUMERIC);
  
  PROCEDURE CREAR_REVIEW(n_usuario VARCHAR2,p_titulo REVIEW.TITULO%type,p_subtitulo  REVIEW.SUBTITULO%type,
  p_genero  REVIEW.GENERO%type,p_cuerpo  REVIEW.CUERPO%type,p_url_video  REVIEW.URL_VIDEO%type,p_url_header  REVIEW.URL_HEADER%type,pRendimiento EVALUACION.RENDIMIENTO%TYPE, 
   pJugabilidad EVALUACION.JUGABILIDAD%TYPE, pContenido EVALUACION.CONTENIDO%TYPE, 
   pControles EVALUACION.CONTROLES%TYPE,respuesta OUT VARCHAR2);
  
  PROCEDURE CONSULTAR_REVIEW(p_id_review REVIEW.ID_REV%type,p_titulo OUT REVIEW.TITULO%type,p_subtitulo OUT REVIEW.SUBTITULO%type,
  p_genero OUT REVIEW.GENERO%type,p_cuerpo OUT REVIEW.CUERPO%type,p_url_video OUT REVIEW.URL_VIDEO%type,p_url_header OUT REVIEW.URL_HEADER%type, p_usuario OUT VARCHAR2, puntuacion out VARCHAR2);
  
 PROCEDURE RETORNAR_COMENTARIOS(p_id_review REVIEW.ID_REV%type, cursor_comentarios OUT tipos.ref_cursor);
 
 PROCEDURE CREAR_COMENTARIO(cuerpo COMENTARIO.CUERPO%type,p_id_review REVIEW.ID_REV%type,n_usuario VARCHAR2,respuesta OUT varchar2);
 
 PROCEDURE MODIFICAR_COMENTARIO(p_id_comen COMENTARIO.ID_COMEN%TYPE,p_cuerpo COMENTARIO.CUERPO%type, respuesta OUT VARCHAR2);
 
 PROCEDURE ACTUALIZA_PUNTOSPERFIL(n_usuario VARCHAR2, operacion VARCHAR2, respuesta OUT VARCHAR2);
 
 PROCEDURE ELIMINAR_REVIEW(p_id_review VARCHAR2,respuesta OUT VARCHAR2);
 
 PROCEDURE Carga_reportes( cursor_reportes OUT tipos.ref_cursor);
 
PROCEDURE ACTUALIZA_PUNTOSPUBLICACION(pRendimiento EVALUACION.RENDIMIENTO%TYPE, 
   pJugabilidad EVALUACION.JUGABILIDAD%TYPE, pContenido EVALUACION.CONTENIDO%TYPE, 
   pControles EVALUACION.CONTROLES%TYPE,p_id_review REVIEW.ID_REV%type, respuesta OUT VARCHAR2);
 PROCEDURE CAMBIO_RANGO_AUTOR(USUARIO PERFIL.USUARIO_NOM_USUARIO%TYPE);
END ;

/
--------------------------------------------------------
--  DDL for Package TIPOS
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "PANTOASTEDDB"."TIPOS" 
as
type ref_cursor is ref cursor;
end;

/
--------------------------------------------------------
--  DDL for Package Body PANTOASTEDFUNC
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "PANTOASTEDDB"."PANTOASTEDFUNC" AS
--funcion verificar existencia de un usario
 function VERIFICAR_USUARIO(n_usuario varchar2) return number
IS
retorno number(1):=-1;
BEGIN
select count(NOM_USUARIO) into retorno from USUARIO where NOM_USUARIO = n_usuario;
return retorno;
end VERIFICAR_USUARIO;


--PROCEDIMIENTO PARA INSERTAR SI NO EXISTE
 PROCEDURE REG_USUARIO(n_usuario VARCHAR2,contrasena VARCHAR2, respuesta OUT VARCHAR2) IS

 BEGIN
  IF (VERIFICAR_USUARIO(n_usuario) > 0 )THEN
  respuesta:='false';
  ELSE 
  INSERT INTO USUARIO VALUES(n_usuario,contrasena,0);
  respuesta:='true';
  END IF;
 END REG_USUARIO;



--Procedimiento autentificar
Procedure VALIDAR(p_usuario varchar2 , p_contrasena varchar2,respuesta OUT VARCHAR2 ) is
user_log varchar2(20) := 'offline';
user_priv varchar2(10) := 'null';
BEGIN
select decode(privilegio, 0,'normal',1,'autor',2,'mod',3,'admin')as priv into  user_priv   from usuario where lower(nom_usuario) = lower(p_usuario) and contrasena = p_contrasena; 
if (SQL%FOUND) then
user_log := p_usuario;
respuesta:=user_priv;
DBMS_OUTPUT.PUT_LINE(user_log||' '||user_priv);
end if;

EXCEPTION WHEN NO_DATA_FOUND THEN
DBMS_OUTPUT.PUT_LINE('error');
respuesta:='null';
END Validar;

--Procedimiento Actualizar perfil
PROCEDURE MODIFICAR_PERFIL(n_usuario VARCHAR2,p_correo_e PERFIL.CORREO_E%type,
  p_url_avatar PERFIL.URL_AVATAR%type,p_fecha_nac PERFIL.FECHA_NAC%type,p_sexo PERFIL.SEXO%type,respuesta OUT VARCHAR2) IS
  BEGIN
    IF (VERIFICAR_USUARIO(n_usuario)>0)THEN
    UPDATE PERFIL
    SET CORREO_E = p_correo_e , URL_AVATAR = p_url_avatar,FECHA_NAC=p_fecha_nac,SEXO=p_sexo
    WHERE lower(USUARIO_NOM_USUARIO) = lower(n_usuario);
    respuesta:='true';
    ELSE
    respuesta:='false';
    END IF;
  END MODIFICAR_PERFIL;
  
--CREAR un nuevo PERFIL
PROCEDURE CREAR_PERFIL(n_usuario VARCHAR2,p_correo_e PERFIL.CORREO_E%type,
  p_url_avatar PERFIL.URL_AVATAR%type,p_fecha_nac PERFIL.FECHA_NAC%type,p_sexo PERFIL.SEXO%type,respuesta OUT VARCHAR2) IS
  BEGIN
    IF (VERIFICAR_USUARIO(n_usuario)>0)THEN
    INSERT INTO PERFIL
    VALUES(SEC_ID_USUARIO.NEXTVAL, p_correo_e, p_url_avatar, p_fecha_nac, p_sexo ,0,n_usuario);
    respuesta:='true';
    ELSE
    respuesta:='false';
    END IF;
  END CREAR_PERFIL;
  


PROCEDURE CONSULTAR_PERFIL(n_usuario VARCHAR2,p_correo_e OUT PERFIL.CORREO_E%type,
  p_url_avatar OUT PERFIL.URL_AVATAR%type,p_fecha_nac OUT PERFIL.FECHA_NAC%type,p_sexo OUT PERFIL.SEXO%type,p_puntos OUT NUMERIC) IS
BEGIN
--Modificar para implementar sistema puntos.
SELECT correo_e,url_avatar,fecha_nac,sexo,puntos into p_correo_e,p_url_avatar,p_fecha_nac,p_sexo,p_puntos
FROM PERFIL
WHERE lower(usuario_nom_usuario) = lower(n_usuario);
END CONSULTAR_PERFIL;


--Control de nuevo Review y consulta de Review
PROCEDURE CREAR_REVIEW(n_usuario VARCHAR2,p_titulo REVIEW.TITULO%type,p_subtitulo  REVIEW.SUBTITULO%type,
  p_genero  REVIEW.GENERO%type,p_cuerpo  REVIEW.CUERPO%type,p_url_video  REVIEW.URL_VIDEO%type,p_url_header  REVIEW.URL_HEADER%type,pRendimiento EVALUACION.RENDIMIENTO%TYPE, 
   pJugabilidad EVALUACION.JUGABILIDAD%TYPE, pContenido EVALUACION.CONTENIDO%TYPE, 
   pControles EVALUACION.CONTROLES%TYPE, respuesta OUT VARCHAR2) IS
  verificador_rev number(10) := 0;
  secuencia number(38) :=0 ;
  BEGIN
  INSERT INTO REVIEW
  values(SEC_ID_REV.NEXTVAL,p_titulo,p_subtitulo,p_url_video,p_url_header,p_cuerpo,p_genero,n_usuario);
 secuencia:=SEC_ID_REV.CURRVAL;
  select count(ID_REV) into verificador_rev
  from REVIEW
  where ID_REV = secuencia;
  IF(verificador_rev>0) THEN
    INSERT INTO EVALUACION VALUES(SEC_ID_EVAL.NEXTVAL,pRendimiento,pJugabilidad,pContenido,pControles,secuencia,n_usuario,1);
    respuesta:='true';
  ELSE
    respuesta:='false';
  END IF;
  END CREAR_REVIEW;
  
  
PROCEDURE CONSULTAR_REVIEW(p_id_review REVIEW.ID_REV%type,p_titulo OUT REVIEW.TITULO%type,p_subtitulo OUT REVIEW.SUBTITULO%type,
  p_genero OUT REVIEW.GENERO%type,p_cuerpo OUT REVIEW.CUERPO%type,p_url_video OUT REVIEW.URL_VIDEO%type,p_url_header OUT REVIEW.URL_HEADER%type, p_usuario OUT VARCHAR2,puntuacion out VARCHAR2) IS
  prendimiento EVALUACION.RENDIMIENTO%type :=0;
  pjugabilidad EVALUACION.JUGABILIDAD%type:=0;
  pcontenido EVALUACION.CONTENIDO%type:=0;
  pcontroles EVALUACION.CONTROLES%type:=0;
  pnum_eval EVALUACION.NUM_EVALUACIONES%type:=0;
  BEGIN
  SELECT titulo,subtitulo,genero,cuerpo,url_video,url_header,usuario_nom_usuario
  INTO p_titulo,p_subtitulo,p_genero,p_cuerpo,p_url_video,p_url_header,p_usuario
  FROM REVIEW
  WHERE ID_REV = p_id_review;
  
  select rendimiento,jugabilidad,contenido,controles,NUM_EVALUACIONES 
  into prendimiento,pjugabilidad,pcontenido,pcontroles,pnum_eval
  from EVALUACION
  where lower(review_id_rev) = lower(p_id_review);
  
  puntuacion:= ((prendimiento/pnum_eval) + (pjugabilidad/pnum_eval) + (pcontenido/pnum_eval) + (pcontroles/pnum_eval))/4;
  END CONSULTAR_REVIEW;
  
  --Procedimiento para eliminar un review
  PROCEDURE ELIMINAR_REVIEW(p_id_review VARCHAR2,respuesta OUT VARCHAR2) IS
  verificador_rev number(10) :=0;
  BEGIN
  DELETE FROM REVIEW where REVIEW.ID_REV = p_id_review;
  select count(id_rev) into verificador_rev from REVIEW where id_rev = p_id_review;
  if verificador_rev>0 then
  respuesta:='false';
  else
  respuesta:='true';
  end if;
  END ELIMINAR_REVIEW;
  
  --Procedimiento para retornar todos los comentarios de una publicacion en un cursor
  PROCEDURE RETORNAR_COMENTARIOS(p_id_review REVIEW.ID_REV%type, cursor_comentarios OUT tipos.ref_cursor)
  AS
  BEGIN
    OPEN cursor_comentarios FOR
      Select id_comen,cuerpo,review_id_rev,usuario_nom_usuario,
      (select url_avatar 
      from perfil 
      where perfil.usuario_nom_usuario = COMENTARIO.USUARIO_NOM_USUARIO) as avatar
      from COMENTARIO
      WHERE REVIEW_ID_REV = p_id_review
      ORDER BY id_comen;
    cursor_comentarios:=cursor_comentarios;
  END RETORNAR_COMENTARIOS;
  
  --Ingreso de un nuevo comentario
   PROCEDURE CREAR_COMENTARIO(cuerpo COMENTARIO.CUERPO%type,p_id_review REVIEW.ID_REV%type,n_usuario VARCHAR2, respuesta OUT varchar2)IS
    verificador_rev number(10) := 0;
    secuencia number(38) :=0 ;
   BEGIN
   INSERT INTO COMENTARIO
   VALUES(SEC_ID_COMEN.NEXTVAL,cuerpo,p_id_review,n_usuario,(select usuario_nom_usuario 
                                                            from review where id_rev = p_id_review));
   secuencia:=SEC_ID_COMEN.CURRVAL;
    select count(ID_COMEN) into verificador_rev
  from COMENTARIO
  where ID_COMEN = secuencia;
  IF(verificador_rev>0) THEN
    respuesta:='true';
  ELSE
    respuesta:='false';
  END IF;
   END CREAR_COMENTARIO;
   
   --Update a un comentario (solo lo puede realizar el mismo usuario o mod/admin)
   PROCEDURE MODIFICAR_COMENTARIO(p_id_comen COMENTARIO.ID_COMEN%TYPE,p_cuerpo COMENTARIO.CUERPO%type, respuesta OUT VARCHAR2)
   IS
   verificar_comen number(10):=0;
   BEGIN
   select count(id_comen) into verificar_comen from comentario where id_comen=p_id_comen;
   if verificar_comen>0 then
    UPDATE COMENTARIO SET cuerpo = p_cuerpo
    WHERE id_comen = p_id_comen;
    respuesta:='true';
   else
    respuesta:='false';
   end if;
   END MODIFICAR_COMENTARIO;
   
   --Actualizar puntos del perfil
   PROCEDURE ACTUALIZA_PUNTOSPERFIL(n_usuario VARCHAR2, operacion VARCHAR2, respuesta OUT VARCHAR2) IS
   BEGIN
   IF (VERIFICAR_USUARIO(n_usuario)>0)THEN    
    IF (operacion = '+') THEN
     UPDATE PERFIL
	   SET PUNTOS = PUNTOS + 1 
     WHERE lower(USUARIO_NOM_USUARIO) = lower(n_usuario);
     respuesta:='SUMO';  
     ELSE
     UPDATE PERFIL
	   SET PUNTOS = PUNTOS - 1 
     WHERE lower(USUARIO_NOM_USUARIO) = lower(n_usuario);
     respuesta:='RESTO';
     END IF;
  END IF;
   END ACTUALIZA_PUNTOSPERFIL;
   
   PROCEDURE CARGA_REPORTES( cursor_reportes OUT tipos.ref_cursor) AS
    
   BEGIN
    open cursor_reportes for
    select id_reporte, motivo , tipo from reporte;
    cursor_reportes := cursor_reportes;
    
   END Carga_reportes;
   
   --Actualizar los puntos de una publicacion
    PROCEDURE ACTUALIZA_PUNTOSPUBLICACION(pRendimiento EVALUACION.RENDIMIENTO%TYPE, 
   pJugabilidad EVALUACION.JUGABILIDAD%TYPE, pContenido EVALUACION.CONTENIDO%TYPE, 
   pControles EVALUACION.CONTROLES%TYPE,p_id_review REVIEW.ID_REV%type, respuesta OUT VARCHAR2)
   IS
      verificador number(10):=0;
   BEGIN
   select count(id_eval) into verificador from evaluacion where review_id_rev = p_id_review;
   if verificador>0 then
   UPDATE EVALUACION
   SET rendimiento = rendimiento + pRendimiento,
   jugabilidad = jugabilidad + pJugabilidad,
   contenido = contenido + pContenido,
   controles = controles + pControles,
   num_evaluaciones = num_evaluaciones + 1 
   WHERE review_id_rev = p_id_review;
   respuesta:='true';
   else
   respuesta:='false';
   end if;
   END ACTUALIZA_PUNTOSPUBLICACION;


--cAMBIAR EL RANGO DE UN USUARIO
 PROCEDURE CAMBIO_RANGO_AUTOR(USUARIO PERFIL.USUARIO_NOM_USUARIO%TYPE) IS
 p_puntos PERFIL.PUNTOS%TYPE:=0;
 BEGIN
 SELECT PUNTOS INTO p_puntos FROM PERFIL where usuario_nom_usuario=usuario;
 if(p_puntos+1>=100) then
 UPDATE USUARIO SET PRIVILEGIO=1 WHERE NOM_USUARIO=USUARIO;
 end if;
 END CAMBIO_RANGO_AUTOR;
 
END PANTOASTEDFUNC;

/
