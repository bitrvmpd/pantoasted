-- Generado por Oracle SQL Developer Data Modeler 4.0.1.836
--   en:        2014-04-05 11:06:09 CLST
--   sitio:      Oracle Database 11g
--   tipo:      Oracle Database 11g

CREATE TABLE COMENTARIO
  (
    ID_COMEN            NUMBER (38) NOT NULL ,
    CUERPO              VARCHAR2 (400 CHAR) NOT NULL ,
    REVIEW_ID_REV       NUMBER (38) ,
    USUARIO_NOM_USUARIO VARCHAR2 (20 CHAR) NOT NULL ,
    REVIEW_NOM_USUARIO  VARCHAR2 (20 CHAR)
  ) ;
ALTER TABLE COMENTARIO ADD CONSTRAINT COMENTARIO_PK PRIMARY KEY ( ID_COMEN, USUARIO_NOM_USUARIO ) ;

CREATE TABLE EVALUACION
  (
    ID_EVAL                    NUMBER (38) NOT NULL ,
    RENDIMIENTO                NUMBER (3) NOT NULL ,
    JUGABILIDAD                NUMBER (3) NOT NULL ,
    CONTENIDO                  NUMBER (3) NOT NULL ,
    CONTROLES                  NUMBER (3) NOT NULL ,
    REVIEW_ID_REV              NUMBER (38) NOT NULL ,
    REVIEW_USUARIO_NOM_USUARIO VARCHAR2 (20 CHAR) NOT NULL
  ) ;
CREATE UNIQUE INDEX EVALUACION__IDX ON EVALUACION
  (
    REVIEW_ID_REV ASC , REVIEW_USUARIO_NOM_USUARIO ASC
  )
  ;
  ALTER TABLE EVALUACION ADD CONSTRAINT EVALUACION_PK PRIMARY KEY ( ID_EVAL ) ;

CREATE TABLE GENERO
  (
    ID_GENERO NUMBER (38) NOT NULL ,
    NOMBRE    VARCHAR2 (50 CHAR)
  ) ;
ALTER TABLE GENERO ADD CONSTRAINT GENERO_PK PRIMARY KEY ( ID_GENERO ) ;

CREATE TABLE PERFIL
  (
    ID_USUARIO          NUMBER (38) NOT NULL ,
    CORREO_E            VARCHAR2 (50 CHAR) NOT NULL ,
    URL_AVATAR          VARCHAR2 (200 CHAR) ,
    FECHA_NAC           DATE NOT NULL ,
    SEXO                CHAR (1 CHAR) NOT NULL ,
    PUNTOS              NUMBER (4) NOT NULL ,
    USUARIO_NOM_USUARIO VARCHAR2 (20 CHAR) NOT NULL
  ) ;
CREATE UNIQUE INDEX PERFIL__IDX ON PERFIL
  (
    USUARIO_NOM_USUARIO ASC
  )
  ;
  ALTER TABLE PERFIL ADD CONSTRAINT PERFIL_PK PRIMARY KEY ( ID_USUARIO ) ;

CREATE TABLE REPORTE
  (
    ID_REPORTE                     NUMBER (38) NOT NULL ,
    MOTIVO                         VARCHAR2 (200 CHAR) NOT NULL ,
    TIPO                           CHAR (1 CHAR) NOT NULL ,
    COMENTARIO_ID_COMEN            NUMBER (38) NOT NULL ,
    REVIEW_ID_REV                  NUMBER (38) NOT NULL ,
    REVIEW_USUARIO_NOM_USUARIO     VARCHAR2 (20 CHAR) NOT NULL ,
    COMENTARIO_USUARIO_NOM_USUARIO VARCHAR2 (20 CHAR) NOT NULL
  ) ;
ALTER TABLE REPORTE ADD CONSTRAINT REPORTE_PK PRIMARY KEY ( ID_REPORTE, COMENTARIO_ID_COMEN, COMENTARIO_USUARIO_NOM_USUARIO, REVIEW_ID_REV, REVIEW_USUARIO_NOM_USUARIO ) ;

CREATE TABLE REVIEW
  (
    ID_REV              NUMBER (38) NOT NULL ,
    TITULO              VARCHAR2 (50 CHAR) NOT NULL ,
    SUBTITULO           VARCHAR2 (75 CHAR) NOT NULL ,
    URL_VIDEO           VARCHAR2 (200 CHAR) ,
    URL_HEADER          VARCHAR2 (200 CHAR) ,
    CUERPO              VARCHAR2 (4000 CHAR) NOT NULL ,
    GENERO              VARCHAR2 (100 CHAR) NOT NULL ,
    USUARIO_NOM_USUARIO VARCHAR2 (20 CHAR) NOT NULL
  ) ;
ALTER TABLE REVIEW ADD CONSTRAINT REVIEW_PK PRIMARY KEY ( ID_REV, USUARIO_NOM_USUARIO ) ;

CREATE TABLE Relation_8
  (
    REVIEW_ID_REV              NUMBER (38) NOT NULL ,
    REVIEW_USUARIO_NOM_USUARIO VARCHAR2 (20 CHAR) NOT NULL ,
    GENERO_ID_GENERO           NUMBER (38) NOT NULL
  ) ;
ALTER TABLE Relation_8 ADD CONSTRAINT Relation_8__IDX PRIMARY KEY ( REVIEW_ID_REV, REVIEW_USUARIO_NOM_USUARIO, GENERO_ID_GENERO ) ;

CREATE TABLE USUARIO
  (
    NOM_USUARIO VARCHAR2 (20 CHAR) NOT NULL ,
    CONTRASENA  VARCHAR2 (15 CHAR) NOT NULL ,
    PRIVILEGIO  NUMBER (1) NOT NULL
  ) ;
ALTER TABLE USUARIO ADD CONSTRAINT USUARIO_PK PRIMARY KEY ( NOM_USUARIO ) ;

ALTER TABLE COMENTARIO ADD CONSTRAINT COMENTARIO_REVIEW_FK FOREIGN KEY ( REVIEW_ID_REV, REVIEW_NOM_USUARIO ) REFERENCES REVIEW ( ID_REV, USUARIO_NOM_USUARIO ) ;

ALTER TABLE COMENTARIO ADD CONSTRAINT COMENTARIO_USUARIO_FK FOREIGN KEY ( USUARIO_NOM_USUARIO ) REFERENCES USUARIO ( NOM_USUARIO ) ;

ALTER TABLE EVALUACION ADD CONSTRAINT EVALUACION_REVIEW_FK FOREIGN KEY ( REVIEW_ID_REV, REVIEW_USUARIO_NOM_USUARIO ) REFERENCES REVIEW ( ID_REV, USUARIO_NOM_USUARIO ) ;

ALTER TABLE Relation_8 ADD CONSTRAINT FK_ASS_7 FOREIGN KEY ( REVIEW_ID_REV, REVIEW_USUARIO_NOM_USUARIO ) REFERENCES REVIEW ( ID_REV, USUARIO_NOM_USUARIO ) ;

ALTER TABLE Relation_8 ADD CONSTRAINT FK_ASS_8 FOREIGN KEY ( GENERO_ID_GENERO ) REFERENCES GENERO ( ID_GENERO ) ;

ALTER TABLE PERFIL ADD CONSTRAINT PERFIL_USUARIO_FK FOREIGN KEY ( USUARIO_NOM_USUARIO ) REFERENCES USUARIO ( NOM_USUARIO ) ;

ALTER TABLE REPORTE ADD CONSTRAINT REPORTE_COMENTARIO_FK FOREIGN KEY ( COMENTARIO_ID_COMEN, COMENTARIO_USUARIO_NOM_USUARIO ) REFERENCES COMENTARIO ( ID_COMEN, USUARIO_NOM_USUARIO ) ;

ALTER TABLE REPORTE ADD CONSTRAINT REPORTE_REVIEW_FK FOREIGN KEY ( REVIEW_ID_REV, REVIEW_USUARIO_NOM_USUARIO ) REFERENCES REVIEW ( ID_REV, USUARIO_NOM_USUARIO ) ;

ALTER TABLE REVIEW ADD CONSTRAINT REVIEW_USUARIO_FK FOREIGN KEY ( USUARIO_NOM_USUARIO ) REFERENCES USUARIO ( NOM_USUARIO ) ;


-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                             8
-- CREATE INDEX                             2
-- ALTER TABLE                             17
-- CREATE VIEW                              0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0


--Creacion de las secuencias necesarias
CREATE SEQUENCE SEC_ID_COMEN MINVALUE 0 INCREMENT BY 1 START WITH 0 CACHE 20 ORDER;
CREATE SEQUENCE SEC_ID_EVAL MINVALUE 0 INCREMENT BY 1 START WITH 0 CACHE 20 ORDER;
CREATE SEQUENCE SEC_ID_GENERO MINVALUE 0 INCREMENT BY 1 START WITH 0 CACHE 20 ORDER;
CREATE SEQUENCE SEC_ID_USUARIO MINVALUE 0 INCREMENT BY 1 START WITH 0 CACHE 20 ORDER;
CREATE SEQUENCE SEC_ID_REPORTE MINVALUE 0 INCREMENT BY 1 START WITH 0 CACHE 20 ORDER;
CREATE SEQUENCE SEC_ID_REV MINVALUE 0 INCREMENT BY 1 START WITH 0 CACHE 20 ORDER;

--Administrador con sus rsepectivas credenciales
INSERT INTO USUARIO values('noxer','0000',3);
INSERT INTO PERFIL values(SEC_ID_USUARIO.NEXTVAL,'e.noyer.silva@gmail.com','http://puu.sh/7Wq9x.png','20-06-1994','m','0','noxer');

--Usuario promedio pantoastedBOT (se utilizara para verificar comentarios)
INSERT INTO USUARIO values('pantoastedBOT','0001',0);
INSERT INTO PERFIL values(SEC_ID_USUARIO.NEXTVAL,'foo@gmail.com','http://3.bp.blogspot.com/--1GO3T-4NQU/UcDZ1oNK5HI/AAAAAAAABZw/Qzb8fZUAOGg/s1600/pan+toasted.jpg','05-04-2014','o','0','pantoastedBOT');

--GENEROS
INSERT INTO GENERO values(SEC_ID_GENERO.NEXTVAL,'RPG');
INSERT INTO GENERO values(SEC_ID_GENERO.NEXTVAL,'MMORPG');
INSERT INTO GENERO values(SEC_ID_GENERO.NEXTVAL,'FPS');
INSERT INTO GENERO values(SEC_ID_GENERO.NEXTVAL,'Accion');
INSERT INTO GENERO values(SEC_ID_GENERO.NEXTVAL,'Puzzle');
INSERT INTO GENERO values(SEC_ID_GENERO.NEXTVAL,'Shooter');
INSERT INTO GENERO values(SEC_ID_GENERO.NEXTVAL,'Multiplayer');
INSERT INTO GENERO values(SEC_ID_GENERO.NEXTVAL,'Survival');
INSERT INTO GENERO values(SEC_ID_GENERO.NEXTVAL,'Aventura');
INSERT INTO GENERO values(SEC_ID_GENERO.NEXTVAL,'Arcade');
INSERT INTO GENERO values(SEC_ID_GENERO.NEXTVAL,'Estrategia');
INSERT INTO GENERO values(SEC_ID_GENERO.NEXTVAL,'Musical');
INSERT INTO GENERO values(SEC_ID_GENERO.NEXTVAL,'Indie');
INSERT INTO GENERO values(SEC_ID_GENERO.NEXTVAL,'Casual');
INSERT INTO GENERO values(SEC_ID_GENERO.NEXTVAL,'Carreras');
INSERT INTO GENERO values(SEC_ID_GENERO.NEXTVAL,'Simuladores');
INSERT INTO GENERO values(SEC_ID_GENERO.NEXTVAL,'Deportes');

--Primera publicacion
INSERT INTO REVIEW values(SEC_ID_REV.NEXTVAL,'Body Hero','El mejor juego de la vida','','','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis diam. Pellentesque ut neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In dui magna, posuere eget, vestibulum et, tempor auctor, justo. In ac felis quis tortor malesuada pretium. Pellentesque auctor neque nec urna. Proin sapien ipsum, porta a, auctor quis, euismod ut, mi. Aenean viverra rhoncus pede. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Phasellus a est. Ph',0,'noxer');

--Primer comentario la estructura es; el id, el cuerpo del comentario, el id de 
--la publicacion,el nombre del usuario que lo escribi� y el nombre del autor del review.
INSERT INTO COMENTARIO values(SEC_ID_COMEN.NEXTVAL,'Lorem ipsum, soy un comentario.',0,'pantoastedBOT','noxer');
