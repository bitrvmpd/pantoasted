/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author EduardoElías
 */
public class ControladorDB {

    //Variable que almacena la conexion a la bd.
    private Connection conn;

    public void conectar() {
        try {
            //Indico el driver de la BD a usar desde java 6 arriba no es necesario
            //Pero con webservices incluidos se mareaba.
            Class.forName("oracle.jdbc.OracleDriver");
            //En sus equipos recuerden cambiar la direccion ip por 201.241.0.13
            //conn = DriverManager.getConnection("jdbc:oracle:thin:@201.241.0.13:1521:xe",
            //        "pantoastedDB", "Pantoasted2014DB");

            //Cambio para segunda entrega
            conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.1.108:1521:xe",
                    "pantoasted2", "pantoasted2");
            System.out.println("exito!");

        } catch (Exception ex) {
            System.out.println("FAIL");
        }
    }

    public boolean cerrarConexion() {
        try {
            conn.close();
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public String iniciarSesion(String usuario, String contrasena) {
        try {
            //Preparo la llamada del procedimiento almacenado
            conectar();
            CallableStatement cs = conn.prepareCall("{call PANTOASTEDFUNC.VALIDAR(?,?,?)}");
            cs.setString("p_usuario", usuario);
            cs.setString("p_contrasena", contrasena);
            cs.registerOutParameter("respuesta", Types.VARCHAR);
            cs.execute();

            return cs.getString("respuesta");

        } catch (SQLException ex) {
            //En caso de no encontrar una coincidencia, retorna false en privilegio
            return "false";
        } finally {
            cerrarConexion();
        }
    }

    public String nuevoUsuario(String usuario, String contrasena) {
        try {
            //Preparo la llamada del procedimiento almacenado
            conectar();
            CallableStatement cs = conn.prepareCall("{call PANTOASTEDFUNC.REG_USUARIO(?,?,?)}");
            cs.setString("n_usuario", usuario);
            cs.setString("contrasena", contrasena);
            cs.registerOutParameter("respuesta", Types.VARCHAR);
            cs.execute();

            //Retorna la respuesta de la operacion
            return cs.getString("respuesta");
        } catch (Exception ex) {
            return ex.getMessage();
        } finally {
            cerrarConexion();
        }
    }

    public String nuevoPerfil(String n_usuario, String p_correo_e, String p_url_avatar, String p_fecha_nac, String p_sexo) {
        try {
            //Preparo la llamada del procedimiento almacenado
            conectar();
            CallableStatement cs = conn.prepareCall("{call PANTOASTEDFUNC.CREAR_PERFIL(?,?,?,?,?,?)}");
            cs.setString("n_usuario", n_usuario);
            cs.setString("p_correo_e", p_correo_e);
            cs.setString("p_url_avatar", p_url_avatar);
            cs.setString("p_fecha_nac", p_fecha_nac);
            cs.setString("p_sexo", p_sexo);
            cs.registerOutParameter("respuesta", Types.VARCHAR);
            cs.execute();

            //Retorna la respuesta de la operacion
            return cs.getString("respuesta");
        } catch (Exception ex) {
            return "false";
        } finally {
            cerrarConexion();
        }
    }

    public String modificarPerfil(String n_usuario, String p_correo_e, String p_url_avatar, String p_fecha_nac, String p_sexo) {
        try {
            //Preparo la llamada del procedimiento almacenado
            conectar();
            CallableStatement cs = conn.prepareCall("{call PANTOASTEDFUNC.MODIFICAR_PERFIL(?,?,?,?,?,?)}");
            cs.setString("n_usuario", n_usuario);
            cs.setString("p_correo_e", p_correo_e);
            cs.setString("p_url_avatar", p_url_avatar);
            cs.setString("p_fecha_nac", p_fecha_nac);
            cs.setString("p_sexo", p_sexo);
            cs.registerOutParameter("respuesta", Types.VARCHAR);
            cs.execute();

            //Retorna la respuesta de la operacion
            return cs.getString("respuesta");
        } catch (Exception ex) {
            return "false";
        } finally {
            cerrarConexion();
        }
    }

    public String consultarPerfil(String n_usuario) {
        try {
            //Preparo la llamada del procedimiento almacenado
            conectar();
            CallableStatement cs = conn.prepareCall("{call PANTOASTEDFUNC.CONSULTAR_PERFIL(?,?,?,?,?,?,?)}");
            cs.setString("n_usuario", n_usuario);
            cs.registerOutParameter("p_correo_e", Types.VARCHAR);
            cs.registerOutParameter("p_url_avatar", Types.VARCHAR);
            cs.registerOutParameter("p_fecha_nac", Types.VARCHAR);
            cs.registerOutParameter("p_sexo", Types.VARCHAR);
            cs.registerOutParameter("p_puntos", Types.NUMERIC);

            cs.registerOutParameter("p_advertencias", Types.NUMERIC);
            cs.execute();

            //Retorna la respuesta de la operacion
            return "{\"correo_e\":\"" + cs.getString("p_correo_e") + "\",\"url_avatar\":\""
                    + cs.getString("p_url_avatar") + "\",\"fecha_nac\":\"" + cs.getString("p_fecha_nac")
                    + "\",\"sexo\":\"" + cs.getString("p_sexo") + "\",\"puntos\":\"" + cs.getString("p_puntos") + "\",\"advertencias\":\"" + cs.getInt("p_advertencias") + "\"}";
        } catch (Exception ex) {
            return "false";
        } finally {
            cerrarConexion();
        }
    }

    public String agregarReview(String n_usuario, String titulo, String subtitulo,
            String genero, String cuerpo, String url_video, String url_header,
            String rendimiento, String jugabilidad, String contenido, String controles) {
        try {
            //Preparo la llamada del procedimiento almacenado
            conectar();
            CallableStatement cs = conn.prepareCall("{call PANTOASTEDFUNC.CREAR_REVIEW(?,?,?,?,?,?,?,?,?,?,?,?)}");
            cs.setString("n_usuario", n_usuario);
            cs.setString("p_titulo", titulo);
            cs.setString("p_subtitulo", subtitulo);
            cs.setString("p_genero", genero);
            cs.setString("p_cuerpo", cuerpo);
            cs.setString("p_url_video", url_video);
            cs.setString("p_url_header", url_header);
            cs.setString("pRendimiento", rendimiento);
            cs.setString("pJugabilidad", jugabilidad);
            cs.setString("pContenido", contenido);
            cs.setString("pControles", controles);
            cs.registerOutParameter("respuesta", Types.VARCHAR);
            cs.execute();
            //Retorna la respuesta de la operacion true o false
            return "{\"respuesta\":\"" + cs.getString("respuesta") + "\"}";

        } catch (Exception ex) {
            return "false";
        } finally {
            cerrarConexion();
        }
    }

    public String consultarReview(String id_review) {
        try {
            //Preparo la llamada del procedimiento almacenado
            conectar();
            CallableStatement cs = conn.prepareCall("{call PANTOASTEDFUNC.CONSULTAR_REVIEW(?,?,?,?,?,?,?,?,?)}");
            cs.setString("p_id_review", id_review);
            cs.registerOutParameter("p_titulo", Types.VARCHAR);
            cs.registerOutParameter("p_subtitulo", Types.VARCHAR);
            cs.registerOutParameter("p_genero", Types.VARCHAR);
            cs.registerOutParameter("p_cuerpo", Types.VARCHAR);
            cs.registerOutParameter("p_url_video", Types.VARCHAR);
            cs.registerOutParameter("p_url_header", Types.VARCHAR);
            cs.registerOutParameter("p_usuario", Types.VARCHAR);
            cs.registerOutParameter("puntuacion", Types.VARCHAR);

            cs.execute();

            //Retorna la respuesta de la operacion
            return "{\"titulo\":\"" + cs.getString("p_titulo") + "\",\"subtitulo\":\""
                    + cs.getString("p_subtitulo") + "\",\"genero\":\"" + cs.getString("p_genero")
                    + "\",\"review\":\"" + cs.getString("p_cuerpo") + "\",\"video\":\""
                    + cs.getString("p_url_video") + "\",\"header\":\"" + cs.getString("p_url_header")
                    + "\",\"autor\":\"" + cs.getString("p_usuario") + "\",\"puntuacion\":\"" + cs.getString("puntuacion") + "\"}";
        } catch (Exception ex) {
            return "false";
        } finally {
            cerrarConexion();
        }
    }

    public String retornarComentarios(String id_review) {
        try {
            String cadenaJson = "[";
            conectar();
            CallableStatement cs = conn.prepareCall("{ call PANTOASTEDFUNC.RETORNAR_COMENTARIOS(?,?)");
            cs.registerOutParameter("cursor_comentarios", OracleTypes.CURSOR);
            cs.setString("p_id_review", id_review);

            cs.execute();
            ResultSet rs = (ResultSet) cs.getObject("cursor_comentarios");
            while (rs.next()) {

                if (rs.isFirst()) {
                    //Dentro de esto consultar por la imagen del usuario
                    //Nah lo puedes mandar todo desde la DB
                    int id_comen = rs.getInt("id_comen");
                    String cuerpo = rs.getString("cuerpo");
                    int rev_id_review = rs.getInt("review_id_rev");
                    String nomUsuario = rs.getString("usuario_nom_usuario");
                    String avatar = rs.getString("avatar");
                    cadenaJson = cadenaJson + "{\"id\":\"" + id_comen + "\",\"cuerpo\":\""
                            + cuerpo + "\",\"autor\":\"" + nomUsuario + "\",\"avatar\":\"" + avatar + "\"}";
                } else {
                    //Dentro de esto consultar por la imagen del usuario
                    //Nah lo puedes mandar todo desde la DB
                    int id_comen = rs.getInt("id_comen");
                    String cuerpo = rs.getString("cuerpo");
                    int rev_id_review = rs.getInt("review_id_rev");
                    String nomUsuario = rs.getString("usuario_nom_usuario");
                    String avatar = rs.getString("avatar");
                    cadenaJson = cadenaJson + ",{\"id\":\"" + id_comen + "\",\"cuerpo\":\""
                            + cuerpo + "\",\"autor\":\"" + nomUsuario + "\",\"avatar\":\"" + avatar + "\"}";
                }
            }
            cadenaJson = cadenaJson + "]";
            return cadenaJson;
        } catch (Exception ex) {
            return ("false");
        } finally {
            cerrarConexion();
        }

    }

    public String agregarComentario(String id_review, String id_usuario, String cuerpo) {
        conectar();
        try {
            CallableStatement cs = conn.prepareCall("{call PANTOASTEDFUNC.CREAR_COMENTARIO(?,?,?,?)}");
            cs.setString("cuerpo", cuerpo);
            cs.setString("p_id_review", id_review);
            cs.setString("n_usuario", id_usuario);
            cs.registerOutParameter("respuesta", Types.VARCHAR);
            cs.execute();
            return cs.getString("respuesta");
        } catch (Exception ex) {
            return "false";
        } finally {
            cerrarConexion();
        }
    }

    public String actualizarPuntos(String id_usuario, String operacion) {
        conectar();
        try {
            CallableStatement cs = conn.prepareCall("{call PANTOASTEDFUNC.ACTUALIZA_PUNTOSPERFIL(?,?,?)}");
            cs.setString("n_usuario", id_usuario);
            cs.setString("operacion", operacion);
            cs.registerOutParameter("respuesta", Types.VARCHAR);
            cs.execute();
            return cs.getString("respuesta");
        } catch (Exception ex) {
            return "false";
        } finally {
            cerrarConexion();
        }

    }

    public String modificarComentario(String id_comentario, String cuerpo) {
        conectar();
        try {
            CallableStatement cs = conn.prepareCall("{call PANTOASTEDFUNC.MODIFICAR_COMENTARIO(?,?,?)}");
            cs.setString("p_id_comen", id_comentario);
            cs.setString("p_cuerpo", cuerpo);
            cs.registerOutParameter("respuesta", Types.VARCHAR);
            cs.execute();
            return cs.getString("respuesta");
        } catch (Exception ex) {
            return "false";
        } finally {
            cerrarConexion();
        }
    }

    public String actualizarPuntosPublicacion(String id_review, String rendimiento, String jugabilidad, String contenido, String controles) {
        conectar();
        try {
            CallableStatement cs = conn.prepareCall("{call PANTOASTEDFUNC.ACTUALIZA_PUNTOSPUBLICACION(?,?,?,?,?,?)}");
            cs.setString("pRendimiento", rendimiento);
            cs.setString("pJugabilidad", jugabilidad);
            cs.setString("pContenido", contenido);
            cs.setString("pControles", controles);
            cs.setString("p_id_review", id_review);
            cs.registerOutParameter("respuesta", Types.VARCHAR);
            cs.execute();
            return cs.getString("respuesta");
        } catch (Exception ex) {
            return "false";
        } finally {
            cerrarConexion();
        }
    }

    public String reportePublicacion(String id, String tipo, String cuerpo) {
        conectar();
        try {
            CallableStatement cs;
            if (tipo.equals("c")) {
                cs = conn.prepareCall("{call PANTOASTEDFUNC.CREAR_REPORTE_COM(?,?,?)}");
            } else {
                cs = conn.prepareCall("{call PANTOASTEDFUNC.CREAR_REPORTE_REV(?,?,?)}");
            }
            cs.setString("p_id", id);
            cs.setString("p_motivo", cuerpo);
            cs.registerOutParameter("respuesta", Types.VARCHAR);
            cs.execute();
            return cs.getString("respuesta");
        } catch (Exception ex) {
            return "false";
        } finally {
            cerrarConexion();
        }
    }

    public String[] retornarReportes() {
        try {
            String[] arrayJson = new String[2];
            String cadenaJson = "[";
            conectar();
            CallableStatement cs = conn.prepareCall("{ call PANTOASTEDFUNC.CARGA_REPORTES(?,?)");
            cs.registerOutParameter("cursor_reportes_com", OracleTypes.CURSOR);
            cs.registerOutParameter("cursor_reportes_rev", OracleTypes.CURSOR);

            cs.execute();
            ResultSet rs = (ResultSet) cs.getObject("cursor_reportes_com");
            while (rs.next()) {

                if (rs.isFirst()) {
                    //Dentro de esto consultar por la imagen del usuario
                    //Nah lo puedes mandar todo desde la DB
                    int id = rs.getInt("id_reporte");
                    String motivo = rs.getString("motivo");
                    int id_com = rs.getInt("COMENTARIO_ID_COMEN");
                    String cuerpo = rs.getString("cuerpo");
                    cadenaJson = cadenaJson + "{\"id\":\"" + id + "\",\"motivo\":\""
                            + motivo + "\",\"id_com\":\"" + id_com + "\",cuerpo:\"" + cuerpo + "\"}";
                } else {
                    //Dentro de esto consultar por la imagen del usuario
                    //Nah lo puedes mandar todo desde la DB
                    int id = rs.getInt("id_reporte");
                    String motivo = rs.getString("motivo");
                    int id_com = rs.getInt("COMENTARIO_ID_COMEN");
                    String cuerpo = rs.getString("cuerpo");
                    cadenaJson = cadenaJson + ",{\"id\":\"" + id + "\",\"motivo\":\""
                            + motivo + "\",\"id_com\":\"" + id_com + "\",cuerpo:\"" + cuerpo + "\"}";
                }
            }
            cadenaJson = cadenaJson + "]";
            arrayJson[1] = cadenaJson;
            cadenaJson = "[";
            rs = (ResultSet) cs.getObject("cursor_reportes_rev");
            while (rs.next()) {

                if (rs.isFirst()) {
                    //Dentro de esto consultar por la imagen del usuario
                    //Nah lo puedes mandar todo desde la DB
                    int id = rs.getInt("id_reporte");
                    String motivo = rs.getString("motivo");
                    int id_rev = rs.getInt("REVIEW_ID_REV");
                    String titulo = rs.getString("titulo");
                    cadenaJson = cadenaJson + "{\"id\":\"" + id + "\",\"motivo\":\""
                            + motivo + "\",\"id_rev\":\"" + id_rev + "\",\"titulo\":\"" + titulo + "\"}";
                } else {
                    //Dentro de esto consultar por la imagen del usuario
                    //Nah lo puedes mandar todo desde la DB
                    int id = rs.getInt("id_reporte");
                    String motivo = rs.getString("motivo");
                    int id_rev = rs.getInt("REVIEW_ID_REV");
                    String titulo = rs.getString("titulo");
                    cadenaJson = cadenaJson + ",{\"id\":\"" + id + "\",\"motivo\":\""
                            + motivo + "\",\"id_rev\":\"" + id_rev + "\",\"titulo\":\"" + titulo + "\"}";
                }
            }
            cadenaJson = cadenaJson + "]";
            arrayJson[0] = cadenaJson;
            return arrayJson;
        } catch (Exception ex) {
            String[] error = {"false"};
            return error;
        } finally {
            cerrarConexion();
        }

    }

    public String eliminarReview(String id_review) {
        conectar();
        try {
            CallableStatement cs = conn.prepareCall("{call PANTOASTEDFUNC.ELIMINAR_REVIEW(?,?)}");
            cs.setString("p_id_review", id_review);
            cs.registerOutParameter("respuesta", Types.VARCHAR);
            cs.execute();
            return cs.getString("respuesta");
        } catch (Exception ex) {
            return "false";
        } finally {
            cerrarConexion();
        }
    }

    public void NuevaAdvertenciaRev(String id_review, String id_rep) {
        conectar();
        try {
            CallableStatement cs = conn.prepareCall("{call PANTOASTEDFUNC.NUEVA_ADVERTENCIA_REV(?,?)}");
            cs.setString("p_id", id_review);
            cs.setString("p_id_reporte", id_rep);
            cs.execute();
        } catch (Exception ex) {
        } finally {
            cerrarConexion();
        }
    }

    public void NuevaAdvertenciaCom(String id_com, String id_rep) {
        conectar();
        try {
            CallableStatement cs = conn.prepareCall("{call PANTOASTEDFUNC.NUEVA_ADVERTENCIA_COM(?,?)}");
            cs.setString("p_id", id_com);
            cs.setString("p_id_reporte", id_rep);
            cs.execute();
        } catch (Exception ex) {
        } finally {
            cerrarConexion();
        }
    }

    public String retornaRandom() {
        try {
            String cadenaJson = "[";
            conectar();
            CallableStatement cs = conn.prepareCall("{ call PANTOASTEDFUNC.RETORNA_RANDOM(?)}");
            cs.registerOutParameter("cursor_rev", OracleTypes.CURSOR);
            cs.execute();
            ResultSet rs = (ResultSet) cs.getObject("cursor_rev");
            while (rs.next()) {

                if (rs.isFirst()) {
                    //Dentro de esto consultar por la imagen del usuario
                    //Nah lo puedes mandar todo desde la DB
                    int id_rev = rs.getInt("id_rev");
                    String titulo = rs.getString("titulo");
                    String subtitulo = rs.getString("subtitulo");
                    String usuario = rs.getString("usuario_nom_usuario");
                    cadenaJson = cadenaJson + "{\"id_rev\":\"" + id_rev + "\",\"titulo\":\""
                            + titulo + "\",\"subtitulo\":\"" + subtitulo + "\",\"usuario\":\"" + usuario + "\"}";
                } else {
                    //Dentro de esto consultar por la imagen del usuario
                    //Nah lo puedes mandar todo desde la DB
                    int id_rev = rs.getInt("id_rev");
                    String titulo = rs.getString("titulo");
                    String subtitulo = rs.getString("subtitulo");
                    String usuario = rs.getString("usuario_nom_usuario");
                    cadenaJson = cadenaJson + ",{\"id_rev\":\"" + id_rev + "\",\"titulo\":\""
                            + titulo + "\",\"subtitulo\":\"" + subtitulo + "\",\"usuario\":\"" + usuario + "\"}";
                }
            }
            cadenaJson = cadenaJson + "]";
            return cadenaJson;
        } catch (Exception ex) {
            return ("false");
        } finally {
            cerrarConexion();
        }
    }

    public String retornoPublicacionesTop() {
        try {
            String cadenaJson = "[";
            conectar();
            CallableStatement cs = conn.prepareCall("{ call PANTOASTEDFUNC.CARGA_TOPREV(?)");
            cs.registerOutParameter("cursor_toprev", OracleTypes.CURSOR);
            cs.execute();
            ResultSet rs = (ResultSet) cs.getObject("cursor_toprev");
            while (rs.next()) {

                if (rs.isFirst()) {
                    //Dentro de esto consultar por la imagen del usuario
                    //Nah lo puedes mandar todo desde la DB
                    int id_rev = rs.getInt("id_rev");
                    String titulo = rs.getString("titulo");
                    String subtitulo = rs.getString("subtitulo");
                    String url_header = rs.getString("url_header");
                    String usuario = rs.getString("usuario_nom_usuario");
                    cadenaJson = cadenaJson + "{\"id_rev\":\"" + id_rev + "\",\"titulo\":\""
                            + titulo + "\",\"subtitulo\":\"" + subtitulo + "\",\"usuario\":\"" + usuario + "\",\"url_header\":\"" + url_header + "\"}";
                } else {
                    //Dentro de esto consultar por la imagen del usuario
                    //Nah lo puedes mandar todo desde la DB
                    int id_rev = rs.getInt("id_rev");
                    String titulo = rs.getString("titulo");
                    String subtitulo = rs.getString("subtitulo");
                    String url_header = rs.getString("url_header");
                    String usuario = rs.getString("usuario_nom_usuario");
                    cadenaJson = cadenaJson + ",{\"id_rev\":\"" + id_rev + "\",\"titulo\":\""
                            + titulo + "\",\"subtitulo\":\"" + subtitulo + "\",\"usuario\":\"" + usuario + "\",\"url_header\":\"" + url_header + "\"}";
                }
            }
            cadenaJson = cadenaJson + "]";
            return cadenaJson;
        } catch (Exception ex) {
            return ("false");
        } finally {
            cerrarConexion();
        }
    }

    public String retornaNuevas_publicaciones() {
        try {
            String publicaciones = "[";
            conectar();
            CallableStatement cs = conn.prepareCall("{ call PANTOASTEDFUNC.NUEVAS_PUBLICACIONES(?)}");
            cs.registerOutParameter("cursor_new_review", OracleTypes.CURSOR);
            cs.execute();
            ResultSet rs = (ResultSet) cs.getObject("cursor_new_review");

            while (rs.next()) {
                if (rs.isFirst()) {
                    int id = rs.getInt("id_rev");
                    String titulo = rs.getString("titulo");
                    publicaciones = publicaciones + "{\"id_rev\":\"" + id + "\",\"titulo\":\"" + titulo + "\"}";
                } else {
                    int id = rs.getInt("id_rev");
                    String titulo = rs.getString("titulo");
                    publicaciones = publicaciones + ",{\"id_rev\":\"" + id + "\",\"titulo\":\"" + titulo + "\"}";
                }
            }
            publicaciones = publicaciones + "]";
            return publicaciones;
        } catch (Exception e) {
            return ("false");
        } finally {
            cerrarConexion();
        }

    }

    public String eliminarComentario(String id_comentario) {
        try {
            conectar();
            CallableStatement cs = conn.prepareCall("{call PANTOASTEDFUNC.ELIMINAR_COMENTARIO(?,?)}");
            cs.setString("p_id_comentario", id_comentario);
            cs.registerOutParameter("respuesta", Types.VARCHAR);
            cs.execute();
            return cs.getString("respuesta");
        } catch (Exception ex) {
            return "false";
        } finally {
            cerrarConexion();
        }
    }

    public String Derivar(String id_rep, String id_procedimiento) {
        conectar();
        try {
            CallableStatement cs;
            if (id_procedimiento.equals("review")) {
                //Llamo a derivar_rev
                cs = conn.prepareCall("{call PANTOASTEDFUNC.DERIVAR_REV(?)}");
            } else {
                //Llamo a derivar_com
                cs = conn.prepareCall("{call PANTOASTEDFUNC.DERIVAR_COM(?)}");
            }
            cs.setString("p_id_rep", id_rep);
            cs.execute();
            return "true";
        } catch (Exception ex) {
            return "false";
        } finally {
            cerrarConexion();
        }
    }
    
    
     public String retornoBaneados() {
        try {
            String cadenaJson = "[";
            conectar();
            CallableStatement cs = conn.prepareCall("{ call PANTOASTEDFUNC.CARGA_BANEADOS(?)");
            cs.registerOutParameter("cursor_baneados", OracleTypes.CURSOR);
            cs.execute();
            ResultSet rs = (ResultSet) cs.getObject("cursor_baneados");
            while (rs.next()) {

                if (rs.isFirst()) {
                    //Dentro de esto consultar por la imagen del usuario
                    //Nah lo puedes mandar todo desde la DB
                    String nom_usuario  = rs.getString("usuario_nom_usuario");
                    cadenaJson = cadenaJson + "{\"nom_usuario\":\"" + nom_usuario + "\"}";
                } else {
                    //Dentro de esto consultar por la imagen del usuario
                    //Nah lo puedes mandar todo desde la DB
                    String nom_usuario  = rs.getString("usuario_nom_usuario");
                    cadenaJson = cadenaJson + ",{\"nom_usuario\":\"" + nom_usuario + "\"}";
                }
            }
            cadenaJson = cadenaJson + "]";
            return cadenaJson;
        } catch (Exception ex) {
            return ("false");
        } finally {
            cerrarConexion();
        }
    }
     
     public String banDefinido(String id_usuario, String fecha) {
        conectar();
        try {
            CallableStatement cs;
            cs = conn.prepareCall("{call PANTOASTEDFUNC.BAN_DEFINIDO(?,?)}");
            cs.setString("p_nom_usuario", id_usuario);
            cs.setString("fecha", fecha);
            cs.execute();
            return "true";
        } catch (Exception ex) {
            return "false";
        } finally {
            cerrarConexion();
        }
    }
     
       public String banIndefinido(String id_usuario) {
        conectar();
        try {
            CallableStatement cs;
            cs = conn.prepareCall("{call PANTOASTEDFUNC.BAN_INDEFINIDO(?)}");
            cs.setString("p_nom_usuario", id_usuario);
            cs.execute();
            return "true";
        } catch (Exception ex) {
            return "false";
        } finally {
            cerrarConexion();
        }
    }

}
