/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author EduardoElías
 */
@WebService(serviceName = "PantoastedWS")
public class PantoastedWS {

    /**
     * Web service operation
     */
    @WebMethod(operationName = "iniciarSesion")
    public String iniciarSesion(@WebParam(name = "usuario") final String usuario, @WebParam(name = "contrasena") final String contrasena) {
         ControladorDB bd = new ControladorDB();
        //Se retorna correctamente el usuario y el privilegio solo en caso
        //de existir dentro de la DB, el nombre de usuario se debe almacenar
        //en una cookie junto con el privilegio
        //Si el privilegio es false/null no existe dentro de la DB y se niega
        //El acceso.
        return "{\"usuario\":\"" + usuario + "\",\"privilegio\":\"" + bd.iniciarSesion(usuario, contrasena) + "\"}";
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "registrarUsuario")
    public String registrarUsuario(@WebParam(name = "usuario") final String usuario, @WebParam(name = "contrasena") final String contrasena) {
        ControladorDB bd = new ControladorDB();
        //Como respuesta retornara un String en formato JSON
        //Para leerlo se llama a la clase DecodificarJSON con el nombre
        //del identificador. en este caso respuesta
        return "{\"respuesta\":\""+bd.nuevoUsuario(usuario, contrasena)+"\"}";
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "cargarPerfil")
    public String cargarPerfil(@WebParam(name = "usuario") final String usuario) {
        //TODO write your implementation code here:
        ControladorDB bd = new ControladorDB();
        return bd.consultarPerfil(usuario);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "modificarPerfil")
    public String modificarPerfil(@WebParam(name = "usuario") final String usuario, @WebParam(name = "correo") final String correo, @WebParam(name = "url_avatar") final String url_avatar, @WebParam(name = "fecha_nac") final String fecha_nac, @WebParam(name = "sexo") final String sexo) {
        //TODO write your implementation code here:+
        ControladorDB bd = new ControladorDB();
        return bd.modificarPerfil(usuario, correo, url_avatar, fecha_nac, sexo);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "agregarReview")
    public String agregarReview(@WebParam(name = "usuario") final String usuario, 
            @WebParam(name = "titulo") final String titulo, @WebParam(name = "subtitulo") final String subtitulo,
            @WebParam(name = "genero") final String genero, @WebParam(name = "review") final String review,
            @WebParam(name = "url_video") final String url_video, @WebParam(name = "url_header") final String url_header,
            @WebParam(name = "rendimiento") final String rendimiento,@WebParam(name = "jugabilidad") final String jugabilidad,
            @WebParam(name = "contenido") final String contenido,@WebParam(name = "controles") final String controles) {
        //TODO write your implementation code here:
        ControladorDB bd = new ControladorDB();
        return bd.agregarReview(usuario, titulo, subtitulo, genero, review, url_video, url_header,rendimiento,jugabilidad,contenido,controles);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "consultarReview")
    public String consultarReview(@WebParam(name = "id_review") final String id_review) {
        //TODO write your implementation code here:
        ControladorDB bd = new ControladorDB();
        return bd.consultarReview(id_review);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "obtenerComentarios")
    public String obtenerComentarios(@WebParam(name = "id_review") final String id_review) {
        //TODO write your implementation code here:
        ControladorDB bd = new ControladorDB();
        return bd.retornarComentarios(id_review);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "crearPerfil")
    public String crearPerfil(@WebParam(name = "usuario") final String usuario, @WebParam(name = "correo") final String correo, @WebParam(name = "url_avatar") final String url_avatar, @WebParam(name = "fecha_nac") final String fecha_nac, @WebParam(name = "sexo") final String sexo) {
        //TODO write your implementation code here:
        ControladorDB bd = new ControladorDB();
        return bd.nuevoPerfil(usuario, correo, url_avatar, fecha_nac, sexo);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "nuevoComentario")
    public String nuevoComentario(@WebParam(name = "id_review") final String id_review, @WebParam(name = "id_usuario") final String id_usuario, @WebParam(name = "cuerpo") final String cuerpo) {
        //TODO write your implementation code here:
        ControladorDB bd = new ControladorDB();
        return bd.agregarComentario(id_review, id_usuario, cuerpo); 
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "actualizarPuntos")
    public String actualizarPuntos(@WebParam(name = "usuario") final String usuario, @WebParam(name = "operacion") final String operacion) {
        //TODO write your implementation code here:
        ControladorDB bd = new ControladorDB();
        return bd.actualizarPuntos(usuario, operacion);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "modificarComentario")
    public String modificarComentario(@WebParam(name = "p_id_comentario") final String p_id_comentario, @WebParam(name = "p_cuerpo") final String p_cuerpo) {
        //TODO write your implementation code here:
       ControladorDB bd = new ControladorDB();
       return bd.modificarComentario(p_id_comentario, p_cuerpo);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "actualizarPuntosPublicacion")
    public String actualizarPuntosPublicacion(@WebParam(name = "id_review") final String id_review, @WebParam(name = "rendimiento") final String rendimiento, @WebParam(name = "jugabilidad") final String jugabilidad, @WebParam(name = "contenido") final String contenido, @WebParam(name = "controles") final String controles) {
        //TODO write your implementation code here:
        ControladorDB bd = new ControladorDB();
        return bd.actualizarPuntosPublicacion(id_review, rendimiento, jugabilidad, contenido, controles);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "reportePublicacion")
    public String reportePublicacion(@WebParam(name = "id_review") final String id_review, @WebParam(name = "tipo") final String tipo, @WebParam(name = "cuerpo") final String cuerpo) {
        //TODO write your implementation code here:
        ControladorDB bd = new ControladorDB();
        return bd.reportePublicacion(id_review, tipo, cuerpo);
    }

    /**
     * Web service operation
     * @return 
     */
    @WebMethod(operationName = "retornarReportes")
    public String[] retornarReportes() {
        //TODO write your implementation code here:
        ControladorDB bd = new ControladorDB();
        return bd.retornarReportes();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "eliminarReview")
    public String eliminarReview(@WebParam(name = "id_review") final String id_review) {
        //TODO write your implementation code here:
        ControladorDB bd = new ControladorDB();
        return bd.eliminarReview(id_review);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "nuevaAdvertenciaRev")
    public void nuevaAdvertenciaRev(@WebParam(name = "id_review") final String id_review,@WebParam(name = "id_rep") final String id_rep) {
        //TODO write your implementation code here:
        ControladorDB bd = new ControladorDB();
        bd.NuevaAdvertenciaRev(id_review,id_rep);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "nuevaAdvertenciaCom")
    public void nuevaAdvertenciaCom(@WebParam(name = "id_com") final String id_com,@WebParam(name = "id_rep") final String id_rep) {
        //TODO write your implementation code here:
        ControladorDB bd = new ControladorDB();
        bd.NuevaAdvertenciaCom(id_com,id_rep);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "retornaRandom")
    public String retornaRandom() {
        //TODO write your implementation code here:
        ControladorDB bd = new ControladorDB();
        return bd.retornaRandom();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "retornarTop")
    public String retornarTop() {
        //TODO write your implementation code here:
        ControladorDB bd = new ControladorDB();
        return bd.retornoPublicacionesTop();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "Nuevas_Publicaciones")
    public String Nuevas_Publicaciones() {
        ControladorDB db = new ControladorDB();
        return db.retornaNuevas_publicaciones();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "eliminarComentario")
    public String eliminarComentario(@WebParam(name = "id_comentario") final String id_comentario) {
        //TODO write your implementation code here:
        ControladorDB db = new ControladorDB();
        return db.eliminarComentario(id_comentario);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "Derivar")
    public String Derivar(@WebParam(name = "id_rep") final String id_rep, @WebParam(name = "id_procedimiento") final String id_procedimiento) {
        //TODO write your implementation code here:
       ControladorDB db = new ControladorDB();
       return db.Derivar(id_rep,id_procedimiento);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "cargaBaneados")
    public String cargaBaneados() {
        //TODO write your implementation code here:
        ControladorDB db = new ControladorDB();
        return db.retornoBaneados();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "banDefinido")
    public String banDefinido(@WebParam(name = "id_usuario") final String id_usuario,@WebParam(name = "fecha") final String fecha) {
        //TODO write your implementation code here:
        ControladorDB db = new ControladorDB();
        return db.banDefinido(id_usuario,fecha);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "banIndefinido")
    public String banIndefinido(@WebParam(name = "id_usuario") final String id_usuario) {
        //TODO write your implementation code here:
        ControladorDB db = new ControladorDB();
        return db.banIndefinido(id_usuario);
    }
    
    
}
